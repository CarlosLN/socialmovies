package com.socialmovies.SocialMovies.model.Serie;

import java.util.List;

public class MSeriesBuscadasEncontradas {

    private int cuantasSeriesEncontradas;
    private List<MSerieBuscada> seriesBuscadasList;

    public MSeriesBuscadasEncontradas(){}

    public MSeriesBuscadasEncontradas(int cuantasSeriesEncontradas, List<MSerieBuscada> seriesBuscadasList){
        this.cuantasSeriesEncontradas = cuantasSeriesEncontradas;
        this.seriesBuscadasList = seriesBuscadasList;
    }

    public int getCuantasSeriesEncontradas() {
        return cuantasSeriesEncontradas;
    }

    public void setCuantasSeriesEncontradas(int cuantasSeriesEncontradas) {
        this.cuantasSeriesEncontradas = cuantasSeriesEncontradas;
    }

    public List<MSerieBuscada> getSeriesBuscadasList() {
        return seriesBuscadasList;
    }

    public void setSeriesBuscadasList(List<MSerieBuscada> seriesBuscadasList) {
        this.seriesBuscadasList = seriesBuscadasList;
    }
}
