package com.socialmovies.SocialMovies.model.Serie;

import java.util.Date;

public class MDatosActualizadosTrasVotarCapitulo {

    private double votacionMediaSerie;
    private int numVotantesSerie;
    private double tuVotoMedioSerie;
    private int numTemporada;
    private int numVotacionesTemporada;
    private double votacionMediaTemporada;
    private int numVotacionesCapitulo;
    private double votacionMediaCapitulo;
    private Date fechatuVoto;

    public MDatosActualizadosTrasVotarCapitulo(){}

    public MDatosActualizadosTrasVotarCapitulo(double votacionMediaSerie, int numVotantesSerie, double tuVotoMedioSerie, int numTemporada, int numVotacionesTemporada, double votacionMediaTemporada, int numVotacionesCapitulo, double votacionMediaCapitulo, Date fechatuVoto) {
        this.votacionMediaSerie = votacionMediaSerie;
        this.numVotantesSerie = numVotantesSerie;
        this.tuVotoMedioSerie = tuVotoMedioSerie;
        this.numTemporada = numTemporada;
        this.numVotacionesTemporada = numVotacionesTemporada;
        this.votacionMediaTemporada = votacionMediaTemporada;
        this.numVotacionesCapitulo = numVotacionesCapitulo;
        this.votacionMediaCapitulo = votacionMediaCapitulo;
        this.fechatuVoto = fechatuVoto;
    }

    public double getVotacionMediaSerie() {
        return votacionMediaSerie;
    }

    public void setVotacionMediaSerie(double votacionMediaSerie) {
        this.votacionMediaSerie = votacionMediaSerie;
    }

    public int getNumVotantesSerie() {
        return numVotantesSerie;
    }

    public void setNumVotantesSerie(int numVotantesSerie) {
        this.numVotantesSerie = numVotantesSerie;
    }

    public double getTuVotoMedioSerie() {
        return tuVotoMedioSerie;
    }

    public void setTuVotoMedioSerie(double tuVotoMedioSerie) {
        this.tuVotoMedioSerie = tuVotoMedioSerie;
    }

    public int getNumTemporada() {
        return numTemporada;
    }

    public void setNumTemporada(int numTemporada) {
        this.numTemporada = numTemporada;
    }

    public int getNumVotacionesTemporada() {
        return numVotacionesTemporada;
    }

    public void setNumVotacionesTemporada(int numVotacionesTemporada) {
        this.numVotacionesTemporada = numVotacionesTemporada;
    }

    public double getVotacionMediaTemporada() {
        return votacionMediaTemporada;
    }

    public void setVotacionMediaTemporada(double votacionMediaTemporada) {
        this.votacionMediaTemporada = votacionMediaTemporada;
    }

    public int getNumVotacionesCapitulo() {
        return numVotacionesCapitulo;
    }

    public void setNumVotacionesCapitulo(int numVotacionesCapitulo) {
        this.numVotacionesCapitulo = numVotacionesCapitulo;
    }

    public double getVotacionMediaCapitulo() {
        return votacionMediaCapitulo;
    }

    public void setVotacionMediaCapitulo(double votacionMediaCapitulo) {
        this.votacionMediaCapitulo = votacionMediaCapitulo;
    }

    public Date getFechatuVoto() {
        return fechatuVoto;
    }

    public void setFechatuVoto(Date fechatuVoto) {
        this.fechatuVoto = fechatuVoto;
    }
}
