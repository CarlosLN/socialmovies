package com.socialmovies.SocialMovies.model.Serie;

import java.util.Date;

public class MVotacionAmigoEnFichaSerie {

    private String username;
    private String avatar;
    private double voto;
    private int numTemporada;
    private int numCapitulo;
    private String tituloCapitulo;
    private Date fechaVoto;

    public MVotacionAmigoEnFichaSerie() {
    }

    public MVotacionAmigoEnFichaSerie(String username, String avatar, double voto, int numTemporada, int numCapitulo, String tituloCapitulo, Date fechaVoto) {
        this.username = username;
        this.avatar = avatar;
        this.voto = voto;
        this.numTemporada = numTemporada;
        this.numCapitulo = numCapitulo;
        this.tituloCapitulo = tituloCapitulo;
        this.fechaVoto = fechaVoto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public double getVoto() {
        return voto;
    }

    public void setVoto(double voto) {
        this.voto = voto;
    }

    public int getNumTemporada() {
        return numTemporada;
    }

    public void setNumTemporada(int numTemporada) {
        this.numTemporada = numTemporada;
    }

    public int getNumCapitulo() {
        return numCapitulo;
    }

    public void setNumCapitulo(int numCapitulo) {
        this.numCapitulo = numCapitulo;
    }

    public String getTituloCapitulo() {
        return tituloCapitulo;
    }

    public void setTituloCapitulo(String tituloCapitulo) {
        this.tituloCapitulo = tituloCapitulo;
    }

    public Date getFechaVoto() {
        return fechaVoto;
    }

    public void setFechaVoto(Date fechaVoto) {
        this.fechaVoto = fechaVoto;
    }
}
