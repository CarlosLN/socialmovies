package com.socialmovies.SocialMovies.model.Serie;

import java.util.Date;

public class MSerieRecomendadaMejorvaloradaMasvotada {

    private long idSerie;
    private String titulo;
    private String poster;
    private Date fechaEstreno;
    private double votacionMedia;
    private int numVotantes;
    private double miVoto;

    public MSerieRecomendadaMejorvaloradaMasvotada(){}

    public MSerieRecomendadaMejorvaloradaMasvotada(long idSerie, String titulo, String poster, Date fechaEstreno, double votacionMedia, int numVotantes, double miVoto) {
        this.idSerie = idSerie;
        this.titulo = titulo;
        this.poster = poster;
        this.fechaEstreno = fechaEstreno;
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.miVoto = miVoto;
    }

    public long getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(long idSerie) {
        this.idSerie = idSerie;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getNumVotantes() {
        return numVotantes;
    }

    public void setNumVotantes(int numVotantes) {
        this.numVotantes = numVotantes;
    }

    public double getMiVoto() {
        return miVoto;
    }

    public void setMiVoto(double miVoto) {
        this.miVoto = miVoto;
    }
}
