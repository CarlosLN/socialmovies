package com.socialmovies.SocialMovies.model.Serie;

import java.util.Date;

public class MComentarioCapituloSerieEnPerfilAmigo {

    private long idSerie;
    private String titulo;
    private Date fechaEstreno;
    private String poster;

    private int numTemporada;
    private int numEpisodio;
    private String tituloEpisodio;

    private String comentarioUsuario;
    private Date fechaComentario;

    public MComentarioCapituloSerieEnPerfilAmigo(){

    }

    public MComentarioCapituloSerieEnPerfilAmigo(long idSerie, String titulo, Date fechaEstreno, String poster, int numTemporada, int numEpisodio, String tituloEpisodio, String comentarioUsuario, Date fechaComentario) {
        this.idSerie = idSerie;
        this.titulo = titulo;
        this.fechaEstreno = fechaEstreno;
        this.poster = poster;
        this.numTemporada = numTemporada;
        this.numEpisodio = numEpisodio;
        this.tituloEpisodio = tituloEpisodio;
        this.comentarioUsuario = comentarioUsuario;
        this.fechaComentario = fechaComentario;
    }

    public long getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(long idSerie) {
        this.idSerie = idSerie;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public int getNumTemporada() {
        return numTemporada;
    }

    public void setNumTemporada(int numTemporada) {
        this.numTemporada = numTemporada;
    }

    public int getNumEpisodio() {
        return numEpisodio;
    }

    public void setNumEpisodio(int numEpisodio) {
        this.numEpisodio = numEpisodio;
    }

    public String getTituloEpisodio() {
        return tituloEpisodio;
    }

    public void setTituloEpisodio(String tituloEpisodio) {
        this.tituloEpisodio = tituloEpisodio;
    }

    public String getComentarioUsuario() {
        return comentarioUsuario;
    }

    public void setComentarioUsuario(String comentarioUsuario) {
        this.comentarioUsuario = comentarioUsuario;
    }

    public Date getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(Date fechaComentario) {
        this.fechaComentario = fechaComentario;
    }
}
