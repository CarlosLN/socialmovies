package com.socialmovies.SocialMovies.model.Serie;

import com.socialmovies.SocialMovies.entity.Series.Serie;

import java.util.Date;

public class MVotacionSerieEnPerfilAmigo {

    private long idSerie;
    private String titulo;
    private Date fechaEstreno;
    private String poster;

    private int numTemporada;
    private int numEpisodio;
    private String tituloEpisodio;

    private double votacionMedia;
    private int numVotantes;

    private double votoUsuario;
    private Date fechaVotoUsuario;

    private double tuVoto;
    private Date fechaTuVoto;

    public MVotacionSerieEnPerfilAmigo(){}

    public MVotacionSerieEnPerfilAmigo(long idSerie, String titulo, Date fechaEstreno, String poster, int numTemporada, int numEpisodio, String tituloEpisodio, double votoUsuario, Date fechaVotoUsuario) {
        this.idSerie = idSerie;
        this.titulo = titulo;
        this.fechaEstreno = fechaEstreno;
        this.poster = poster;
        this.numTemporada = numTemporada;
        this.numEpisodio = numEpisodio;
        this.tituloEpisodio = tituloEpisodio;
        this.votoUsuario = votoUsuario;
        this.fechaVotoUsuario = fechaVotoUsuario;
    }

    public MVotacionSerieEnPerfilAmigo(Serie serie, int numTemporada, int numEpisodio, String tituloEpisodio, double votacionMedia, int numVotantes, double votoUsuario, Date fechaVotoUsuario, double tuVoto, Date fechaTuVoto) {
        this.idSerie = serie.getId();
        this.titulo = serie.getTitulo();
        this.fechaEstreno = serie.getFecha_estreno();
        this.poster = serie.getPoster();
        this.numTemporada = numTemporada;
        this.numEpisodio = numEpisodio;
        this.tituloEpisodio = tituloEpisodio;
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.votoUsuario = votoUsuario;
        this.fechaVotoUsuario = fechaVotoUsuario;
        this.tuVoto = tuVoto;
        this.fechaTuVoto = fechaTuVoto;
    }

    public long getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(long idSerie) {
        this.idSerie = idSerie;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public int getNumTemporada() {
        return numTemporada;
    }

    public void setNumTemporada(int numTemporada) {
        this.numTemporada = numTemporada;
    }

    public int getNumEpisodio() {
        return numEpisodio;
    }

    public void setNumEpisodio(int numEpisodio) {
        this.numEpisodio = numEpisodio;
    }

    public String getTituloEpisodio() {
        return tituloEpisodio;
    }

    public void setTituloEpisodio(String tituloEpisodio) {
        this.tituloEpisodio = tituloEpisodio;
    }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getNumVotantes() {
        return numVotantes;
    }

    public void setNumVotantes(int numVotantes) {
        this.numVotantes = numVotantes;
    }

    public double getVotoUsuario() {
        return votoUsuario;
    }

    public void setVotoUsuario(double votoUsuario) {
        this.votoUsuario = votoUsuario;
    }

    public Date getFechaVotoUsuario() {
        return fechaVotoUsuario;
    }

    public void setFechaVotoUsuario(Date fechaVotoUsuario) {
        this.fechaVotoUsuario = fechaVotoUsuario;
    }

    public double getTuVoto() {
        return tuVoto;
    }

    public void setTuVoto(double tuVoto) {
        this.tuVoto = tuVoto;
    }

    public Date getFechaTuVoto() {
        return fechaTuVoto;
    }

    public void setFechaTuVoto(Date fechaTuVoto) {
        this.fechaTuVoto = fechaTuVoto;
    }
}
