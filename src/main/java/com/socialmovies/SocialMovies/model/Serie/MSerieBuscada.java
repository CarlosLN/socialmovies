package com.socialmovies.SocialMovies.model.Serie;

import com.socialmovies.SocialMovies.entity.Series.Serie;

import java.util.Date;

public class MSerieBuscada {

    private long idSerie;
    private String titulo;
    private String poster;
    private Date fechaEstreno;

    public MSerieBuscada(){}

    public MSerieBuscada(Serie serie){
        this.idSerie = serie.getId();
        this.titulo = serie.getTitulo();
        this.poster = serie.getPoster();
        this.fechaEstreno = serie.getFecha_estreno();
    }

    public MSerieBuscada(long idSerie, String titulo, String poster, Date fechaEstreno) {
        this.idSerie = idSerie;
        this.titulo = titulo;
        this.poster = poster;
        this.fechaEstreno = fechaEstreno;
    }

    public long getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(long idSerie) {
        this.idSerie = idSerie;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }
}
