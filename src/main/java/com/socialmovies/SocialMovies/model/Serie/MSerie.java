package com.socialmovies.SocialMovies.model.Serie;

import com.socialmovies.SocialMovies.entity.Series.Serie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MSerie {

    private long id;
    private String titulo;
    private String sinopsis;
    private Date fechaEstreno;
    private String direccion;
    private String reparto;
    private String poster;
    private String duracion;
    private String trailer;
    private String genero;
    private List<String> distribuidoras;
    private List<MTemporada> temporadas;
    private double votacionMedia;
    private int numVotantes;
    private double tuVoto;
    private List<MVotacionAmigoEnFichaSerie> votacionesAmigos;
    private int cuantosAmigosHanVotadoLaSerie;
    private List<MComentarioAmigoEnFichaCapituloSerie> comentariosAmigos;
    private int cuantosAmigosHanComentadoLaSerie;

    public MSerie(){}

    public MSerie(Serie serie, List<MTemporada> temporadas, double votacionMedia, int numVotantes, double tuVoto, int cuantosAmigosHanVotadoLaSerie, int cuantosAmigosHanComentadoLaSerie){
        this.id = serie.getId();
        this.titulo = serie.getTitulo();
        this.sinopsis = serie.getSinopsis();
        this.fechaEstreno = serie.getFecha_estreno();
        this.direccion = serie.getDireccion();
        this.reparto = serie.getReparto();
        this.poster = serie.getPoster();
        this.duracion = serie.getDuracion();
        this.trailer = serie.getTrailer();
        this.genero = serie.getGenero();
        this.distribuidoras = this.obtenerDistribuidoras(serie.getDistribuidoras());
        this.temporadas = temporadas;
    }

    public MSerie(Serie serie, List<MTemporada> temporadas, double votacionMedia, int numVotantes, double tuVoto, List<MVotacionAmigoEnFichaSerie> votacionesAmigos, int cuantosAmigosHanVotadoLaSerie, List<MComentarioAmigoEnFichaCapituloSerie> comentariosAmigos, int cuantosAmigosHanComentadoLaSerie){
        this.id = serie.getId();
        this.titulo = serie.getTitulo();
        this.sinopsis = serie.getSinopsis();
        this.fechaEstreno = serie.getFecha_estreno();
        this.direccion = serie.getDireccion();
        this.reparto = serie.getReparto();
        this.poster = serie.getPoster();
        this.duracion = serie.getDuracion();
        this.trailer = serie.getTrailer();
        this.genero = serie.getGenero();
        this.distribuidoras = this.obtenerDistribuidoras(serie.getDistribuidoras());
        this.temporadas = temporadas;
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.tuVoto = tuVoto;
        this.votacionesAmigos = votacionesAmigos;
        this.cuantosAmigosHanVotadoLaSerie = cuantosAmigosHanVotadoLaSerie;
        this.comentariosAmigos = comentariosAmigos;
        this.cuantosAmigosHanComentadoLaSerie = cuantosAmigosHanComentadoLaSerie;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getReparto() {
        return reparto;
    }

    public void setReparto(String reparto) {
        this.reparto = reparto;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public List<String> getDistribuidoras() {
        return distribuidoras;
    }

    public void setDistribuidoras(List<String> distribuidoras) {
        this.distribuidoras = distribuidoras;
    }

    public List<MTemporada> getTemporadas() {
        return temporadas;
    }

    public void setTemporadas(List<MTemporada> temporadas) {
        this.temporadas = temporadas;
    }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getNumVotantes() {
        return numVotantes;
    }

    public void setNumVotantes(int numVotantes) {
        this.numVotantes = numVotantes;
    }

    public double getTuVoto() {
        return tuVoto;
    }

    public void setTuVoto(double tuVoto) {
        this.tuVoto = tuVoto;
    }

    public int getCuantosAmigosHanVotadoLaSerie() {
        return cuantosAmigosHanVotadoLaSerie;
    }

    public List<MComentarioAmigoEnFichaCapituloSerie> getComentariosAmigos() {
        return comentariosAmigos;
    }

    public void setComentariosAmigos(List<MComentarioAmigoEnFichaCapituloSerie> comentariosAmigos) {
        this.comentariosAmigos = comentariosAmigos;
    }

    public void setCuantosAmigosHanVotadoLaSerie(int cuantosAmigosHanVotadoLaSerie) {
        this.cuantosAmigosHanVotadoLaSerie = cuantosAmigosHanVotadoLaSerie;
    }

    public List<MVotacionAmigoEnFichaSerie> getVotacionesAmigos() {
        return votacionesAmigos;
    }

    public void setVotacionesAmigos(List<MVotacionAmigoEnFichaSerie> votacionesAmigos) {
        this.votacionesAmigos = votacionesAmigos;
    }

    public int getCuantosAmigosHanComentadoLaSerie() {
        return cuantosAmigosHanComentadoLaSerie;
    }

    public void setCuantosAmigosHanComentadoLaSerie(int cuantosAmigosHanComentadoLaSerie) {
        this.cuantosAmigosHanComentadoLaSerie = cuantosAmigosHanComentadoLaSerie;
    }

    private List<String> obtenerDistribuidoras(String distribuidoras){
        try{
            return Arrays.asList(distribuidoras.split("\\."));
        }catch (Exception e){
            return new ArrayList<>();
        }

    }
}
