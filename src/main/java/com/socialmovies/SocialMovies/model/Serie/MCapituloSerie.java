package com.socialmovies.SocialMovies.model.Serie;

import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;

import java.util.Date;

public class MCapituloSerie {

    private long id;
    private String titulo_capitulo;
    private int numero_capitulo;
    private Date fecha_incluido;
    private int numVotaciones;
    private double votacionMedia;
    private int tuVoto;
    private Date fechatuVoto;
    private String miComentario;

    public MCapituloSerie(){}

    public MCapituloSerie(CapituloSerie cs, int numVotaciones, double votacionMedia, int tuVoto, Date fechatuVoto, String miComentario){
        this.id = cs.getId();
        this.titulo_capitulo = cs.getTituloCapitulo();
        this.numero_capitulo = cs.getNumeroCapitulo();
        this.fecha_incluido = cs.getFechaIncluido();
        this.numVotaciones = numVotaciones;
        this.votacionMedia = votacionMedia;
        this.tuVoto = tuVoto;
        this.fechatuVoto = fechatuVoto;
        this.miComentario = miComentario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo_capitulo() {
        return titulo_capitulo;
    }

    public void setTitulo_capitulo(String titulo_capitulo) {
        this.titulo_capitulo = titulo_capitulo;
    }

    public int getNumero_capitulo() {
        return numero_capitulo;
    }

    public void setNumero_capitulo(int numero_capitulo) {
        this.numero_capitulo = numero_capitulo;
    }

    public Date getFecha_incluido() {
        return fecha_incluido;
    }

    public void setFecha_incluido(Date fecha_incluido) {
        this.fecha_incluido = fecha_incluido;
    }

    public int getNumVotaciones() {
        return numVotaciones;
    }

    public void setNumVotaciones(int numVotaciones) {
        this.numVotaciones = numVotaciones;
    }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getTuVoto() {
        return tuVoto;
    }

    public void setTuVoto(int tuVoto) {
        this.tuVoto = tuVoto;
    }

    public Date getFechatuVoto() {
        return fechatuVoto;
    }

    public void setFechatuVoto(Date fechatuVoto) {
        this.fechatuVoto = fechatuVoto;
    }

    public String getMiComentario() {
        return miComentario;
    }

    public void setMiComentario(String miComentario) {
        this.miComentario = miComentario;
    }
}
