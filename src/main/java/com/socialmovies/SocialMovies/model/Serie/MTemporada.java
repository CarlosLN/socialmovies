package com.socialmovies.SocialMovies.model.Serie;

import java.util.List;

public class MTemporada {

    private int numTemporada;
    private int numVotaciones;
    private double votacionMedia;
    private int cuantosCapitulosEnLaTemporada;
    private List<MCapituloSerie> capitulosTemporada;

    public MTemporada(){}

    /*
    public MTemporada(int numTemporada,int cuantosCapitulosEnLaTemporada){
        this.numTemporada = numTemporada;
        this.cuantosCapitulosEnLaTemporada = cuantosCapitulosEnLaTemporada;
    }
    */

    public MTemporada(int numTemporada, int numVotaciones, double votacionMedia, int cuantosCapitulosEnLaTemporada, List<MCapituloSerie> capitulosTemporada) {
        this.numTemporada = numTemporada;
        this.numVotaciones = numVotaciones;
        this.votacionMedia = votacionMedia;
        this.cuantosCapitulosEnLaTemporada = cuantosCapitulosEnLaTemporada;
        this.capitulosTemporada = capitulosTemporada;
    }

    //public MTemporada(int numTemporada, int cuantosCapitulosEnLaTemporada, List<MCapituloSerie> capitulosTemporada){
    //    this.numTemporada = numTemporada;
    //    this.cuantosCapitulosEnLaTemporada = cuantosCapitulosEnLaTemporada;
    //    this.capitulosTemporada = capitulosTemporada;
    //}

    public int getNumTemporada() {
        return numTemporada;
    }

    public void setNumTemporada(int numTemporada) {
        this.numTemporada = numTemporada;
    }

    public int getNumVotaciones() {
        return numVotaciones;
    }

    public void setNumVotaciones(int numVotaciones) {
        this.numVotaciones = numVotaciones;
    }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getCuantosCapitulosEnLaTemporada() {
        return cuantosCapitulosEnLaTemporada;
    }

    public void setCuantosCapitulosEnLaTemporada(int cuantosCapitulosEnLaTemporada) {
        this.cuantosCapitulosEnLaTemporada = cuantosCapitulosEnLaTemporada;
    }

    public List<MCapituloSerie> getCapitulosTemporada() {
        return capitulosTemporada;
    }

    public void setCapitulosTemporada(List<MCapituloSerie> capitulosTemporada) {
        this.capitulosTemporada = capitulosTemporada;
    }
}
