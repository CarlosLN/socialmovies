package com.socialmovies.SocialMovies.model.Serie;

import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;

import java.util.Date;
import java.util.List;

public class MCrearSerie {

    private String titulo;
    private String sinopsis;
    private Date fecha_estreno;
    private String direccion;
    private String reparto;
    private String poster;
    private String duracion;
    private String trailer;
    private String genero;
    private String distribuidoras;
    private Date fecha_incluida;
    private boolean activa;
    private List<CapituloSerie> capituloSerieList;

    public MCrearSerie() {
    }

    public MCrearSerie(Serie serie, List<CapituloSerie> capituloSerieList1) {
        this.titulo = serie.getTitulo();
        this.sinopsis = serie.getSinopsis();
        this.fecha_estreno = serie.getFecha_estreno();
        this.direccion = serie.getDireccion();
        this.reparto = serie.getReparto();
        this.poster = serie.getPoster();
        this.duracion = serie.getDuracion();
        this.trailer = serie.getTrailer();
        this.genero = serie.getGenero();
        this.distribuidoras = serie.getDistribuidoras();
        this.fecha_incluida = serie.getFecha_incluida();
        this.activa = serie.isActiva();
        this.capituloSerieList = capituloSerieList1;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Date getFecha_estreno() {
        return fecha_estreno;
    }

    public void setFecha_estreno(Date fecha_estreno) {
        this.fecha_estreno = fecha_estreno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getReparto() {
        return reparto;
    }

    public void setReparto(String reparto) {
        this.reparto = reparto;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDistribuidoras() {
        return distribuidoras;
    }

    public void setDistribuidoras(String distribuidoras) {
        this.distribuidoras = distribuidoras;
    }

    public Date getFecha_incluida() {
        return fecha_incluida;
    }

    public void setFecha_incluida(Date fecha_incluida) {
        this.fecha_incluida = fecha_incluida;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public List<CapituloSerie> getCapituloSerieList() {
        return capituloSerieList;
    }

    public void setCapituloSerieList(List<CapituloSerie> capituloSerieList) {
        this.capituloSerieList = capituloSerieList;
    }

    public Serie devolverDatosSerie() {
        return new Serie(
                this.titulo,
                this.sinopsis,
                this.fecha_estreno,
                this.direccion,
                this.reparto,
                this.poster,
                this.duracion,
                this.trailer,
                this.genero,
                this.distribuidoras,
                this.fecha_incluida,
                this.activa
        );
    }
}
