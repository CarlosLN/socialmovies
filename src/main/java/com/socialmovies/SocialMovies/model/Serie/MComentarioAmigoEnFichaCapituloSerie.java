package com.socialmovies.SocialMovies.model.Serie;

import java.util.Date;

public class MComentarioAmigoEnFichaCapituloSerie {

    private String username;
    private String avatar;
    private String comentario;
    private Date fechaComentario;

    private int numTemporada;
    private int numCapitulo;
    private String tituloCapitulo;

    public MComentarioAmigoEnFichaCapituloSerie(){}

    public MComentarioAmigoEnFichaCapituloSerie(String username, String avatar, String comentario, Date fechaComentario, int numTemporada, int numCapitulo, String tituloCapitulo) {
        this.username = username;
        this.avatar = avatar;
        this.comentario = comentario;
        this.fechaComentario = fechaComentario;
        this.numTemporada = numTemporada;
        this.numCapitulo = numCapitulo;
        this.tituloCapitulo = tituloCapitulo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(Date fechaComentario) {
        this.fechaComentario = fechaComentario;
    }

    public int getNumTemporada() {
        return numTemporada;
    }

    public void setNumTemporada(int numTemporada) {
        this.numTemporada = numTemporada;
    }

    public int getNumCapitulo() {
        return numCapitulo;
    }

    public void setNumCapitulo(int numCapitulo) {
        this.numCapitulo = numCapitulo;
    }

    public String getTituloCapitulo() {
        return tituloCapitulo;
    }

    public void setTituloCapitulo(String tituloCapitulo) {
        this.tituloCapitulo = tituloCapitulo;
    }
}
