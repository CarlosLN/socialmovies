package com.socialmovies.SocialMovies.model.Pelicula;

import com.socialmovies.SocialMovies.entity.Pelicula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MPelicula {

    private long id;

    private String titulo;
    private String sinopsis;
    private Date fechaEstreno;
    private String direccion;
    private String reparto;
    private String poster;
    private String duracion;
    private String trailer;
    private String genero;
    private List<String> distribuidoras;
    private double votacionMedia;
    private int numVotantes;
    private double tuVoto;
    private Date fechatuVoto;
    private List<MVotacionAmigoEnFichaPelicula> votacionesAmigos;
    private int cuantosAmigosHanVotadoLaPelicula;
    private List<MComentarioAmigoEnFichaPelicula> comentariosAmigos;
    private String miComentario;
    private int cuantosAmigosHanComentadoLaPelicula;

    public MPelicula(){}

    public MPelicula(Pelicula p, double votacionMedia, int numVotantes, double tuVoto, Date fechatuVoto,List<MVotacionAmigoEnFichaPelicula> votacionesAmigos, int cuantosAmigosHanVotadoLaPelicula, List<MComentarioAmigoEnFichaPelicula> comentariosAmigos, String miComentario, int cuantosAmigosHanComentadoLaPelicula) {
        this.id = p.getId();
        this.titulo = p.getTitulo();
        this.sinopsis = p.getSinopsis();
        this.fechaEstreno = p.getFecha_estreno();
        this.direccion = p.getDireccion();
        this.reparto = p.getReparto();
        this.poster = p.getPoster();
        this.duracion = p.getDuracion();
        this.trailer = p.getTrailer();
        this.genero = p.getGenero();
        this.distribuidoras = this.obtenerDistribuidoras(p.getDistribuidoras());
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.tuVoto = tuVoto;
        this.fechatuVoto = fechatuVoto;
        this.votacionesAmigos = votacionesAmigos;
        this.cuantosAmigosHanVotadoLaPelicula = cuantosAmigosHanVotadoLaPelicula;
        this.comentariosAmigos = comentariosAmigos;
        this.miComentario = miComentario;
        this.cuantosAmigosHanComentadoLaPelicula = cuantosAmigosHanComentadoLaPelicula;
    }

    public MPelicula(Pelicula p, double votacionMedia, int numVotantes, double tuVoto, Date fechatuVoto) {
        this.id = p.getId();
        this.titulo = p.getTitulo();
        this.sinopsis = p.getSinopsis();
        this.fechaEstreno = p.getFecha_estreno();
        this.direccion = p.getDireccion();
        this.reparto = p.getReparto();
        this.poster = p.getPoster();
        this.duracion = p.getDuracion();
        this.trailer = p.getTrailer();
        this.genero = p.getGenero();
        this.distribuidoras = this.obtenerDistribuidoras(p.getDistribuidoras());
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.tuVoto = tuVoto;
        this.fechatuVoto = fechatuVoto;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getReparto() {
        return reparto;
    }

    public void setReparto(String reparto) {
        this.reparto = reparto;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public List<String> getDistribuidoras() { return distribuidoras; }

    public void setDistribuidoras(List<String> distribuidoras) { this.distribuidoras = distribuidoras; }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getNumVotantes() {
        return numVotantes;
    }

    public void setNumVotantes(int numVotantes) {
        this.numVotantes = numVotantes;
    }

    public double getTuVoto() {
        return tuVoto;
    }

    public void setTuVoto(double tuVoto) {
        this.tuVoto = tuVoto;
    }

    public Date getFechatuVoto() {
        return fechatuVoto;
    }

    public void setFechatuVoto(Date fechatuVoto) {
        this.fechatuVoto = fechatuVoto;
    }

    public List<MVotacionAmigoEnFichaPelicula> getVotacionesAmigos() {
        return votacionesAmigos;
    }

    public void setVotacionesAmigos(List<MVotacionAmigoEnFichaPelicula> votacionesAmigos) {
        this.votacionesAmigos = votacionesAmigos;
    }

    public int getCuantosAmigosHanVotadoLaPelicula() {
        return cuantosAmigosHanVotadoLaPelicula;
    }

    public void setCuantosAmigosHanVotadoLaPelicula(int cuantosAmigosHanVotadoLaPelicula) {
        this.cuantosAmigosHanVotadoLaPelicula = cuantosAmigosHanVotadoLaPelicula;
    }

    public List<MComentarioAmigoEnFichaPelicula> getComentariosAmigos() {
        return comentariosAmigos;
    }

    public void setComentariosAmigos(List<MComentarioAmigoEnFichaPelicula> comentariosAmigos) {
        this.comentariosAmigos = comentariosAmigos;
    }

    public String getMiComentario() {
        return miComentario;
    }

    public void setMiComentario(String miComentario) {
        this.miComentario = miComentario;
    }

    public int getCuantosAmigosHanComentadoLaPelicula() {
        return cuantosAmigosHanComentadoLaPelicula;
    }

    public void setCuantosAmigosHanComentadoLaPelicula(int cuantosAmigosHanComentadoLaPelicula) {
        this.cuantosAmigosHanComentadoLaPelicula = cuantosAmigosHanComentadoLaPelicula;
    }

    private List<String> obtenerDistribuidoras(String distribuidoras){
        try{
            return Arrays.asList(distribuidoras.split("\\."));
        }catch (Exception e){
            return new ArrayList<>();
        }

    }
}
