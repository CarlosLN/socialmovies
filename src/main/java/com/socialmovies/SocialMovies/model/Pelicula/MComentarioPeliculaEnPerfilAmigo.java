package com.socialmovies.SocialMovies.model.Pelicula;

import com.socialmovies.SocialMovies.entity.Pelicula;

import java.util.Date;

public class MComentarioPeliculaEnPerfilAmigo {

    private long idPelicula;
    private String titulo;
    private Date fechaEstreno;
    private String poster;

    private String comentarioUsuario;
    private Date fechaComentario;

    public MComentarioPeliculaEnPerfilAmigo(){}

    public MComentarioPeliculaEnPerfilAmigo(Pelicula p, String comentarioUsuario, Date fechaComentario){
        this.idPelicula = p.getId();
        this.titulo = p.getTitulo();
        this.fechaEstreno = p.getFecha_estreno();
        this.poster = p.getPoster();
        this.comentarioUsuario = comentarioUsuario;
        this.fechaComentario = fechaComentario;
    }

    public long getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(long idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getComentarioUsuario() {
        return comentarioUsuario;
    }

    public void setComentarioUsuario(String comentarioUsuario) {
        this.comentarioUsuario = comentarioUsuario;
    }

    public Date getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(Date fechaComentario) {
        this.fechaComentario = fechaComentario;
    }
}
