package com.socialmovies.SocialMovies.model.Pelicula;

import java.util.Date;

public class MPeliculaRecomendadaMejorvaloradaMasvotada {

    private long idPelicula;
    private String titulo;
    private String poster;
    private Date fechaEstreno;
    private double votacionMedia;
    private int numVotantes;
    private Date fechaMiVoto;
    private double miVoto;

    public MPeliculaRecomendadaMejorvaloradaMasvotada(){}

    public MPeliculaRecomendadaMejorvaloradaMasvotada(long idPelicula, String titulo, String poster, Date fechaEstreno, double votacionMedia, int numVotantes, Date fechaMivoto, double miVoto) {
        this.idPelicula = idPelicula;
        this.titulo = titulo;
        this.poster = poster;
        this.fechaEstreno = fechaEstreno;
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.fechaMiVoto = fechaMivoto;
        this.miVoto = miVoto;
    }

    public long getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(long idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getNumVotantes() {
        return numVotantes;
    }

    public void setNumVotantes(int numVotantes) {
        this.numVotantes = numVotantes;
    }

    public Date getFechaMiVoto() {
        return fechaMiVoto;
    }

    public void setFechaMiVoto(Date fechaMiVoto) {
        this.fechaMiVoto = fechaMiVoto;
    }

    public double getMiVoto() {
        return miVoto;
    }

    public void setMiVoto(double miVoto) {
        this.miVoto = miVoto;
    }
}
