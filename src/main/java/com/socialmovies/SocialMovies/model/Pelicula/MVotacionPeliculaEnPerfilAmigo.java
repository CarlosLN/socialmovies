package com.socialmovies.SocialMovies.model.Pelicula;

import com.socialmovies.SocialMovies.entity.Pelicula;

import java.util.Date;

public class MVotacionPeliculaEnPerfilAmigo {

    private long idPelicula;
    private String titulo;
    private Date fechaEstreno;
    private String poster;

    private double votacionMedia;
    private int numVotantes;

    private double votoUsuario;
    private Date fechaVotoUsuario;

    private double tuVoto;
    private Date fechaTuVoto;

    public MVotacionPeliculaEnPerfilAmigo(){}

    public MVotacionPeliculaEnPerfilAmigo(Pelicula p, double votacionMedia, int numVotantes, double votoUsuario, Date fechaVotoUsuario, double tuVoto, Date fechaTuVoto) {
        this.idPelicula = p.getId();
        this.titulo = p.getTitulo();
        this.fechaEstreno = p.getFecha_estreno();
        this.poster = p.getPoster();
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.votoUsuario = votoUsuario;
        this.fechaVotoUsuario = fechaVotoUsuario;
        this.tuVoto = tuVoto;
        this.fechaTuVoto = fechaTuVoto;
    }

    public MVotacionPeliculaEnPerfilAmigo(Pelicula p, double votacionMedia, int numVotantes, double votoUsuario, Date fechaVotoUsuario) {
        this.idPelicula = p.getId();
        this.titulo = p.getTitulo();
        this.fechaEstreno = p.getFecha_estreno();
        this.poster = p.getPoster();
        this.votacionMedia = votacionMedia;
        this.numVotantes = numVotantes;
        this.votoUsuario = votoUsuario;
        this.fechaVotoUsuario = fechaVotoUsuario;
    }

    public long getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(long idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public double getVotacionMedia() {
        return votacionMedia;
    }

    public void setVotacionMedia(double votacionMedia) {
        this.votacionMedia = votacionMedia;
    }

    public int getNumVotantes() {
        return numVotantes;
    }

    public void setNumVotantes(int numVotantes) {
        this.numVotantes = numVotantes;
    }

    public double getVotoUsuario() {
        return votoUsuario;
    }

    public void setVotoUsuario(double votoUsuario) {
        this.votoUsuario = votoUsuario;
    }

    public Date getFechaVotoUsuario() {
        return fechaVotoUsuario;
    }

    public void setFechaVotoUsuario(Date fechaVotoUsuario) {
        this.fechaVotoUsuario = fechaVotoUsuario;
    }

    public double getTuVoto() {
        return tuVoto;
    }

    public void setTuVoto(double tuVoto) {
        this.tuVoto = tuVoto;
    }

    public Date getFechaTuVoto() {
        return fechaTuVoto;
    }

    public void setFechaTuVoto(Date fechaTuVoto) {
        this.fechaTuVoto = fechaTuVoto;
    }
}
