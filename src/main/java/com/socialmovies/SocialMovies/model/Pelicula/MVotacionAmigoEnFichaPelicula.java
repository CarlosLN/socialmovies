package com.socialmovies.SocialMovies.model.Pelicula;

import java.io.Serializable;
import java.util.Date;

public class MVotacionAmigoEnFichaPelicula implements Serializable {

    private String username;
    private String avatar;
    private double voto;
    private Date fechaVoto;

    public MVotacionAmigoEnFichaPelicula(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public double getVoto() {
        return voto;
    }

    public void setVoto(double voto) {
        this.voto = voto;
    }

    public Date getFechaVoto() {
        return fechaVoto;
    }

    public void setFechaVoto(Date fechaVoto) {
        this.fechaVoto = fechaVoto;
    }
}
