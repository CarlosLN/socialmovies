package com.socialmovies.SocialMovies.model.Pelicula;

import com.socialmovies.SocialMovies.entity.Pelicula;

import java.util.Date;

public class MPeliculaBuscada {

    private long idPelicula;
    private String titulo;
    private String poster;
    private Date fechaEstreno;

    public MPeliculaBuscada(){}

    public MPeliculaBuscada(long idPelicula, String titulo, String poster, Date fechaEstreno) {
        this.idPelicula = idPelicula;
        this.titulo = titulo;
        this.poster = poster;
        this.fechaEstreno = fechaEstreno;
    }

    public MPeliculaBuscada(Pelicula pelicula){
        this.idPelicula = pelicula.getId();
        this.titulo = pelicula.getTitulo();
        this.poster = pelicula.getPoster();
        this.fechaEstreno = pelicula.getFecha_estreno();
    }

    public long getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(long idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Date getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(Date fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

}
