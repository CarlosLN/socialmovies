package com.socialmovies.SocialMovies.model.Pelicula;

import java.io.Serializable;
import java.util.Date;

public class MComentarioAmigoEnFichaPelicula implements Serializable {

    private String username;
    private String avatar;
    private String comentario;
    private Date fechaComentario;

    public MComentarioAmigoEnFichaPelicula(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(Date fechaComentario) {
        this.fechaComentario = fechaComentario;
    }
}
