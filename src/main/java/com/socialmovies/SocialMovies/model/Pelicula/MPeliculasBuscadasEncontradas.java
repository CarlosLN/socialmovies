package com.socialmovies.SocialMovies.model.Pelicula;

import java.util.List;

public class MPeliculasBuscadasEncontradas {

    private int cuantasPeliculasEncontradas;
    private List<MPeliculaBuscada> peliculaBuscadaList;

    public MPeliculasBuscadasEncontradas(){}

    public MPeliculasBuscadasEncontradas(int cuantasPeliculasEncontradas, List<MPeliculaBuscada> peliculaBuscadaList){
        this.cuantasPeliculasEncontradas = cuantasPeliculasEncontradas;
        this.peliculaBuscadaList = peliculaBuscadaList;
    }

    public int getCuantasPeliculasEncontradas() {
        return cuantasPeliculasEncontradas;
    }

    public void setCuantasPeliculasEncontradas(int cuantasPeliculasEncontradas) {
        this.cuantasPeliculasEncontradas = cuantasPeliculasEncontradas;
    }

    public List<MPeliculaBuscada> getPeliculaBuscadaList() {
        return peliculaBuscadaList;
    }

    public void setPeliculaBuscadaList(List<MPeliculaBuscada> peliculaBuscadaList) {
        this.peliculaBuscadaList = peliculaBuscadaList;
    }
}
