package com.socialmovies.SocialMovies.model.Usuario;

import com.socialmovies.SocialMovies.entity.Usuario;

public class MUsuariosBuscados {

    private String avatar;
    private String username;
    /**
     * El tipo puede ser:
     *  - 'peticionAmistadEnviada'
     *  - 'peticionAmistadRecibida'
     *  - 'amigo'
     *  - 'usuario'
     */
    private String tipo;

    public  MUsuariosBuscados(Usuario usuario, String tipo){
        avatar = usuario.getAvatar();
        username = usuario.getUsername();
        this.tipo = tipo;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
