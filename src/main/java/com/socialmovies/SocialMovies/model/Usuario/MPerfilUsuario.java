package com.socialmovies.SocialMovies.model.Usuario;

import com.socialmovies.SocialMovies.entity.Usuario;

public class MPerfilUsuario {

    private String username;
    private String avatar;
    private int peliculasVotadas;
    private int peliculasComentadas;
    private int capitulosVotados;
    private int capitulosComentados;
    private int temporadasComentadas;
    private int seriesComentadas;
    private int puntuacionJuegos;

    /**
     * TODO AÑADIR EL RESTO DE VALORES
     *List<Votaciones>;
     *List<Comentarios>;
     */

    public MPerfilUsuario(){

    }

    public MPerfilUsuario(Usuario user){
        this.username = user.getUsername();
        this.avatar = user.getAvatar();
    }

    public MPerfilUsuario(Usuario user, int peliculasVotadas,int peliculasComentadas,
                          int capitulosVotados, int capitulosComentados, int temporadasComentadas,
                          int seriesComentadas, int puntuacionJuegos){
        this.username = user.getUsername();
        this.avatar = user.getAvatar();
        this.peliculasVotadas = peliculasVotadas;
        this.peliculasComentadas = peliculasComentadas;
        this.capitulosVotados = capitulosVotados;
        this.capitulosComentados = capitulosComentados;
        this.temporadasComentadas = temporadasComentadas;
        this.seriesComentadas = seriesComentadas;
        this.puntuacionJuegos = puntuacionJuegos;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getPeliculasVotadas() {
        return peliculasVotadas;
    }

    public void setPeliculasVotadas(int peliculasVotadas) {
        this.peliculasVotadas = peliculasVotadas;
    }

    public int getPeliculasComentadas() {
        return peliculasComentadas;
    }

    public void setPeliculasComentadas(int peliculasComentadas) {
        this.peliculasComentadas = peliculasComentadas;
    }

    public int getCapitulosVotados() {
        return capitulosVotados;
    }

    public void setCapitulosVotados(int capitulosVotados) {
        this.capitulosVotados = capitulosVotados;
    }

    public int getCapitulosComentados() {
        return capitulosComentados;
    }

    public void setCapitulosComentados(int capitulosComentados) {
        this.capitulosComentados = capitulosComentados;
    }

    public int getTemporadasComentadas() {
        return temporadasComentadas;
    }

    public void setTemporadasComentadas(int temporadasComentadas) {
        this.temporadasComentadas = temporadasComentadas;
    }

    public int getSeriesComentadas() {
        return seriesComentadas;
    }

    public void setSeriesComentadas(int seriesComentadas) {
        this.seriesComentadas = seriesComentadas;
    }

    public int getPuntuacionJuegos() {
        return puntuacionJuegos;
    }

    public void setPuntuacionJuegos(int puntuacionJuegos) {
        this.puntuacionJuegos = puntuacionJuegos;
    }
}
