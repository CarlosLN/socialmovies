package com.socialmovies.SocialMovies.model.Usuario;

public class MDatosPerfilAmigo {

    private int numPeliculasVotadas;
    private double mediaVotosPeliculas;
    private int numPeliculasComentadas;
    private int numSeriesVotadas;
    private int numCapitulosSeriesVotados;
    private double mediaVotosSeries;
    private int numSeriesComentadas;
    private int numCapitulosSeriesComentados;
    private int numAmigos;

    public MDatosPerfilAmigo(){}

    public MDatosPerfilAmigo(int numPeliculasVotadas, double mediaVotosPeliculas, int numPeliculasComentadas, int numSeriesVotadas, int numCapitulosSeriesVotados, double mediaVotosSeries, int numSeriesComentadas, int numCapitulosSeriesComentados, int numAmigos) {
        this.numPeliculasVotadas = numPeliculasVotadas;
        this.mediaVotosPeliculas = mediaVotosPeliculas;
        this.numPeliculasComentadas = numPeliculasComentadas;
        this.numSeriesVotadas = numSeriesVotadas;
        this.numCapitulosSeriesVotados = numCapitulosSeriesVotados;
        this.mediaVotosSeries = mediaVotosSeries;
        this.numSeriesComentadas = numSeriesComentadas;
        this.numCapitulosSeriesComentados = numCapitulosSeriesComentados;
        this.numAmigos = numAmigos;
    }

    public int getNumPeliculasVotadas() {
        return numPeliculasVotadas;
    }

    public void setNumPeliculasVotadas(int numPeliculasVotadas) {
        this.numPeliculasVotadas = numPeliculasVotadas;
    }

    public double getMediaVotosPeliculas() {
        return mediaVotosPeliculas;
    }

    public void setMediaVotosPeliculas(double mediaVotosPeliculas) {
        this.mediaVotosPeliculas = mediaVotosPeliculas;
    }

    public int getNumPeliculasComentadas() {
        return numPeliculasComentadas;
    }

    public void setNumPeliculasComentadas(int numPeliculasComentadas) {
        this.numPeliculasComentadas = numPeliculasComentadas;
    }

    public int getNumSeriesVotadas() {
        return numSeriesVotadas;
    }

    public void setNumSeriesVotadas(int numSeriesVotadas) {
        this.numSeriesVotadas = numSeriesVotadas;
    }

    public int getNumCapitulosSeriesVotados() {
        return numCapitulosSeriesVotados;
    }

    public void setNumCapitulosSeriesVotados(int numCapitulosSeriesVotados) {
        this.numCapitulosSeriesVotados = numCapitulosSeriesVotados;
    }

    public double getMediaVotosSeries() {
        return mediaVotosSeries;
    }

    public void setMediaVotosSeries(double mediaVotosSeries) {
        this.mediaVotosSeries = mediaVotosSeries;
    }

    public int getNumSeriesComentadas() {
        return numSeriesComentadas;
    }

    public void setNumSeriesComentadas(int numSeriesComentadas) {
        this.numSeriesComentadas = numSeriesComentadas;
    }

    public int getNumCapitulosSeriesComentados() {
        return numCapitulosSeriesComentados;
    }

    public void setNumCapitulosSeriesComentados(int numCapitulosSeriesComentados) {
        this.numCapitulosSeriesComentados = numCapitulosSeriesComentados;
    }

    public int getNumAmigos() {
        return numAmigos;
    }

    public void setNumAmigos(int numAmigos) {
        this.numAmigos = numAmigos;
    }


}
