package com.socialmovies.SocialMovies.model.Usuario;

import com.socialmovies.SocialMovies.entity.Usuario;

public class MUsuarioAvatar {

    private String username;
    private String avatar;

    public MUsuarioAvatar(){

    }

    public MUsuarioAvatar(Usuario usuario){
        this.username = usuario.getUsername();
        this.avatar = usuario.getAvatar();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
