package com.socialmovies.SocialMovies.model.Usuario;

import java.sql.Blob;

import com.socialmovies.SocialMovies.entity.Usuario;
import com.sun.xml.internal.ws.api.ServiceSharedFeatureMarker;

public class MUsuario {

	/*CONSTRUCTORES*/
	public MUsuario() {
		
	}
	
	public MUsuario(boolean activo, String email, String username, String password, String avatar) {
		this.email = email; 
		this.username = username;
		this.password = password;
		this.avatar = avatar;
	}
	
	public MUsuario(Usuario user) {
		this.email = user.getEmail(); 
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.avatar = user.getAvatar();
	}
	
	/*ATRIBUTOS*/
	private String avatar;
	private String email;
	private String password;
	private String username;
	
	/*GET & SET*/
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
}
