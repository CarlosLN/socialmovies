package com.socialmovies.SocialMovies.model.TimeLine;

import java.util.Date;
import java.util.List;

public class MTimeLine {

    /*private Date primerElementomiVotoPelicula;
    private Date primerElementomiVotoCapituloSerie;
    private Date primerElementomiComentarioPelicula;
    private Date primerElementomiComentarioCapituloSerie;
    private Date primerElementoamigosVotoPelicula;
    private Date primerElementoamigosVotoCapituloSerie;
    private Date primerElementoamigosComentarioPelicula;
    private Date primerElementoamigosComentarioCapituloSerie;
    private Date primerElementonuevaPeliculaDisponible;
    private Date primerElementonuevoCapituloDisponible;
    private Date primerElementomiParticipacionJuego;
    private Date primerElementoamigosParticipacionJuegos;*/

    private Date fechaPrimerElemento;
    private List<MTimeLineElement> elementList;
    private Date fechaUltimoElemento;

    /*private Date ultimoElementomiVotoPelicula;
    private Date ultimoElementomiVotoCapituloSerie;
    private Date ultimoElementomiComentarioPelicula;
    private Date ultimoElementomiComentarioCapituloSerie;
    private Date ultimoElementoamigosVotoPelicula;
    private Date ultimoElementoamigosVotoCapituloSerie;
    private Date ultimoElementoamigosComentarioPelicula;
    private Date ultimoElementoamigosComentarioCapituloSerie;
    private Date ultimoElementonuevaPeliculaDisponible;
    private Date ultimoElementonuevoCapituloDisponible;
    private Date ultimoElementomiParticipacionJuego;
    private Date ultimoElementoamigosParticipacionJuegos;*/

    public MTimeLine() {
    }

    public List<MTimeLineElement> getElementList() {
        return elementList;
    }

    public void setElementList(List<MTimeLineElement> elementList) {
        this.elementList = elementList;
    }

    public Date getFechaPrimerElemento() {
        return fechaPrimerElemento;
    }

    public void setFechaPrimerElemento(Date fechaPrimerElemento) {
        this.fechaPrimerElemento = fechaPrimerElemento;
    }

    public Date getFechaUltimoElemento() {
        return fechaUltimoElemento;
    }

    public void setFechaUltimoElemento(Date fechaUltimoElemento) {
        this.fechaUltimoElemento = fechaUltimoElemento;
    }

    /*public Date getPrimerElementomiVotoPelicula() {
        return primerElementomiVotoPelicula;
    }

    public void setPrimerElementomiVotoPelicula(Date primerElementomiVotoPelicula) {
        this.primerElementomiVotoPelicula = primerElementomiVotoPelicula;
    }

    public Date getPrimerElementomiVotoCapituloSerie() {
        return primerElementomiVotoCapituloSerie;
    }

    public void setPrimerElementomiVotoCapituloSerie(Date primerElementomiVotoCapituloSerie) {
        this.primerElementomiVotoCapituloSerie = primerElementomiVotoCapituloSerie;
    }

    public Date getPrimerElementomiComentarioPelicula() {
        return primerElementomiComentarioPelicula;
    }

    public void setPrimerElementomiComentarioPelicula(Date primerElementomiComentarioPelicula) {
        this.primerElementomiComentarioPelicula = primerElementomiComentarioPelicula;
    }

    public Date getPrimerElementomiComentarioCapituloSerie() {
        return primerElementomiComentarioCapituloSerie;
    }

    public void setPrimerElementomiComentarioCapituloSerie(Date primerElementomiComentarioCapituloSerie) {
        this.primerElementomiComentarioCapituloSerie = primerElementomiComentarioCapituloSerie;
    }

    public Date getPrimerElementoamigosVotoPelicula() {
        return primerElementoamigosVotoPelicula;
    }

    public void setPrimerElementoamigosVotoPelicula(Date primerElementoamigosVotoPelicula) {
        this.primerElementoamigosVotoPelicula = primerElementoamigosVotoPelicula;
    }

    public Date getPrimerElementoamigosVotoCapituloSerie() {
        return primerElementoamigosVotoCapituloSerie;
    }

    public void setPrimerElementoamigosVotoCapituloSerie(Date primerElementoamigosVotoCapituloSerie) {
        this.primerElementoamigosVotoCapituloSerie = primerElementoamigosVotoCapituloSerie;
    }

    public Date getPrimerElementoamigosComentarioPelicula() {
        return primerElementoamigosComentarioPelicula;
    }

    public void setPrimerElementoamigosComentarioPelicula(Date primerElementoamigosComentarioPelicula) {
        this.primerElementoamigosComentarioPelicula = primerElementoamigosComentarioPelicula;
    }

    public Date getPrimerElementoamigosComentarioCapituloSerie() {
        return primerElementoamigosComentarioCapituloSerie;
    }

    public void setPrimerElementoamigosComentarioCapituloSerie(Date primerElementoamigosComentarioCapituloSerie) {
        this.primerElementoamigosComentarioCapituloSerie = primerElementoamigosComentarioCapituloSerie;
    }

    public Date getPrimerElementonuevaPeliculaDisponible() {
        return primerElementonuevaPeliculaDisponible;
    }

    public void setPrimerElementonuevaPeliculaDisponible(Date primerElementonuevaPeliculaDisponible) {
        this.primerElementonuevaPeliculaDisponible = primerElementonuevaPeliculaDisponible;
    }

    public Date getPrimerElementonuevoCapituloDisponible() {
        return primerElementonuevoCapituloDisponible;
    }

    public void setPrimerElementonuevoCapituloDisponible(Date primerElementonuevoCapituloDisponible) {
        this.primerElementonuevoCapituloDisponible = primerElementonuevoCapituloDisponible;
    }

    public Date getPrimerElementomiParticipacionJuego() {
        return primerElementomiParticipacionJuego;
    }

    public void setPrimerElementomiParticipacionJuego(Date primerElementomiParticipacionJuego) {
        this.primerElementomiParticipacionJuego = primerElementomiParticipacionJuego;
    }

    public Date getPrimerElementoamigosParticipacionJuegos() {
        return primerElementoamigosParticipacionJuegos;
    }

    public void setPrimerElementoamigosParticipacionJuegos(Date primerElementoamigosParticipacionJuegos) {
        this.primerElementoamigosParticipacionJuegos = primerElementoamigosParticipacionJuegos;
    }

    public Date getUltimoElementomiVotoPelicula() {
        return ultimoElementomiVotoPelicula;
    }

    public void setUltimoElementomiVotoPelicula(Date ultimoElementomiVotoPelicula) {
        this.ultimoElementomiVotoPelicula = ultimoElementomiVotoPelicula;
    }

    public Date getUltimoElementomiVotoCapituloSerie() {
        return ultimoElementomiVotoCapituloSerie;
    }

    public void setUltimoElementomiVotoCapituloSerie(Date ultimoElementomiVotoCapituloSerie) {
        this.ultimoElementomiVotoCapituloSerie = ultimoElementomiVotoCapituloSerie;
    }

    public Date getUltimoElementomiComentarioPelicula() {
        return ultimoElementomiComentarioPelicula;
    }

    public void setUltimoElementomiComentarioPelicula(Date ultimoElementomiComentarioPelicula) {
        this.ultimoElementomiComentarioPelicula = ultimoElementomiComentarioPelicula;
    }

    public Date getUltimoElementomiComentarioCapituloSerie() {
        return ultimoElementomiComentarioCapituloSerie;
    }

    public void setUltimoElementomiComentarioCapituloSerie(Date ultimoElementomiComentarioCapituloSerie) {
        this.ultimoElementomiComentarioCapituloSerie = ultimoElementomiComentarioCapituloSerie;
    }

    public Date getUltimoElementoamigosVotoPelicula() {
        return ultimoElementoamigosVotoPelicula;
    }

    public void setUltimoElementoamigosVotoPelicula(Date ultimoElementoamigosVotoPelicula) {
        this.ultimoElementoamigosVotoPelicula = ultimoElementoamigosVotoPelicula;
    }

    public Date getUltimoElementoamigosVotoCapituloSerie() {
        return ultimoElementoamigosVotoCapituloSerie;
    }

    public void setUltimoElementoamigosVotoCapituloSerie(Date ultimoElementoamigosVotoCapituloSerie) {
        this.ultimoElementoamigosVotoCapituloSerie = ultimoElementoamigosVotoCapituloSerie;
    }

    public Date getUltimoElementoamigosComentarioPelicula() {
        return ultimoElementoamigosComentarioPelicula;
    }

    public void setUltimoElementoamigosComentarioPelicula(Date ultimoElementoamigosComentarioPelicula) {
        this.ultimoElementoamigosComentarioPelicula = ultimoElementoamigosComentarioPelicula;
    }

    public Date getUltimoElementoamigosComentarioCapituloSerie() {
        return ultimoElementoamigosComentarioCapituloSerie;
    }

    public void setUltimoElementoamigosComentarioCapituloSerie(Date ultimoElementoamigosComentarioCapituloSerie) {
        this.ultimoElementoamigosComentarioCapituloSerie = ultimoElementoamigosComentarioCapituloSerie;
    }

    public Date getUltimoElementonuevaPeliculaDisponible() {
        return ultimoElementonuevaPeliculaDisponible;
    }

    public void setUltimoElementonuevaPeliculaDisponible(Date ultimoElementonuevaPeliculaDisponible) {
        this.ultimoElementonuevaPeliculaDisponible = ultimoElementonuevaPeliculaDisponible;
    }

    public Date getUltimoElementonuevoCapituloDisponible() {
        return ultimoElementonuevoCapituloDisponible;
    }

    public void setUltimoElementonuevoCapituloDisponible(Date ultimoElementonuevoCapituloDisponible) {
        this.ultimoElementonuevoCapituloDisponible = ultimoElementonuevoCapituloDisponible;
    }

    public Date getUltimoElementomiParticipacionJuego() {
        return ultimoElementomiParticipacionJuego;
    }

    public void setUltimoElementomiParticipacionJuego(Date ultimoElementomiParticipacionJuego) {
        this.ultimoElementomiParticipacionJuego = ultimoElementomiParticipacionJuego;
    }

    public Date getUltimoElementoamigosParticipacionJuegos() {
        return ultimoElementoamigosParticipacionJuegos;
    }

    public void setUltimoElementoamigosParticipacionJuegos(Date ultimoElementoamigosParticipacionJuegos) {
        this.ultimoElementoamigosParticipacionJuegos = ultimoElementoamigosParticipacionJuegos;
    }*/
}
