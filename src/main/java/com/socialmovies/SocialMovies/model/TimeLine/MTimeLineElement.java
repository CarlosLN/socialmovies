package com.socialmovies.SocialMovies.model.TimeLine;

import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioCapituloSerie;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioPelicula;
import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionCapituloSerie;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionPelicula;
import com.socialmovies.SocialMovies.model.Juego.MParticipacionUsuarioEnJuegoTimeLineElement;

import java.util.Date;

public class MTimeLineElement implements Comparable<MTimeLineElement>{

    /**
     *
     *  Pueden ser de los siguientes tipos
     *
     *  miVotoPelicula
     * 	miVotoCapituloSerie
     * 	miComentarioPelicula
     * 	miComentarioCapituloSerie
     *  miParticipacionJuego
     * 	amigosVotoPelicula
     * 	amigosVotoCapituloSerie
     * 	amigosComentarioPelicula
     * 	amigosComentarioCapituloSerie
     *  amigosParticipacionJuegos
     *  nuevaPeliculaDisponible
     * 	nuevoCapituloDisponible
     *  nuevoJuegoDisponible
     *
     */
    private String tipoElemento;
    private long idUsuario;
    private String nombreUsuario;
    private long idSeriePelicula;
    private String tituloSeriePelicula;
    private String imagenUser;
    private String poster;
    private Date fecha;
    private String comentario;
    private String comentarioCapituloPelicula;
    private double votoCapituloSeriePelicula;

    public MTimeLineElement(VotacionPelicula votacionPelicula, String usuario){
        this.tipoElemento = usuario.equals("tu") ? "miVotoPelicula" : "amigosVotoPelicula";
        this.idUsuario = votacionPelicula.getIdUsuario().getId();
        this.nombreUsuario = votacionPelicula.getIdUsuario().getUsername();
        this.idSeriePelicula = votacionPelicula.getIdPelicula().getId();
        this.tituloSeriePelicula = votacionPelicula.getIdPelicula().getTitulo();
        this.imagenUser = votacionPelicula.getIdUsuario().getAvatar();
        this.poster = votacionPelicula.getIdPelicula().getPoster();
        this.fecha = votacionPelicula.getFecha_votacion();
        this.votoCapituloSeriePelicula = votacionPelicula.getVotacion();
    }

    public MTimeLineElement(VotacionCapituloSerie votacionCapituloSerie, String usuario){
        this.tipoElemento = usuario.equals("tu") ? "miVotoCapituloSerie" : "amigosVotoCapituloSerie";
        this.idUsuario = votacionCapituloSerie.getIdUsuario().getId();
        this.nombreUsuario = votacionCapituloSerie.getIdUsuario().getUsername();
        this.idSeriePelicula = votacionCapituloSerie.getIdCapituloSerie().getIdSerie().getId();
        this.tituloSeriePelicula = votacionCapituloSerie.getIdCapituloSerie().getIdSerie().getTitulo();
        this.imagenUser = votacionCapituloSerie.getIdUsuario().getAvatar();
        this.poster = votacionCapituloSerie.getIdCapituloSerie().getIdSerie().getPoster();
        this.fecha = votacionCapituloSerie.getFechaVotacion();
        this.comentario = "";
        if(votacionCapituloSerie.getIdCapituloSerie().getNumeroTemporada()<10){
            this.comentario += "0"+votacionCapituloSerie.getIdCapituloSerie().getNumeroTemporada();
        }else{
            this.comentario += votacionCapituloSerie.getIdCapituloSerie().getNumeroTemporada();
        }
        if(votacionCapituloSerie.getIdCapituloSerie().getNumeroCapitulo()<10){
            this.comentario += "x0"+votacionCapituloSerie.getIdCapituloSerie().getNumeroCapitulo();
        }else{
            this.comentario += "x" +votacionCapituloSerie.getIdCapituloSerie().getNumeroCapitulo();
        }
        this.comentario += " " + votacionCapituloSerie.getIdCapituloSerie().getTituloCapitulo();
        this.votoCapituloSeriePelicula = votacionCapituloSerie.getVotacion();
    }

    public MTimeLineElement(ComentarioPelicula comentarioPelicula, String usuario){
        this.tipoElemento = usuario.equals("tu") ? "miComentarioPelicula" : "amigosComentarioPelicula";
        this.idUsuario = comentarioPelicula.getIdUsuario().getId();
        this.nombreUsuario = comentarioPelicula.getIdUsuario().getUsername();
        this.idSeriePelicula = comentarioPelicula.getIdPelicula().getId();
        this.tituloSeriePelicula = comentarioPelicula.getIdPelicula().getTitulo();
        this.imagenUser = comentarioPelicula.getIdUsuario().getAvatar();
        this.poster = comentarioPelicula.getIdPelicula().getPoster();
        this.fecha = comentarioPelicula.getFechaComentario();
        this.comentarioCapituloPelicula = comentarioPelicula.getComentarioPelicula();
    }

    public MTimeLineElement(ComentarioCapituloSerie comentarioCapituloSerie, String usuario){
        this.tipoElemento = usuario.equals("tu") ? "miComentarioCapituloSerie" : "amigosComentarioCapituloSerie";
        this.idUsuario = comentarioCapituloSerie.getIdUsuario().getId();
        this.nombreUsuario = comentarioCapituloSerie.getIdUsuario().getUsername();
        this.idSeriePelicula = comentarioCapituloSerie.getIdCapituloSerie().getIdSerie().getId();
        this.tituloSeriePelicula = comentarioCapituloSerie.getIdCapituloSerie().getIdSerie().getTitulo();
        this.imagenUser = comentarioCapituloSerie.getIdUsuario().getAvatar();
        this.poster = comentarioCapituloSerie.getIdCapituloSerie().getIdSerie().getPoster();
        this.fecha = comentarioCapituloSerie.getFechaComentario();
        this.comentario = "";
        if(comentarioCapituloSerie.getIdCapituloSerie().getNumeroTemporada()<10){
            this.comentario += "0"+comentarioCapituloSerie.getIdCapituloSerie().getNumeroTemporada();
        }else{
            this.comentario += comentarioCapituloSerie.getIdCapituloSerie().getNumeroTemporada();
        }
        if(comentarioCapituloSerie.getIdCapituloSerie().getNumeroCapitulo()<10){
            this.comentario += "x0"+comentarioCapituloSerie.getIdCapituloSerie().getNumeroCapitulo();
        }else{
            this.comentario += "x" +comentarioCapituloSerie.getIdCapituloSerie().getNumeroCapitulo();
        }
        this.comentario += " " + comentarioCapituloSerie.getIdCapituloSerie().getTituloCapitulo();
        this.comentarioCapituloPelicula = comentarioCapituloSerie.getComentarioCapituloSerie();
    }

    public MTimeLineElement(MParticipacionUsuarioEnJuegoTimeLineElement respuestasJuego, String usuario){
        this.tipoElemento = usuario.equals("tu") ? "miParticipacionJuego" : "amigosParticipacionJuegos";
        this.idUsuario = respuestasJuego.getUsuario().getId();
        this.nombreUsuario = respuestasJuego.getUsuario().getUsername();
        this.idSeriePelicula = respuestasJuego.getJuegoAdivinarPeliculaSerie().getIdJuego();
        this.tituloSeriePelicula = respuestasJuego.getJuegoAdivinarPeliculaSerie().getTitulo();
        this.imagenUser = respuestasJuego.getUsuario().getAvatar();
        this.fecha = respuestasJuego.getFechaParticipacion();
        this.comentarioCapituloPelicula = "" + respuestasJuego.getPuntuacionObtenida();
    }

    public MTimeLineElement(Pelicula pelicula){
        this.tipoElemento = "nuevaPeliculaDisponible";
        this.idSeriePelicula = pelicula.getId();
        this.poster = pelicula.getPoster();
        this.fecha = pelicula.getFecha_incluida();
        this.tituloSeriePelicula = pelicula.getTitulo();
    }

    public MTimeLineElement(CapituloSerie capituloSerie){
        this.tipoElemento = "nuevoCapituloDisponible";
        this.idSeriePelicula = capituloSerie.getIdSerie().getId();
        this.poster = capituloSerie.getIdSerie().getPoster();
        this.fecha = capituloSerie.getFechaIncluido();
        this.comentario = "";
        if(capituloSerie.getNumeroTemporada()<10){
            this.comentario += "0"+capituloSerie.getNumeroTemporada();
        }else{
            this.comentario += capituloSerie.getNumeroTemporada();
        }
        if(capituloSerie.getNumeroCapitulo()<10){
            this.comentario += "x0"+capituloSerie.getNumeroCapitulo();
        }else{
            this.comentario += "x" +capituloSerie.getNumeroCapitulo();
        }
        this.comentario += " " +capituloSerie.getTituloCapitulo();
        this.tituloSeriePelicula = capituloSerie.getIdSerie().getTitulo();
    }

    public MTimeLineElement(JuegoAdivinarPeliculaSerie juegoAdivinarPeliculaSerie){
        this.tipoElemento = "nuevoJuegoDisponible";
        this.idSeriePelicula = juegoAdivinarPeliculaSerie.getIdJuego();
        this.fecha = juegoAdivinarPeliculaSerie.getFecha_creacion();
        this.tituloSeriePelicula = juegoAdivinarPeliculaSerie.getTitulo();
        this.comentario = juegoAdivinarPeliculaSerie.getDificultad();
    }


    public String getTipoElemento() {
        return tipoElemento;
    }

    public void setTipoElemento(String tipoElemento) {
        this.tipoElemento = tipoElemento;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdSeriePelicula() {
        return idSeriePelicula;
    }

    public void setIdSeriePelicula(long idSeriePelicula) {
        this.idSeriePelicula = idSeriePelicula;
    }

    public String getTituloSeriePelicula() {
        return tituloSeriePelicula;
    }

    public void setTituloSeriePelicula(String tituloSeriePelicula) {
        this.tituloSeriePelicula = tituloSeriePelicula;
    }

    public String getImagenUser() {
        return imagenUser;
    }

    public void setImagenUser(String imagenUser) {
        this.imagenUser = imagenUser;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getComentarioCapituloPelicula() {
        return comentarioCapituloPelicula;
    }

    public void setComentarioCapituloPelicula(String comentarioCapituloPelicula) {
        this.comentarioCapituloPelicula = comentarioCapituloPelicula;
    }

    public double getVotoCapituloSeriePelicula() {
        return votoCapituloSeriePelicula;
    }

    public void setVotoCapituloSeriePelicula(double votoCapituloSeriePelicula) {
        this.votoCapituloSeriePelicula = votoCapituloSeriePelicula;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Override
    public int compareTo(MTimeLineElement o) {
        return getFecha().compareTo(o.getFecha());
    }
}
