package com.socialmovies.SocialMovies.model.Juego;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Usuario;

import java.util.Date;

public class MParticipacionUsuarioEnJuegoTimeLineElement {

    private Usuario usuario;
    private JuegoAdivinarPeliculaSerie juegoAdivinarPeliculaSerie;
    private int puntuacionObtenida;
    private Date fechaParticipacion;

    public MParticipacionUsuarioEnJuegoTimeLineElement() {
    }

    public MParticipacionUsuarioEnJuegoTimeLineElement(Usuario usuario, JuegoAdivinarPeliculaSerie juegoAdivinarPeliculaSerie, int puntuacionObtenida, Date fechaParticipacion) {
        this.usuario = usuario;
        this.juegoAdivinarPeliculaSerie = juegoAdivinarPeliculaSerie;
        this.puntuacionObtenida = puntuacionObtenida;
        this.fechaParticipacion = fechaParticipacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public JuegoAdivinarPeliculaSerie getJuegoAdivinarPeliculaSerie() {
        return juegoAdivinarPeliculaSerie;
    }

    public void setJuegoAdivinarPeliculaSerie(JuegoAdivinarPeliculaSerie juegoAdivinarPeliculaSerie) {
        this.juegoAdivinarPeliculaSerie = juegoAdivinarPeliculaSerie;
    }

    public int getPuntuacionObtenida() {
        return puntuacionObtenida;
    }

    public void setPuntuacionObtenida(int puntuacionObtenida) {
        this.puntuacionObtenida = puntuacionObtenida;
    }

    public Date getFechaParticipacion() {
        return fechaParticipacion;
    }

    public void setFechaParticipacion(Date fechaParticipacion) {
        this.fechaParticipacion = fechaParticipacion;
    }
}
