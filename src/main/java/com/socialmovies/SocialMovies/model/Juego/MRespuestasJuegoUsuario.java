package com.socialmovies.SocialMovies.model.Juego;

public class MRespuestasJuegoUsuario {

    private long idPreguntaJuego;
    private int tiempoTardado;
    private String respuesta;
    private String respuestaCorrecta;

    public MRespuestasJuegoUsuario() {
    }

    public MRespuestasJuegoUsuario(long idPreguntaJuego, int tiempoTardado, String respuesta, String respuestaCorrecta) {
        this.idPreguntaJuego = idPreguntaJuego;
        this.tiempoTardado = tiempoTardado;
        this.respuesta = respuesta;
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public long getIdPreguntaJuego() {
        return idPreguntaJuego;
    }

    public void setIdPreguntaJuego(long idPreguntaJuego) {
        this.idPreguntaJuego = idPreguntaJuego;
    }

    public int getTiempoTardado() {
        return tiempoTardado;
    }

    public void setTiempoTardado(int tiempoTardado) {
        this.tiempoTardado = tiempoTardado;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }
}
