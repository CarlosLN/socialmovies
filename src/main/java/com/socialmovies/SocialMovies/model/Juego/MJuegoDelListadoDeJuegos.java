package com.socialmovies.SocialMovies.model.Juego;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;

import java.util.Date;

public class MJuegoDelListadoDeJuegos {

    private long idJuego;
    private Date fecha_creacion;
    private String titulo;
    private String dificultad; /** Puede ser facil, media o dificil **/
    private int miPuntuacion;
    private Date miFechaParticipacion;

    public MJuegoDelListadoDeJuegos(){
    }

    public MJuegoDelListadoDeJuegos(JuegoAdivinarPeliculaSerie juego){
        this.idJuego = juego.getIdJuego();
        this.fecha_creacion = juego.getFecha_creacion();
        this.titulo = juego.getTitulo();
        this.dificultad = juego.getDificultad();
    }

    public long getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(long idJuego) {
        this.idJuego = idJuego;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDificultad() {
        return dificultad;
    }

    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }

    public int getMiPuntuacion() {
        return miPuntuacion;
    }

    public void setMiPuntuacion(int miPuntuacion) {
        this.miPuntuacion = miPuntuacion;
    }

    public Date getMiFechaParticipacion() {
        return miFechaParticipacion;
    }

    public void setMiFechaParticipacion(Date miFechaParticipacion) {
        this.miFechaParticipacion = miFechaParticipacion;
    }
}
