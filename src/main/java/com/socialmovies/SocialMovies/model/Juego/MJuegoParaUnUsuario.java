package com.socialmovies.SocialMovies.model.Juego;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.PreguntasJuego;
import com.socialmovies.SocialMovies.entity.RespuestasJuego.RespuestasJuego;

import java.util.Date;
import java.util.List;

public class MJuegoParaUnUsuario {

    private MJuegoDelListadoDeJuegos juegoDelListadoDeJuegos;
    private List<PreguntasJuego> preguntasJuego;
    private List<MRespuestasJuegoUsuario> respuestasJuegos;

    public MJuegoParaUnUsuario(){}

    public MJuegoParaUnUsuario(JuegoAdivinarPeliculaSerie juego){
        this.juegoDelListadoDeJuegos = new MJuegoDelListadoDeJuegos(juego);
    }

    public MJuegoParaUnUsuario(JuegoAdivinarPeliculaSerie juego, List<PreguntasJuego> preguntasJuego){
        this.juegoDelListadoDeJuegos = new MJuegoDelListadoDeJuegos(juego);
        this.preguntasJuego = preguntasJuego;
    }

    public MJuegoParaUnUsuario(JuegoAdivinarPeliculaSerie juego, List<PreguntasJuego> preguntasJuego, List<MRespuestasJuegoUsuario> respuestasJuegos){
        this.juegoDelListadoDeJuegos = new MJuegoDelListadoDeJuegos(juego);
        this.preguntasJuego = preguntasJuego;
        this.respuestasJuegos = respuestasJuegos;
    }

    public MJuegoDelListadoDeJuegos getJuegoDelListadoDeJuegos() {
        return juegoDelListadoDeJuegos;
    }

    public void setJuegoDelListadoDeJuegos(MJuegoDelListadoDeJuegos juegoDelListadoDeJuegos) {
        this.juegoDelListadoDeJuegos = juegoDelListadoDeJuegos;
    }

    public List<PreguntasJuego> getPreguntasJuego() {
        return preguntasJuego;
    }

    public void setPreguntasJuego(List<PreguntasJuego> preguntasJuego) {
        this.preguntasJuego = preguntasJuego;
    }

    public List<MRespuestasJuegoUsuario> getRespuestasJuegos() {
        return respuestasJuegos;
    }

    public void setRespuestasJuegos(List<MRespuestasJuegoUsuario> respuestasJuegos) {
        this.respuestasJuegos = respuestasJuegos;
    }

    public void setMiPuntuacion(int puntuacion){
        this.juegoDelListadoDeJuegos.setMiPuntuacion(puntuacion);
    }

    public void setMiFechaParticipacion(Date miFechaDeParticipacion){
        this.juegoDelListadoDeJuegos.setMiFechaParticipacion(miFechaDeParticipacion);
    }
}
