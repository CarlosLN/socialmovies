package com.socialmovies.SocialMovies.model.Juego;

public class MUsuarioRankingJuego implements  Comparable<MUsuarioRankingJuego>{

    private String username;
    private int puntosTotales;

    public MUsuarioRankingJuego(){}

    public MUsuarioRankingJuego(String username, int puntosTotales){
        this.username = username;
        this.puntosTotales = puntosTotales;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPuntosTotales() {
        return puntosTotales;
    }

    public void setPuntosTotales(int puntosTotales) {
        this.puntosTotales = puntosTotales;
    }

    @Override
    public int compareTo(MUsuarioRankingJuego o) {
        return o.getPuntosTotales() - getPuntosTotales();
    }
}
