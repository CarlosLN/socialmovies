package com.socialmovies.SocialMovies.model.Juego;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.PreguntasJuego;

import java.util.Date;
import java.util.List;

public class MCrearJuego {

    private Date fecha_creacion;
    private String titulo;
    private String dificultad;
    private boolean activo;
    private List<PreguntasJuego> preguntasJuego;

    public MCrearJuego() {
    }

    public MCrearJuego(Date fecha_creacion, String titulo, String dificultad, boolean activo, List<PreguntasJuego> preguntasJuego) {
        this.fecha_creacion = fecha_creacion;
        this.titulo = titulo;
        this.dificultad = dificultad; /** Puede ser facil, media o dificil **/
        this.activo = activo;
        this.preguntasJuego = preguntasJuego;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDificultad() {
        return dificultad;
    }

    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public List<PreguntasJuego> getPreguntasJuego() {
        return preguntasJuego;
    }

    public void setPreguntasJuego(List<PreguntasJuego> preguntasJuego) {
        this.preguntasJuego = preguntasJuego;
    }

    public JuegoAdivinarPeliculaSerie devolverJuego(){
        return new JuegoAdivinarPeliculaSerie(
                this.fecha_creacion,
                this.titulo,
                this.dificultad,
                this.activo
        );
    }
}
