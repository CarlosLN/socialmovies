package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioCapituloSerie;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioPelicula;
import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.RespuestasJuego.RespuestasJuego;
import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionCapituloSerie;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionPelicula;
import com.socialmovies.SocialMovies.model.Juego.MJuegoParaUnUsuario;
import com.socialmovies.SocialMovies.model.Juego.MParticipacionUsuarioEnJuegoTimeLineElement;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLineElement;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("timeLineService")
public class TimeLineService {

    @Autowired
    private AmistadesService amistadesService;

    @Autowired
    private VotacionPeliculaService votacionPeliculaService;

    @Autowired
    private VotacionCapituloService votacionCapituloService;

    @Autowired
    private ComentarioCapituloSerieService comentarioCapituloSerieService;

    @Autowired
    private ComentarioPeliculaService comentarioPeliculaService;

    @Autowired
    private JuegoService juegoService;

    @Autowired
    private PeliculaService peliculaService;

    @Autowired
    private SerieService serieService;

    //public MTimeLine obtenerTimeLineUsuario(Usuario usuario, MTimeLine mTimeLine, String orden){
    public MTimeLine obtenerTimeLineUsuario(Usuario usuario, String cuando, MTimeLine timeLineActual) {

        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();
        idsAmigos.add(usuario.getId());
        List<MTimeLineElement> timeLineElements = new ArrayList<>();
        MTimeLine timeLine = new MTimeLine();

        for (Amistad amistad : amistades
        ) {
            if (amistad.getIdUserSolicitante().getId() == usuario.getId()) {
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            } else {
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        List<VotacionPelicula> votacionPeliculas = this.votacionPeliculaService.obtenerUltimasVotacionesTimeLine(idsAmigos, timeLineActual, cuando);
        for (VotacionPelicula votacionPelicula : votacionPeliculas
        ) {
            if (votacionPelicula.getIdUsuario().getId() == usuario.getId()) {
                timeLineElements.add(new MTimeLineElement(votacionPelicula, "tu"));
            } else {
                timeLineElements.add(new MTimeLineElement(votacionPelicula, ""));
            }
        }

        List<VotacionCapituloSerie> votacionCapituloSeries = this.votacionCapituloService.obtenerUltimasVotacionesTimeLine(idsAmigos, timeLineActual, cuando);
        for (VotacionCapituloSerie votacionCapituloSerie : votacionCapituloSeries
        ) {
            if (votacionCapituloSerie.getIdUsuario().getId() == usuario.getId()) {
                timeLineElements.add(new MTimeLineElement(votacionCapituloSerie, "tu"));
            } else {
                timeLineElements.add(new MTimeLineElement(votacionCapituloSerie, ""));
            }
        }

        List<ComentarioPelicula> comentarioPeliculas = this.comentarioPeliculaService.obtenerUltimosComentariosTimeLine(idsAmigos, timeLineActual, cuando);
        for (ComentarioPelicula comentarioPelicula : comentarioPeliculas
        ) {
            if (comentarioPelicula.getIdUsuario().getId() == usuario.getId()) {
                timeLineElements.add(new MTimeLineElement(comentarioPelicula, "tu"));
            } else {
                timeLineElements.add(new MTimeLineElement(comentarioPelicula, ""));
            }
        }

        List<ComentarioCapituloSerie> comentarioCapituloSeries = this.comentarioCapituloSerieService.obtenerUltimosComentariosTimeLine(idsAmigos, timeLineActual, cuando);
        for (ComentarioCapituloSerie comentarioCapitulo : comentarioCapituloSeries
        ) {
            if (comentarioCapitulo.getIdUsuario().getId() == usuario.getId()) {
                timeLineElements.add(new MTimeLineElement(comentarioCapitulo, "tu"));
            } else {
                timeLineElements.add(new MTimeLineElement(comentarioCapitulo, ""));
            }
        }

        List<RespuestasJuego> respuestasJuegos = this.juegoService.obtenerUltimasPaticipacionesTimeLine(idsAmigos, timeLineActual, cuando);
        for (RespuestasJuego respuestasJuego : respuestasJuegos
        ) {
            MJuegoParaUnUsuario juegoParaUnUsuario = this.juegoService.devolverUnJuegoParaUnUsuario(
                    respuestasJuego.getIdPreguntaJuego().getIdJuego(), respuestasJuego.getIdUsuario());
            if (respuestasJuego.getIdUsuario().getId() == usuario.getId()) {
                timeLineElements.add(new MTimeLineElement(
                        new MParticipacionUsuarioEnJuegoTimeLineElement(
                                respuestasJuego.getIdUsuario(),
                                respuestasJuego.getIdPreguntaJuego().getIdJuego(),
                                juegoParaUnUsuario.getJuegoDelListadoDeJuegos().getMiPuntuacion(),
                                respuestasJuego.getFechaRespuesta()), "tu"));
            } else {
                timeLineElements.add(new MTimeLineElement(
                        new MParticipacionUsuarioEnJuegoTimeLineElement(
                                respuestasJuego.getIdUsuario(),
                                respuestasJuego.getIdPreguntaJuego().getIdJuego(),
                                juegoParaUnUsuario.getJuegoDelListadoDeJuegos().getMiPuntuacion(),
                                respuestasJuego.getFechaRespuesta()), ""));
            }
        }

        List<Pelicula> peliculas = this.peliculaService.obtenerPeliculasTimeLine(timeLineActual, cuando);
        for (Pelicula pelicula : peliculas
        ) {
            timeLineElements.add(new MTimeLineElement(pelicula));
        }

        List<CapituloSerie> capituloSeries = this.serieService.obtenerCapitulosSeriesTimeLine(timeLineActual, cuando);
        for (CapituloSerie capitulos : capituloSeries
        ) {
            timeLineElements.add(new MTimeLineElement(capitulos));
        }

        List<JuegoAdivinarPeliculaSerie> juegoAdivinarPeliculaSeries = this.juegoService.obtenenerJuegosTimeLine(timeLineActual, cuando);
        for (JuegoAdivinarPeliculaSerie juego: juegoAdivinarPeliculaSeries
             ) {
            timeLineElements.add(new MTimeLineElement(juego));
        }

        //Se ordenan
        Collections.sort(timeLineElements, Collections.reverseOrder());

        //Se cogen los 10 primeros
        if (timeLineElements.size() >= 10) {
            timeLineElements = timeLineElements.subList(0, 10);
        }

        //Se añaden los elementos a la lista del timeline
        timeLine.setElementList(timeLineElements);

        return this.sacarLosFechasDePrimerosYUltimosDelTimeLine(timeLine, timeLineActual, cuando);
    }

    private MTimeLine sacarLosFechasDePrimerosYUltimosDelTimeLine(MTimeLine timeLineNuevo, MTimeLine actual, String cuando) {
        if (!timeLineNuevo.getElementList().isEmpty()) {
            switch (cuando) {
                case "antes":
                    timeLineNuevo.setFechaPrimerElemento(timeLineNuevo.getElementList().get(0).getFecha());
                    timeLineNuevo.setFechaUltimoElemento(actual.getFechaUltimoElemento());
                    break;
                case "despues":
                    timeLineNuevo.setFechaPrimerElemento(actual.getFechaPrimerElemento());
                    timeLineNuevo.setFechaUltimoElemento(timeLineNuevo.getElementList().get(timeLineNuevo.getElementList().size() - 1).getFecha());
                    break;
                default:
                    timeLineNuevo.setFechaPrimerElemento(timeLineNuevo.getElementList().get(0).getFecha());
                    timeLineNuevo.setFechaUltimoElemento(timeLineNuevo.getElementList().get(timeLineNuevo.getElementList().size() - 1).getFecha());
                    break;
            }
            return timeLineNuevo;
        } else {
            return null;
        }
    }


}
