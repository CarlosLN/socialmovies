package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionCapituloSerie;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionCapituloSerieID;
import com.socialmovies.SocialMovies.model.Serie.MDatosActualizadosTrasVotarCapitulo;
import com.socialmovies.SocialMovies.model.Serie.MVotacionAmigoEnFichaSerie;
import com.socialmovies.SocialMovies.model.Serie.MVotacionSerieEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.repository.CapituloSerieRepository;
import com.socialmovies.SocialMovies.repository.VotacionCapituloSerieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class VotacionCapituloService {

    @Autowired
    private VotacionCapituloSerieRepository votacionCapituloSerieRepository;

    @Autowired
    private CapituloSerieRepository capituloSerieRepository;

    @Autowired
    private AmistadesService amistadesService;

    @Autowired
    private SerieService serieService;

    public MDatosActualizadosTrasVotarCapitulo crearActualizarVotacion(Usuario usuario, CapituloSerie capituloSerie, double nota) {

        VotacionCapituloSerie votacionCapituloSerie = this.obtenerMiVotacionDeUnCapitulo(usuario, capituloSerie);
        if (votacionCapituloSerie != null) {
            if (nota < 0) {
                votacionCapituloSerie.setActivo(false);
            } else {
                votacionCapituloSerie.setVotacion(nota);
                votacionCapituloSerie.setFechaVotacion(new Date());
            }
            this.votacionCapituloSerieRepository.save(votacionCapituloSerie);
            return new MDatosActualizadosTrasVotarCapitulo(
                    this.votacionCapituloSerieRepository.mediaVotosSerie(capituloSerie.getIdSerie()).orElse(0.0),
                    this.votacionCapituloSerieRepository.cuantosVotosPorSerie(capituloSerie.getIdSerie()),
                    this.votacionCapituloSerieRepository.mediaMisVotosDeUnaSerie(usuario, capituloSerie.getIdSerie()).orElse(-1.0),
                    capituloSerie.getNumeroTemporada(),
                    this.votacionCapituloSerieRepository.cuantosVotosPorTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada()),
                    this.votacionCapituloSerieRepository.mediaVotosTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada()).orElse(0.0),
                    this.votacionCapituloSerieRepository.cuantosVotosPorCapitulosTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada(), capituloSerie),
                    this.votacionCapituloSerieRepository.mediaVotosCapituloTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada(), capituloSerie).orElse(0.0),
                    new Date()
            );
        } else {
            if (!(nota < 0)) {
                VotacionCapituloSerieID votacionCapituloSerieID = new VotacionCapituloSerieID(usuario.getId(), capituloSerie.getId());
                VotacionCapituloSerie aux = new VotacionCapituloSerie(votacionCapituloSerieID, usuario, capituloSerie, nota);
                try {
                    this.votacionCapituloSerieRepository.save(aux);
                    return new MDatosActualizadosTrasVotarCapitulo(
                            this.votacionCapituloSerieRepository.mediaVotosSerie(capituloSerie.getIdSerie()).orElse(0.0),
                            this.votacionCapituloSerieRepository.cuantosVotosPorSerie(capituloSerie.getIdSerie()),
                            this.votacionCapituloSerieRepository.mediaMisVotosDeUnaSerie(usuario, capituloSerie.getIdSerie()).orElse(0.0),
                            capituloSerie.getNumeroTemporada(),
                            this.votacionCapituloSerieRepository.cuantosVotosPorTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada()),
                            this.votacionCapituloSerieRepository.mediaVotosTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada()).orElse(0.0),
                            this.votacionCapituloSerieRepository.cuantosVotosPorCapitulosTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada(), capituloSerie),
                            this.votacionCapituloSerieRepository.mediaVotosCapituloTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada(), capituloSerie).orElse(0.0),
                            new Date()
                    );
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        }
        return null;
    }

    public VotacionCapituloSerie obtenerMiVotacionDeUnCapitulo(Usuario usuario, CapituloSerie capituloSerie) {
        return this.votacionCapituloSerieRepository.findByIdUsuarioAndIdCapituloSerieAndActivoTrue(usuario, capituloSerie);
    }

    public List<MVotacionAmigoEnFichaSerie> obtenerVotacionesDeAmigosDeUnaSerie(Usuario usuario, Serie serie, int pagina, int cuantosPorPagina){
        Pageable pagination = PageRequest.of(pagina, cuantosPorPagina);
        List<MVotacionAmigoEnFichaSerie> votacionAmigoEnFichaSeries = new ArrayList<>();

        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
        ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        List<Object[]> objetos = idsAmigos.size() != 0 ? this.votacionCapituloSerieRepository.obtenerVotacionesMediaDeAmigosDeUnaSerie(idsAmigos, serie, pagination) : new ArrayList<>();

        for (Object[] object : objetos
             ) {

            votacionAmigoEnFichaSeries.add(new MVotacionAmigoEnFichaSerie(
                    object[0].toString(),
                    object[1].toString(),
                    Double.parseDouble(object[3].toString()),
                    Integer.parseInt(object[4].toString()),
                    Integer.parseInt(object[5].toString()),
                    object[6].toString(),
                    (Date) object[2]
            ));
        }

        return votacionAmigoEnFichaSeries;
    }

    public int obtenerCuantosAmigosHanVotadoUnaSerie(Usuario usuario, Serie serie){
        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
        ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        return idsAmigos.size() != 0 ? this.votacionCapituloSerieRepository.obtenerCuantosAmigosHanVotadoUnCapituloDeUnaSerie(idsAmigos,serie) : 0;
    }

    public List<MVotacionSerieEnPerfilAmigo> obtenerVotacionesDeSerieDeUnAmigo(Usuario usuario, Usuario amigo, String orden, int pagina, int cuantosAmigos){
        List<MVotacionSerieEnPerfilAmigo> votacionSerieEnPerfilAmigos = new ArrayList<>();
        MVotacionSerieEnPerfilAmigo votacion = new MVotacionSerieEnPerfilAmigo();
        List<Object[]> votaciones = new ArrayList<>();

        Pageable pagination = PageRequest.of(pagina, cuantosAmigos);

        switch(orden){
            case "VotacionDesc":    //Ordenar por votacion descendiente
                votaciones = this.votacionCapituloSerieRepository.obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorVotacionDesc(amigo, pagination);
                break;
            case "VotacionAsc":    //Ordenar por votacion ascedente
                votaciones = this.votacionCapituloSerieRepository.obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorVotacionAsc(amigo, pagination);
                break;
            case "FechaVotacionAsc":    //Ordenar por fechaVotacion ascendente
                votaciones = this.votacionCapituloSerieRepository.obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorFechaVotacionAsc(amigo, pagination);
                break;
            default:    //Ordenar por fechaVotacion descendente
                votaciones = this.votacionCapituloSerieRepository.obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorFechaVotacionDesc(amigo, pagination);
                break;
        }

        for (Object[] object: votaciones
             ) {
            votacion = new MVotacionSerieEnPerfilAmigo(
                    Long.parseLong(object[0].toString()),
                    object[1].toString(),
                    (Date) object[2],
                    object[3].toString(),
                    Integer.parseInt(object[4].toString()),
                    Integer.parseInt(object[5].toString()),
                    object[6].toString(),
                    Double.parseDouble(object[7].toString()),
                    (Date) object[8]
            );

            CapituloSerie capituloSerie = this.capituloSerieRepository.findByIdAndActivoTrue(Long.parseLong(object[9].toString())).orElse(null);

            votacion.setVotacionMedia(Math.round(this.votacionCapituloSerieRepository.mediaVotosCapituloTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada(), capituloSerie).orElse(0.0) * 10)/10.0);
            votacion.setNumVotantes(this.votacionCapituloSerieRepository.cuantosVotosPorCapitulosTemporadaSerie(capituloSerie.getIdSerie(), capituloSerie.getNumeroTemporada(), capituloSerie));

            if(usuario.getId() != amigo.getId()){
                VotacionCapituloSerie miVoto = this.votacionCapituloSerieRepository.findByIdUsuarioAndIdCapituloSerieAndActivoTrue(usuario,capituloSerie);
                if(miVoto != null){
                    votacion.setTuVoto(miVoto.getVotacion());
                    votacion.setFechaTuVoto(miVoto.getFechaVotacion());
                }
            }

            votacionSerieEnPerfilAmigos.add(votacion);
        }

        return votacionSerieEnPerfilAmigos;
    }


    public List<VotacionCapituloSerie> obtenerUltimasVotacionesTimeLine(List<Long> usuarios, MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.votacionCapituloSerieRepository.obtenerLasUltimasVotacionesAntesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.votacionCapituloSerieRepository.obtenerLasUltimasVotacionesDespuesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.votacionCapituloSerieRepository.obtenerLasUltimasVotacionesTimeLine(usuarios,10);
        }
    }

    public int darDeBajaAltaVotacionCapituloSerie(Usuario usuario, boolean altaBaja){
        return this.votacionCapituloSerieRepository.darDeBajaAltaVotacionCapituloSerie(usuario, altaBaja);
    }


}
