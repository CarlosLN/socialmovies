package com.socialmovies.SocialMovies.service.exceptions;

import java.util.function.Supplier;

public class GlobalExcepcion extends RuntimeException {
    private int codigoError;
    private String mensajeError;

    public GlobalExcepcion(int codigoError, String mensajeError) {
        this.codigoError = codigoError;
        this.mensajeError = mensajeError;
    }

    public int getCodigoError() {
        return codigoError;
    }

    public String getMensajeError() {
        return mensajeError;
    }
}
