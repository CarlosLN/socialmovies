package com.socialmovies.SocialMovies.service;

import java.util.ArrayList;
import java.util.List;

import com.socialmovies.SocialMovies.model.Usuario.MDatosPerfilAmigo;
import com.socialmovies.SocialMovies.model.Usuario.MUsuarioAvatar;
import com.socialmovies.SocialMovies.model.Usuario.MUsuariosBuscados;
import com.socialmovies.SocialMovies.repository.*;
import com.socialmovies.SocialMovies.service.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Amistad.AmistadID;
import com.socialmovies.SocialMovies.entity.Usuario;

import static com.socialmovies.SocialMovies.constantes.Constantes.*;

@Service("servicioAmistades")
public class AmistadesService {

    @Autowired
    @Qualifier("amistadesRepository")
    private AmistadRepository amistadesRepository;

    @Autowired
    @Qualifier("usuarioRepository")
    private UsuarioRepository userRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    @Qualifier("votacionPeliculaRepository")
    private VotacionPeliculaRepository votacionPeliculaRepository;

    @Autowired
    private VotacionCapituloSerieRepository votacionCapituloSerieRepository;

    @Autowired
    private ComentarioPeliculaRepository comentarioPeliculaRepository;

    @Autowired
    private ComentarioCapituloSerieRepository comentarioCapituloSerieRepository;

    /**
     * @param userSolicitante
     * @param userSolicitado
     * @return
     */
    public boolean mandarPeticionAmistad(Usuario userSolicitante, Usuario userSolicitado) {
        AmistadID amistadID = new AmistadID(userSolicitante.getId(), userSolicitado.getId());
        AmistadID amistadInversaID = new AmistadID(userSolicitado.getId(), userSolicitante.getId());

        if (!this.amistadesRepository.existsById(amistadID) && !this.amistadesRepository.existsById(amistadInversaID)) {
            Amistad solicitudAmistad = new Amistad();
            solicitudAmistad.setId(amistadID);
            solicitudAmistad.setIdUserSolicitante(userSolicitante);
            solicitudAmistad.setIdUserSolicitado(userSolicitado);
            this.amistadesRepository.save(solicitudAmistad);
            return true;
        }

        Amistad amistad = this.amistadesRepository.findById(amistadID).orElse(null);

        if (amistad != null) {
            if (!amistad.isActivo()) { // Si no está activa la mando
                amistad.setActivo(true);
                amistad.setPeticionAceptada(false);
                this.amistadesRepository.save(amistad);
                return true;
            } else if (amistad.isPeticionAceptada()) { // Si está activa y aceptada
                throw new GlobalExcepcion(606, YA_SOIS_AMIGOS);
            } else { // Si está activa pero no aceptada
                throw new GlobalExcepcion(607, PETICION_AMISTAD_ENVIADA);
            }
        }

        amistad = this.amistadesRepository.findById(amistadInversaID).orElse(null);

        if (amistad != null) {
            if (!amistad.isActivo()) { // Si no está activa la borro y mando una nueva cambiando el orden del
                // solicitante
                this.amistadesRepository.delete(amistad);
                Amistad nuevaPeticion = new Amistad();
                nuevaPeticion.setId(amistadID);
                nuevaPeticion.setIdUserSolicitante(userSolicitante);
                nuevaPeticion.setIdUserSolicitado(userSolicitado);
                this.amistadesRepository.save(nuevaPeticion);
                return true;
            } else if (amistad.isPeticionAceptada()) { // Si está activa y aceptada
                throw new GlobalExcepcion(606, YA_SOIS_AMIGOS);
            } else { // Si está activa pero no aceptada mira tus solicitudes de amistad recibida
                throw new GlobalExcepcion(608, PETICION_AMISTAD_RECIBIDA);
            }
        }

        return true;

    }

    /**
     * @param user
     * @return
     */
    public List<MUsuarioAvatar> obtenerPeticionesAmistad(Usuario user, boolean recibidas) {
        List<Amistad> misPeticiones;

        if (recibidas) {
            misPeticiones = this.amistadesRepository
                    .findByIdUserSolicitadoAndPeticionAceptadaAndActivo(user, false, true);
        } else {
            misPeticiones = this.amistadesRepository
                    .findByIdUserSolicitanteAndPeticionAceptadaAndActivo(user, false, true);
        }

        if (misPeticiones.size() > 0) {
            List<MUsuarioAvatar> nombresUsuario = new ArrayList<MUsuarioAvatar>();
            if (recibidas) {
                for (Amistad amistad : misPeticiones) {
                    if(amistad.getIdUserSolicitante().isActivo()){
                        nombresUsuario
                                .add(new MUsuarioAvatar(amistad.getIdUserSolicitante()));
                    }
                }
            } else {
                for (Amistad amistad : misPeticiones) {
                    if(amistad.getIdUserSolicitado().isActivo()){
                        nombresUsuario
                                .add(new MUsuarioAvatar(amistad.getIdUserSolicitado()));
                    }
                }
            }

            return nombresUsuario;
        } else {
            if (recibidas) {
                throw new GlobalExcepcion(609, SIN_PETICIONES_RECIBIDAS);
            } else {
                throw new GlobalExcepcion(614, SIN_PETICIONES_ENVIADAS);
            }

        }
    }

    public List<Amistad> obtenerAmistades(Usuario user){
        List<Amistad> misAmistades = new ArrayList<>();
        misAmistades = this.amistadesRepository.obtenerAmistades(user, true, true);
        return misAmistades;
    }

    public ArrayList<MUsuarioAvatar> obtenerAmistadesMUsuarioAvatar(Usuario user) {
        List<Amistad> misAmistades = this.amistadesRepository
                .obtenerAmistades(user, true, true);

        if (misAmistades.size() > 0) {
            ArrayList<MUsuarioAvatar> listaAmigos = new ArrayList<MUsuarioAvatar>();
            for (Amistad amistad : misAmistades) {
                if (user.getId() == amistad.getIdUserSolicitante().getId()) {
                    if(amistad.getIdUserSolicitado().isActivo()){
                        listaAmigos.add(new MUsuarioAvatar(amistad.getIdUserSolicitado()));
                    }
                } else {
                    if(amistad.getIdUserSolicitante().isActivo()){
                        listaAmigos.add(new MUsuarioAvatar(amistad.getIdUserSolicitante()));
                    }
                }
            }
            return listaAmigos;
        } else {
            throw new GlobalExcepcion(613, NO_TIENES_AMISTADES);
        }
    }

    /**
     * @param user
     * @param userAmigo
     * @return
     */
    public boolean aceptarPeticionAmistad(Usuario user, Usuario userAmigo) {
        AmistadID amistadID = new AmistadID(userAmigo.getId(), user.getId());
        Amistad peticionAmistad = this.amistadesRepository.findById(amistadID).orElse(null);
        if (peticionAmistad != null) {
            peticionAmistad.setPeticionAceptada(true);
            this.amistadesRepository.save(peticionAmistad);
            return true;
        } else {
            throw new GlobalExcepcion(610, ACEPTAR_PETICION);
        }
    }

    /**
     * @param user
     * @param userAmigo
     * @param accion
     * @return
     */
    public boolean eliminarCancelarAmistad(Usuario user, Usuario userAmigo, String accion) {
        AmistadID amistadID = new AmistadID(userAmigo.getId(), user.getId());
        Amistad peticionAmistad = this.amistadesRepository.findById(amistadID).orElse(null);
        if (peticionAmistad != null) {
            peticionAmistad.setPeticionAceptada(false);
            peticionAmistad.setActivo(false);
            this.amistadesRepository.save(peticionAmistad);
            return true;
        } else {
            if (accion.equals("eliminarAmistad")) {
                amistadID = new AmistadID(user.getId(), userAmigo.getId());
                peticionAmistad = this.amistadesRepository.findById(amistadID).orElse(null);
                if (peticionAmistad != null) {
                    peticionAmistad.setPeticionAceptada(false);
                    peticionAmistad.setActivo(false);
                    this.amistadesRepository.save(peticionAmistad);
                    return true;
                } else {
                    throw new GlobalExcepcion(612, ELIMINAR_AMISTAD);
                }
            }
            throw new GlobalExcepcion(611, RECHAZAR_PETICION);
        }
    }

    public List<MUsuariosBuscados> devolverUsuariosBuscados(Usuario username, List<Usuario> usuarios){
        List<MUsuariosBuscados> listaUsuarios = new ArrayList<>();

        for(Usuario usuario : usuarios){
            if (usuario.getId() != username.getId()){
                if(this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(usuario,username,false,true)){
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "peticionAmistadEnviada"));
                }else if(this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(username,usuario,false,true)){
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "peticionAmistadRecibida"));
                }else if(this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(usuario,username,true,true) ||
                        this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(username,usuario,true,true)){
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "amigo"));
                }else{
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "usuario"));
                }
            }
        }

        return listaUsuarios;
    }

    public boolean sonAmigosOPeticion(Usuario usuarioSolicitante, Usuario usuarioSolicitado){
        //Si son amigos
        if(this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(usuarioSolicitante,usuarioSolicitado,true,true)){
            return true;
        }else if (this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(usuarioSolicitado,usuarioSolicitante,true,true)){
            return true;
        }else if (this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(usuarioSolicitante,usuarioSolicitado,false,true)){
            return true;
        }else{
            throw new GlobalExcepcion(617, NO_PUEDE_ACCEDER_PERFIL);
        }
    }

    public MDatosPerfilAmigo devolverDatosPerfilAmigo(Usuario userSolicitante, Usuario userAmigo){
        return new MDatosPerfilAmigo(
                this.votacionPeliculaRepository.numPeliculasVotadasPorUnUsuario(userAmigo).orElse(0),
                this.votacionPeliculaRepository.mediaVotosDePeliculasDeUnUsuario(userAmigo).orElse(0.0),
                this.comentarioPeliculaRepository.numPeliculasComentadasPorUnUsuario(userAmigo).orElse(0),
                this.votacionCapituloSerieRepository.numSeriesVotadasPorUnUsuario(userAmigo),
                this.votacionCapituloSerieRepository.numCapitulosVotadosPorUnsuario(userAmigo),
                this.votacionCapituloSerieRepository.mediaVotosDeSeriesDeUnUsuario(userAmigo).orElse(0.0),
                this.comentarioCapituloSerieRepository.numSeriesComentadasPorUnUsuario(userAmigo),
                this.comentarioCapituloSerieRepository.numCapitulosSeriesComentadasPorUnUsuario(userAmigo),
                this.amistadesRepository.obtenerNumAmistadesDeUnAmigo(userAmigo).orElse(0)
        );
    }

    public List<MUsuariosBuscados> obtenerMiRelacionConAmigosDeUnAmigo(Usuario user, Usuario amigo, int pagina, int cuantosPorPagina){
        Pageable paginator = PageRequest.of(pagina, cuantosPorPagina);
        List<String> listadoUsuarios = this.amistadesRepository.obtenerListadoAmigosDeUnUsuario(amigo, paginator).orElse(new ArrayList<>());

        List<MUsuariosBuscados> listaUsuarios = new ArrayList<>();

        for(String username : listadoUsuarios){
            Usuario usuario = this.usuarioService.devolverUsuario(username);
            if (usuario.getId() != user.getId()){
                if(this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(usuario,user,false,true)){
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "peticionAmistadEnviada"));
                }else if(this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(user,usuario,false,true)){
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "peticionAmistadRecibida"));
                }else if(this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(usuario,user,true,true) ||
                        this.amistadesRepository.existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(user,usuario,true,true)){
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "amigo"));
                }else{
                    listaUsuarios.add(new MUsuariosBuscados(usuario, "usuario"));
                }
            }else{
                listaUsuarios.add(new MUsuariosBuscados(usuario, "usuarioMismo"));
            }
        }

        return listaUsuarios;
    }

    public int darDeBajaAltaAmistades(Usuario usuario, boolean altaBaja){
        return this.amistadesRepository.darDeBajaAltaAmistades(usuario, altaBaja);
    }
}
