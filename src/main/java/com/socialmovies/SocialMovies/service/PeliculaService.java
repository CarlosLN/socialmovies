package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionPelicula;
import com.socialmovies.SocialMovies.model.Pelicula.*;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.repository.PeliculaRepository;
import com.socialmovies.SocialMovies.service.exceptions.GlobalExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Pageable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.socialmovies.SocialMovies.constantes.Constantes.*;

@Service("servicioPelicula")
public class PeliculaService {

    @Autowired
    @Qualifier("peliculaRepository")
    private PeliculaRepository peliculaRepository;

    @Autowired
    private VotacionPeliculaService votacionPeliculaService;

    @Autowired
    private ComentarioPeliculaService comentarioPeliculaService;

    @Autowired
    private AmistadesService amistadesService;

    @Autowired
    private UsuarioService usuarioService;

    public boolean crearPelicula(Pelicula pelicula){
        try{
            pelicula.setFecha_incluida(new Date());
            pelicula.setActiva(true);
            this.peliculaRepository.save(pelicula);
            return true;
        }catch (Exception e){
            throw new GlobalExcepcion(619,PELICULA_NO_GUARDADA);
        }
    }

    public MPeliculasBuscadasEncontradas buscarPelicula(String nombrePelicula, int pagina, int cuantasPorPagina){
        return new MPeliculasBuscadasEncontradas(
                this.obtenerCuantasPeliculasBuscadas(nombrePelicula),
                this.obtenerListadoPeliculasBuscadas(nombrePelicula, pagina, cuantasPorPagina)
        );
    }

    public MPelicula buscarPeliculaConUsuario(Usuario usuario, Pelicula pelicula, int cuantosAmigos){

        VotacionPelicula miVoto = this.votacionPeliculaService.obtenerMiVotacionDePelicula(usuario, pelicula);
        double voto;
        Date fechaVoto = null;
        if(miVoto!=null){
            voto = miVoto.getVotacion();
            fechaVoto = miVoto.getFecha_votacion();
        }else{
            voto = -1.0;
        }

        List<MVotacionAmigoEnFichaPelicula> votacionAmigoEnFichaPelicula =
                this.votacionPeliculaService.obtenerVotacionesDeAmigosDeUnaPelicula(usuario, pelicula,0,cuantosAmigos);

        return new MPelicula(
                pelicula,
                this.votacionPeliculaService.mediaVotosPelicula(pelicula),
                this.votacionPeliculaService.cuantosVotosPorPelicula(pelicula),
                voto,
                fechaVoto,
                votacionAmigoEnFichaPelicula,
                this.votacionPeliculaService.obtenerCuantosAmigosHanVotadoUnaPelicula(usuario, pelicula),
                this.comentarioPeliculaService.obtenerComentarioDeAmigosDeUnaPelicula(usuario, pelicula,0,cuantosAmigos),
                this.comentarioPeliculaService.obtenerComentarioDeUnUsuarioDeUnaPelicula(usuario, pelicula),
                this.comentarioPeliculaService.obtenerCuantosAmigosHanComentadoUnaPelicula(usuario, pelicula)
                );
    }

    public Pelicula obtenerPeliculaPorId(long idPelicula){
        return this.peliculaRepository.findByIdAndActivaTrue(idPelicula).orElseThrow(
                ()-> new GlobalExcepcion(620, PELICULA_DONT_EXISTS)
        );
    }

    public MPeliculaRecomendadaMejorvaloradaMasvotada obtenerPeliculaRecomendada(Usuario u, Pelicula p){
        List<Object[]> objects = null;
        if(p==null){
            objects = this.peliculaRepository.obtenerPeliculaRecomendada(u,0).orElse(null);
        }else{
            objects = this.peliculaRepository.obtenerPeliculaRecomendada(u,p.getId()).orElse(null);
        }

        try{
            Object[] object = objects.get(0);
                return new MPeliculaRecomendadaMejorvaloradaMasvotada(
                        Long.parseLong(object[0].toString()),
                        object[1].toString(),
                        object[2].toString(),
                        (Date)object[3],
                        Double.parseDouble(object[5].toString()),
                        Integer.parseInt(object[4].toString()),
                        null,
                        -1
                );
        }catch (Exception e){
            return null;
        }

    }

    public MPeliculaRecomendadaMejorvaloradaMasvotada obtenerPeliculaMejorValorada(Usuario usuario, int pagina, int cuantosPoPagina){
        Pageable paginator = PageRequest.of(pagina, cuantosPoPagina);
        List<Object[]> objects = this.peliculaRepository.obtenerPeliculaMejorValorada(paginator).orElse(null);

        try{
            Object[] object = objects.get(0);
            VotacionPelicula miVoto = this.votacionPeliculaService.obtenerMiVotacionDePelicula(usuario,
                    this.obtenerPeliculaPorId(Long.parseLong(object[0].toString())));
            double voto;
            Date fechaVoto = null;
            if(miVoto!=null){
                voto = miVoto.getVotacion();
                fechaVoto = miVoto.getFecha_votacion();
            }else{
                voto = -1;
            }
            return new MPeliculaRecomendadaMejorvaloradaMasvotada(
                    Long.parseLong(object[0].toString()),
                    object[1].toString(),
                    object[2].toString(),
                    (Date)object[3],
                    Double.parseDouble(object[5].toString()),
                    Integer.parseInt(object[4].toString()),
                    fechaVoto,
                    voto
            );
        }catch (Exception e){
            return null;
        }

    }

    public MPeliculaRecomendadaMejorvaloradaMasvotada obtenerPeliculaMasVotada(Usuario usuario, int pagina, int cuantosPoPagina){
        Pageable paginator = PageRequest.of(pagina, cuantosPoPagina);
        List<Object[]> objects = this.peliculaRepository.obtenerPeliculaMasVotada(paginator).orElse(null);

        try{
            Object[] object = objects.get(0);
            VotacionPelicula miVoto = this.votacionPeliculaService.obtenerMiVotacionDePelicula(usuario,
                    this.obtenerPeliculaPorId(Long.parseLong(object[0].toString())));
            double voto;
            Date fechaVoto = null;
            if(miVoto!=null){
                voto = miVoto.getVotacion();
                fechaVoto = miVoto.getFecha_votacion();
            }else{
                voto = -1;
            }
            return new MPeliculaRecomendadaMejorvaloradaMasvotada(
                    Long.parseLong(object[0].toString()),
                    object[1].toString(),
                    object[2].toString(),
                    (Date)object[3],
                    Double.parseDouble(object[5].toString()),
                    Integer.parseInt(object[4].toString()),
                    fechaVoto,
                    voto
            );
        }catch (Exception e){
            return null;
        }

    }

    private List<MPeliculaBuscada> obtenerListadoPeliculasBuscadas(String parteBuscada, int pagina, int cuantasPorPagina){
        Pageable paginator = PageRequest.of(pagina, cuantasPorPagina);
        List<Object[]> objects = this.peliculaRepository.obtenerListBusquedaPeliculas(parteBuscada, paginator).orElse(null);

        List<MPeliculaBuscada> peliculasEncontradas = new ArrayList();

        if(objects!=null){
            for (Object[] object : objects
            ) {
                peliculasEncontradas.add(new MPeliculaBuscada(
                        Long.parseLong(object[0].toString()),
                        object[1].toString(),
                        object[2].toString(),
                        (Date) object[3]));
            }
        }

        return peliculasEncontradas;
    }

    private int obtenerCuantasPeliculasBuscadas(String parteBuscada){
        List<Integer> cuantos = this.peliculaRepository.obtenerCuantasBusquedaPeliculas(parteBuscada).orElse(new ArrayList<>());
        return cuantos.size();
    }

    public List<Pelicula> obtenerPeliculasTimeLine(MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.peliculaRepository.obtenerPeliculasAntesLimiteFechaTimeLine(10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.peliculaRepository.obtenerPeliculasDespuesLimiteFechaTimeLine(10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.peliculaRepository.obtenerPeliculasTimeLine(10);
        }
    }

}
