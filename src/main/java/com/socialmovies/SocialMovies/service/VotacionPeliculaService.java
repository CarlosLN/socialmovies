package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionPelicula;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionPeliculaID;
import com.socialmovies.SocialMovies.model.Pelicula.MVotacionAmigoEnFichaPelicula;
import com.socialmovies.SocialMovies.model.Pelicula.MVotacionPeliculaEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.repository.VotacionPeliculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class VotacionPeliculaService {

    @Autowired
    private VotacionPeliculaRepository votacionPeliculaRepository;

    @Autowired
    private AmistadesService amistadesService;

    @Autowired
    private PeliculaService peliculaService;



    public void crearActualizarVotacion(Usuario usuario, Pelicula pelicula, double nota){
        VotacionPelicula votacionPelicula = this.obtenerMiVotacionDePelicula(usuario, pelicula);
        if(votacionPelicula != null){
            if(nota < 0){
                votacionPelicula.setActivo(false);
            }else{
                votacionPelicula.setVotacion(nota);
                votacionPelicula.setFecha_votacion(new Date());
            }
            this.votacionPeliculaRepository.save(votacionPelicula);
        }else{
            if(!(nota < 0)){
                VotacionPeliculaID votacionID = new VotacionPeliculaID(usuario.getId(), pelicula.getId());
                VotacionPelicula aux = new VotacionPelicula(votacionID,usuario,pelicula,nota);
                try{
                    this.votacionPeliculaRepository.save(aux);
                } catch (Exception e){
                    System.out.println(e.toString());
                }
            }
        }
    }

    public VotacionPelicula obtenerMiVotacionDePelicula(Usuario usuario, Pelicula pelicula){
        return this.votacionPeliculaRepository.findByIdUsuarioAndIdPeliculaAndActivoTrue(usuario, pelicula);
    }

    public double mediaVotosPelicula(Pelicula pelicula){
        try{
            return (Math.round(this.votacionPeliculaRepository.mediaVotosPelicula(pelicula)* 10)/10.0);
        }catch (Exception e){
            return 0;
        }
    }

    public int cuantosVotosPorPelicula(Pelicula pelicula){
        return this.votacionPeliculaRepository.cuantosVotosPorPelicula(pelicula);
    }

    public List<MVotacionAmigoEnFichaPelicula>obtenerVotacionesDeAmigosDeUnaPelicula(Usuario usuario, Pelicula pelicula, int pagina, int cuantasPeliculas){
        Pageable pagination = PageRequest.of(pagina, cuantasPeliculas);
        List<MVotacionAmigoEnFichaPelicula> votacionAmigoEnFichaPeliculaList = new ArrayList<>();

        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
             ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        List<Object[]> objetos = idsAmigos.size() != 0 ? this.votacionPeliculaRepository.obtenerVotacionesDeAmigosDeUnaPelicula(idsAmigos, pelicula, pagination).orElse(new ArrayList<>()) :  new ArrayList<>();

        MVotacionAmigoEnFichaPelicula votacionAmigoEnFichaPelicula;
        for (Object[] object : objetos
             ) {
            votacionAmigoEnFichaPelicula = new MVotacionAmigoEnFichaPelicula();
            votacionAmigoEnFichaPelicula.setUsername(object[0].toString());
            votacionAmigoEnFichaPelicula.setAvatar(object[1].toString());
            votacionAmigoEnFichaPelicula.setFechaVoto((Date) object[2]);
            votacionAmigoEnFichaPelicula.setVoto((Double) object[3]);
            votacionAmigoEnFichaPeliculaList.add(votacionAmigoEnFichaPelicula);
        }
        return votacionAmigoEnFichaPeliculaList;
    }

    public int obtenerCuantosAmigosHanVotadoUnaPelicula(Usuario usuario, Pelicula pelicula){
        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
        ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        return idsAmigos.size() != 0 ? this.votacionPeliculaRepository.obtenerCuantosAmigosHanVotadoUnaPelicula(idsAmigos,pelicula).orElse(0) : 0;
    }

    //Obtener listado de peliculas votadas por un usuario paginadas
    public ArrayList<MVotacionPeliculaEnPerfilAmigo> obtenerVotacionesAmigo(Usuario usuario, Usuario amigo, String orden, int pagina, int cuantosAmigos){
        ArrayList<MVotacionPeliculaEnPerfilAmigo> votosPeliculasAmigo = new ArrayList<>();
        MVotacionPeliculaEnPerfilAmigo votacion = new MVotacionPeliculaEnPerfilAmigo();
        ArrayList<VotacionPelicula> votaciones = new ArrayList<VotacionPelicula>();

        Pageable pagination = PageRequest.of(pagina, cuantosAmigos);

        switch(orden){
            case "VotacionDesc":    //Ordenar por votacion descendiente
                votaciones = this.votacionPeliculaRepository.findByIdUsuarioAndActivoTrueOrderByVotacionDesc(amigo, pagination);
                break;
            case "VotacionAsc":    //Ordenar por votacion ascedente
                votaciones = this.votacionPeliculaRepository.findByIdUsuarioAndActivoTrueOrderByVotacionAsc(amigo, pagination);
                break;
            case "FechaVotacionAsc":    //Ordenar por fechaVotacion ascendente
                votaciones = this.votacionPeliculaRepository.findByIdUsuarioAndActivoTrueOrderByFechaVotacionAsc(amigo, pagination);
                break;
            default:    //Ordenar por fechaVotacion descendente
                votaciones = this.votacionPeliculaRepository.findByIdUsuarioAndActivoTrueOrderByFechaVotacionDesc(amigo, pagination);
                break;
        }

        for (VotacionPelicula votoAmigo: votaciones
        ) {
            votacion = new MVotacionPeliculaEnPerfilAmigo(
                    votoAmigo.getIdPelicula(),
                    (Math.round(this.mediaVotosPelicula(votoAmigo.getIdPelicula()) * 10)/10.0),
                    this.cuantosVotosPorPelicula(votoAmigo.getIdPelicula()),
                    votoAmigo.getVotacion(),
                    votoAmigo.getFecha_votacion());

            if(usuario.getId() != amigo.getId()){
                VotacionPelicula miVoto = this.votacionPeliculaRepository.findByIdUsuarioAndIdPeliculaAndActivoTrue(usuario,votoAmigo.getIdPelicula());
                if(miVoto != null){
                    votacion.setTuVoto(miVoto.getVotacion());
                    votacion.setFechaTuVoto(miVoto.getFecha_votacion());
                }
            }

            votosPeliculasAmigo.add(votacion);
        }

        return votosPeliculasAmigo;
    }

    public List<VotacionPelicula> obtenerUltimasVotacionesTimeLine(List<Long> usuarios, MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.votacionPeliculaRepository.obtenerLasUltimasVotacionesAntesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.votacionPeliculaRepository.obtenerLasUltimasVotacionesDespuesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.votacionPeliculaRepository.obtenerLasUltimasVotacionesTimeLine(usuarios,10);
        }
    }

    public int darDeBajaAltaVotacionPelicula(Usuario usuario, boolean altaBaja){
        return this.votacionPeliculaRepository.darDeBajaAltaVotacionPelicula(usuario, altaBaja);
    }

}
