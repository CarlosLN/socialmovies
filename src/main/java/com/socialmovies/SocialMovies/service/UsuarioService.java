package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.service.exceptions.GlobalExcepcion;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.socialmovies.SocialMovies.converter.Convertidor;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Usuario.MUsuario;
import com.socialmovies.SocialMovies.repository.UsuarioRepository;

import static com.socialmovies.SocialMovies.constantes.Constantes.*;

@Service("servicioUsuario")
public class UsuarioService implements UserDetailsService{

	@Autowired
	@Qualifier("usuarioRepository")
	private UsuarioRepository userRepository;

	@Autowired
	private AmistadesService amistadesService;

	@Autowired
	private VotacionPeliculaService votacionPeliculaService;

	@Autowired
	private VotacionCapituloService votacionCapituloService;

	@Autowired
	private ComentarioCapituloSerieService comentarioCapituloSerieService;

	@Autowired
	private ComentarioPeliculaService comentarioPeliculaService;

	@Autowired
	private JuegoService juegoService;
	
	@Autowired
	@Qualifier("convertidor")
	private Convertidor convertidor;
	
	
	private static final Log logger = LogFactory.getLog(UsuarioService.class);
	
	public UserDetails crear(Usuario usuario) {
		logger.info("CREANDO USUARIO");
		try {
			usuario.setActivo(true);
			userRepository.save(usuario);
			logger.info("USUARIO CREADO");
			return this.login(usuario.getUsername(),usuario.getPassword());
		} catch (Exception e) {
			throw new GlobalExcepcion(603, USER_DONT_EXISTS);
		}
	}
	
	public boolean actualizar(Usuario usuario) {
		logger.info("ACTUALIZANDO USUARIO");
		try {
			userRepository.save(usuario);
			logger.info("USUARIO ACTUALIZADO");
			return true;
		} catch (Exception e) {
			logger.error("HUBO UN ERROR ACTUALIZANDO USUARIO");
			return false;
		}
	}
	
	public boolean darDeBajaAltaUsuario(Usuario usuario, boolean altaBaja) {
		try {
			this.userRepository.darDeBajaAltaUsuario(usuario, altaBaja);
			this.amistadesService.darDeBajaAltaAmistades(usuario, altaBaja);
			this.comentarioCapituloSerieService.darDeBajaAltaComentariosCapitulosSeries(usuario, altaBaja);
			this.comentarioPeliculaService.darDeBajaAltaComentariosPeliculas(usuario, altaBaja);
			this.juegoService.darDeBajaAltaParticipacionesJuego(usuario, altaBaja);
			this.votacionCapituloService.darDeBajaAltaVotacionCapituloSerie(usuario, altaBaja);
			this.votacionPeliculaService.darDeBajaAltaVotacionPelicula(usuario, altaBaja);
			return true;
		}catch (Exception e) {
			logger.error(e.toString());
			return false;
		}
	}
	
	public List<MUsuario> obtener(){
		try {
			logger.info("DEVOLVIENDO USUARIOS");
			return convertidor.convertirUsuario(userRepository.findAll());
		}catch (Exception e) {
			logger.error("ERROR DEVOLVIENDO USUARIOS");
			return null;
		}
	}
	
	public UserDetails login(String username, String password) {
			logger.info("HACIENDO LOGIN");
			Usuario usuario = userRepository.findByUsernameAndPassword(username, password).
					orElseThrow(()-> new GlobalExcepcion(603, USER_DONT_EXISTS));

			if(usuario.isActivo()) {
				logger.info("LOGIN HECHO");
				return new User(username, password, new ArrayList<>());
			}else {
				logger.info("USUARIO NO EXISTE");
				throw new GlobalExcepcion(616, ERROR_USUARIO);
			}
	}
	
	/*
	 * public MUsuario devolverUsuario(String username) {
	 * logger.info("DEVOLVIENDO USUARIO"); Usuario usuario =
	 * userRepository.findByUsername(username).orElseThrow(UserDontExistsException::
	 * new); if(usuario.isActivo() == true) { return new MUsuario(usuario); }else {
	 * logger.info("USUARIO NO EXISTE"); throw new UserDontExistsException(); } }
	 */
	
	public Usuario devolverUsuario(String username) {

		logger.info("DEVOLVIENDO USUARIO");
		return userRepository.findByUsernameAndActivo(username, true).
				orElseThrow(() -> new GlobalExcepcion(603, USER_DONT_EXISTS));
	}
	
	public boolean updatePassword(Usuario user, String newPassword) {
		logger.info("Actualizando contraseña" + user.getPassword() + " " + newPassword);
		user.setPassword(newPassword);
		if(userRepository.save(user) != null) {
			return true;
		}else {
			throw new GlobalExcepcion(605, ERROR_UPDATE_PASSWORD);
		}
		
	}
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
			logger.info("DEVOLVIENDO USUARIO loadUserByUsername");		
			Usuario usuario = userRepository.findByUsernameAndActivo(username, true).
					orElseThrow(() -> new GlobalExcepcion(603, USER_DONT_EXISTS));
			
			return new User(username, usuario.getPassword(), new ArrayList<>());
			
	}

	public List<Usuario> buscarUsuarios(String username) {
		return this.userRepository.findAllContainsUsername(username).
				orElseThrow(() -> new GlobalExcepcion(615, USUARIOS_NO_ENCONTRADOS));
	}
	
	
	
}
