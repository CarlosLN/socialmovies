package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioPelicula;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioPeliculaID;
import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Pelicula.MComentarioAmigoEnFichaPelicula;
import com.socialmovies.SocialMovies.model.Pelicula.MComentarioPeliculaEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.repository.ComentarioPeliculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ComentarioPeliculaService {

    @Autowired
    private ComentarioPeliculaRepository comentarioPeliculaRepository;

    @Autowired
    private AmistadesService amistadesService;

    //Guardar/Actualizar/Eliminar un comentario de una pelicula
    public void crearActualizarEliminarComentario(Usuario u, Pelicula p, String comentario){
        ComentarioPelicula comentarioPelicula = this.comentarioPeliculaRepository.findByIdUsuarioAndIdPeliculaAndActivoTrue(u,p);
        if(comentarioPelicula != null){
            if(comentario.length() == 0){
                comentarioPelicula.setActivo(false);
            }else{
                comentarioPelicula.setComentarioPelicula(comentario);
                comentarioPelicula.setFechaComentario(new Date());
            }
            this.comentarioPeliculaRepository.save(comentarioPelicula);
        } else {
            if(comentario.length()>0){
                ComentarioPeliculaID comentarioPeliculaID = new ComentarioPeliculaID(u.getId(),p.getId());
                ComentarioPelicula comentarioPeliculaAux = new ComentarioPelicula(comentarioPeliculaID,u,p,comentario);
                this.comentarioPeliculaRepository.save(comentarioPeliculaAux);
            }
        }
    }

    public String obtenerComentarioDeUnUsuarioDeUnaPelicula(Usuario u, Pelicula p){
        try{
            return this.comentarioPeliculaRepository.findByIdUsuarioAndIdPeliculaAndActivoTrue(u,p).getComentarioPelicula();
        }catch(Exception e){
            return "";
        }
    }

    //Cuantos comentarios tiene una pelicula
    public int cuantosComentariosPorPelicula(Pelicula p){
        return this.comentarioPeliculaRepository.cuantosComentariosPorPelicula(p).orElse(0);
    }

    //Obtener cuantos amigos han comentado una pelicula
    public int obtenerCuantosAmigosHanComentadoUnaPelicula(Usuario usuario, Pelicula pelicula){
        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
        ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        return idsAmigos.size() != 0 ? this.comentarioPeliculaRepository.obtenerCuantosAmigosHanComentadoUnaPelicula(idsAmigos,pelicula).orElse(0) : 0;
    }

    //Obtener listado de peliculas votadas por un usuario de forma paginada
    public List<MComentarioPeliculaEnPerfilAmigo> obtenerComentariosPeliculasAmigo(Usuario usuario, Usuario amigo, String orden, int pagina, int cuantosAmigos){
        List<MComentarioPeliculaEnPerfilAmigo> comentariosPeliculaAmigos = new ArrayList<>();
        MComentarioPeliculaEnPerfilAmigo comentario = new MComentarioPeliculaEnPerfilAmigo();
        List<ComentarioPelicula> comentarios = new ArrayList<>();

        Pageable pagination = PageRequest.of(pagina, cuantosAmigos);

        if(orden.equals("FechaComentarioAsc")){
            comentarios = this.comentarioPeliculaRepository.findByIdUsuarioAndActivoTrueOrderByFechaComentarioAsc(amigo, pagination);
        }else{
            comentarios = this.comentarioPeliculaRepository.findByIdUsuarioAndActivoTrueOrderByFechaComentarioDesc(amigo, pagination);
        }

        for (ComentarioPelicula comentarioAmigo: comentarios
             ) {
            comentario = new MComentarioPeliculaEnPerfilAmigo(
                    comentarioAmigo.getIdPelicula(),
                    comentarioAmigo.getComentarioPelicula(),
                    comentarioAmigo.getFechaComentario()
            );

            comentariosPeliculaAmigos.add(comentario);
        }

        return comentariosPeliculaAmigos;
    }

    //Obtener listado de comentarios de amigos en ficha de pelicula
    public List<MComentarioAmigoEnFichaPelicula> obtenerComentarioDeAmigosDeUnaPelicula(Usuario usuario, Pelicula pelicula, int pagina, int cuantasPeliculas){
        Pageable pagination = PageRequest.of(pagina, cuantasPeliculas);
        List<MComentarioAmigoEnFichaPelicula> votacionAmigoEnFichaPeliculaList = new ArrayList<>();

        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
        ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        List<Object[]> objetos = idsAmigos.size() != 0 ? this.comentarioPeliculaRepository.obtenerComentariosDeAmigosDeUnaPelicula(idsAmigos, pelicula, pagination) : new ArrayList<>();
        MComentarioAmigoEnFichaPelicula comentarioAmigoEnFichaPelicula;
        for (Object[] object : objetos
        ) {
            comentarioAmigoEnFichaPelicula = new MComentarioAmigoEnFichaPelicula();
            comentarioAmigoEnFichaPelicula.setUsername(object[0].toString());
            comentarioAmigoEnFichaPelicula.setAvatar(object[1].toString());
            comentarioAmigoEnFichaPelicula.setFechaComentario((Date) object[2]);
            comentarioAmigoEnFichaPelicula.setComentario(object[3].toString());
            votacionAmigoEnFichaPeliculaList.add(comentarioAmigoEnFichaPelicula);
        }
        return votacionAmigoEnFichaPeliculaList;
    }
    
    public List<ComentarioPelicula> obtenerUltimosComentariosTimeLine(List<Long> usuarios, MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.comentarioPeliculaRepository.obtenerLosUltimosComentariosAntesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.comentarioPeliculaRepository.obtenerLosUltimosComentariosDespuesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.comentarioPeliculaRepository.obtenerLosUltimosComentariosTimeLine(usuarios,10);
        }
    }

    public int darDeBajaAltaComentariosPeliculas(Usuario usuario, boolean altaBaja){
        return this.comentarioPeliculaRepository.darDeBajaAltaComentariosPeliculas(usuario, altaBaja);
    }

}
