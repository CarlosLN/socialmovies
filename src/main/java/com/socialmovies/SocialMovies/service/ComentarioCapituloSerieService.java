package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioCapituloSerie;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioCapituloSerieID;
import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioPelicula;
import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Serie.MComentarioAmigoEnFichaCapituloSerie;
import com.socialmovies.SocialMovies.model.Serie.MComentarioCapituloSerieEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.repository.ComentarioCapituloSerieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ComentarioCapituloSerieService {

    @Autowired
    private ComentarioCapituloSerieRepository comentarioCapituloSerieRepository;

    @Autowired
    private AmistadesService amistadesService;

    public void crearActualizarEliminarComentarioCapituloSerie(Usuario usuario, CapituloSerie capituloSerie, String comentario){
        ComentarioCapituloSerie comentarioCapituloSerie = this.comentarioCapituloSerieRepository.findByIdUsuarioAndIdCapituloSerieAndActivoTrue(usuario, capituloSerie);
        if(comentarioCapituloSerie != null){
            if(comentario.length() == 0){
                comentarioCapituloSerie.setActivo(false);
            }else{
                comentarioCapituloSerie.setComentarioCapituloSerie(comentario);
                comentarioCapituloSerie.setFechaComentario(new Date());
            }
            this.comentarioCapituloSerieRepository.save(comentarioCapituloSerie);
        }else{
            if(comentario.length()>0){
                ComentarioCapituloSerieID comentarioCapituloSerieID = new ComentarioCapituloSerieID(usuario.getId(),capituloSerie.getId());
                ComentarioCapituloSerie comentarioCapituloSerieAux = new ComentarioCapituloSerie(comentarioCapituloSerieID,usuario,capituloSerie,comentario);
                this.comentarioCapituloSerieRepository.save(comentarioCapituloSerieAux);
            }
        }
    }

    public String obtenerComentarioDeUnUsuarioDeUnCapituloSerie(Usuario usuario, CapituloSerie capituloSerie){
        try{
            return this.comentarioCapituloSerieRepository.findByIdUsuarioAndIdCapituloSerieAndActivoTrue(usuario,capituloSerie).getComentarioCapituloSerie();
        }catch (Exception e){
            return "";
        }
    }

    public int obtenerCuantosAmigosHanComentadoCapitulosDeUnaSerie(Usuario usuario, Serie serie){
        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
        ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        return idsAmigos.size() != 0 ? this.comentarioCapituloSerieRepository.cuantosComentariosPorCapituloDeUnaSerie(idsAmigos, serie) : 0;
    }

    public List<MComentarioCapituloSerieEnPerfilAmigo> obtenerComentarioCapituloSerieAmigo(Usuario amigo, String orden, int pagina, int cuantosAmigos){
        List<MComentarioCapituloSerieEnPerfilAmigo> comentarioCapituloSerieEnPerfilAmigos = new ArrayList<>();
        MComentarioCapituloSerieEnPerfilAmigo comentario = new MComentarioCapituloSerieEnPerfilAmigo();
        List<Object[]> comentarios = new ArrayList<>();

        Pageable pagination = PageRequest.of(pagina, cuantosAmigos);

        if(orden.equals("FechaComentarioAsc")){
            comentarios = this.comentarioCapituloSerieRepository.findByIdUsuarioAndActivoTrueOrderByFechaComentarioAsc(amigo, pagination);
        }else{
            comentarios = this.comentarioCapituloSerieRepository.findByIdUsuarioAndActivoTrueOrderByFechaComentarioDesc(amigo, pagination);
        }

        for (Object[] object: comentarios
             ) {
            comentario = new MComentarioCapituloSerieEnPerfilAmigo(
                    Long.parseLong(object[0].toString()),
                    object[1].toString(),
                    (Date) object[2],
                    object[3].toString(),
                    Integer.parseInt(object[4].toString()),
                    Integer.parseInt(object[5].toString()),
                    object[6].toString(),
                    object[7].toString(),
                    (Date) object[8]
            );

            comentarioCapituloSerieEnPerfilAmigos.add(comentario);
        }

        return comentarioCapituloSerieEnPerfilAmigos;
    }

    public List<MComentarioAmigoEnFichaCapituloSerie> obtenerComentarioDeAmigosDeUnaSerie(Usuario usuario, Serie serie, int pagina, int cuantasPeliculas){
        Pageable pagination = PageRequest.of(pagina, cuantasPeliculas);
        List<MComentarioAmigoEnFichaCapituloSerie> comentarioAmigoEnFichaCapituloSeries = new ArrayList<>();

        List<Amistad> amistades = this.amistadesService.obtenerAmistades(usuario);
        List<Long> idsAmigos = new ArrayList<>();

        for (Amistad amistad: amistades
        ) {
            if(amistad.getIdUserSolicitante().getId() == usuario.getId()){
                idsAmigos.add(amistad.getIdUserSolicitado().getId());
            }else{
                idsAmigos.add(amistad.getIdUserSolicitante().getId());
            }
        }

        List<Object[]> objetos = idsAmigos.size() != 0 ? this.comentarioCapituloSerieRepository.obtenerComentariosDeAmigosDeLosCapitulosDeUnaSerie(idsAmigos, serie, pagination) : new ArrayList<>();

        for (Object[] object: objetos
             ) {
            comentarioAmigoEnFichaCapituloSeries.add(new MComentarioAmigoEnFichaCapituloSerie(
                    object[0].toString(),
                    object[1].toString(),
                    object[2].toString(),
                    (Date) object[3],
                    Integer.parseInt(object[4].toString()),
                    Integer.parseInt(object[5].toString()),
                    object[6].toString()
            ));
        }

        return comentarioAmigoEnFichaCapituloSeries;
    }

    public List<ComentarioCapituloSerie> obtenerUltimosComentariosTimeLine(List<Long> usuarios, MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.comentarioCapituloSerieRepository.obtenerLosUltimosComentariosAntesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.comentarioCapituloSerieRepository.obtenerLosUltimosComentariosDespuesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.comentarioCapituloSerieRepository.obtenerLosUltimosComentariosTimeLine(usuarios,10);
        }
    }

    public int darDeBajaAltaComentariosCapitulosSeries(Usuario usuario, boolean altaBaja){
        return this.comentarioCapituloSerieRepository.darDeBajaAltaComentariosCapitulosSeries(usuario, altaBaja);
    }
}
