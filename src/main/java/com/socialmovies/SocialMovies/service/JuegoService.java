package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.PreguntasJuego;
import com.socialmovies.SocialMovies.entity.RespuestasJuego.RespuestasJuego;
import com.socialmovies.SocialMovies.entity.RespuestasJuego.RespuestasJuegoID;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Juego.*;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.repository.JuegoRepository;
import com.socialmovies.SocialMovies.repository.PreguntaJuegoRepository;
import com.socialmovies.SocialMovies.repository.RespuestaPreguntaJuegoRepository;
import com.socialmovies.SocialMovies.service.exceptions.GlobalExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.socialmovies.SocialMovies.constantes.Constantes.*;


@Service("servicioJuego")
public class JuegoService {

    @Autowired
    private JuegoRepository juegoRepository;

    @Autowired
    private PreguntaJuegoRepository preguntaJuegoRepository;

    @Autowired
    private RespuestaPreguntaJuegoRepository respuestaPreguntaJuegoRepository;

    public boolean crearJuego(MCrearJuego juego) {
        try {
            juego.setFecha_creacion(new Date());
            juego.setActivo(true);
            JuegoAdivinarPeliculaSerie juegoAdivinarPeliculaSerie = this.juegoRepository.save(juego.devolverJuego());

            for (PreguntasJuego pregunta : juego.getPreguntasJuego()
            ) {
                pregunta.setIdJuego(juegoAdivinarPeliculaSerie);
                pregunta.setFecha_creacion(new Date());
                pregunta.setActivo(true);
                this.preguntaJuegoRepository.save(pregunta);
                Thread.sleep(1000);
            }

            return true;
        } catch (Exception e) {
            throw new GlobalExcepcion(621, JUEGO_NO_GUARDADO);
        }
    }

    public JuegoAdivinarPeliculaSerie buscarJuego(long idJuego) {
        return this.juegoRepository.findByIdJuegoAndActivoTrue(idJuego).orElseThrow(
                () -> new GlobalExcepcion(624, JUEGO_NO_EXISTE));
    }


    /**
     * Guarda la particiopacion en el juego de un usuario
     * @param usuario
     * @param respuestasDelJuego
     */
    public int participarEnUnJuego(Usuario usuario, List<MRespuestasJuegoUsuario> respuestasDelJuego){
        PreguntasJuego pregunta = null;
        for (MRespuestasJuegoUsuario respuesta: respuestasDelJuego
             ) {
            pregunta = this.preguntaJuegoRepository.findByIdAndActivoTrue(respuesta.getIdPreguntaJuego()).
                    orElseThrow(() -> new GlobalExcepcion(625, PARTIDA_NO_GUARDADA));
            RespuestasJuego respuestasJuego = new RespuestasJuego(respuesta);
            RespuestasJuegoID respuestasJuegoID = new RespuestasJuegoID(usuario.getId(), pregunta.getId());
            respuestasJuego.setId_respuesta(respuestasJuegoID);
            respuestasJuego.setIdPreguntaJuego(pregunta);
            respuestasJuego.setIdUsuario(usuario);
            respuestasJuego.setFechaRespuesta(new Date());
            respuestasJuego.setActivo(true);
            this.respuestaPreguntaJuegoRepository.save(respuestasJuego);
        }

        if(pregunta != null){
            return this.calcularPuntos(this.respuestaPreguntaJuegoRepository.obtenerLasRespuestasDeUnUsuarioParaUnJuego(
                    usuario, pregunta.getIdJuego()).orElse(new ArrayList<RespuestasJuego>()));
        }else{
            return -1;
        }
    }

    /**
     * Devuelve un listado con todos los juegos que hay y con la puntuacion que haya obtenido el usuario y la fecha
     * en caso de que haya participado
     * @param usuario
     * @return
     */
    public List<MJuegoDelListadoDeJuegos> devolverListadoDeJuegosParaUnUsuario(Usuario usuario) {

        List<MJuegoDelListadoDeJuegos> listadoDeJuegos = new ArrayList<>();

        List<JuegoAdivinarPeliculaSerie> listaDeJuegosActivos = this.juegoRepository.findByActivoTrue();

        for (JuegoAdivinarPeliculaSerie juego : listaDeJuegosActivos
        ) {
            MJuegoDelListadoDeJuegos juegoDelListadoDeJuegos = new MJuegoDelListadoDeJuegos(juego);
            List<RespuestasJuego> respuestasJuegosList = this.respuestaPreguntaJuegoRepository.
                    obtenerLasRespuestasDeUnUsuarioParaUnJuego(usuario, juego).orElse(new ArrayList<>());

            if(respuestasJuegosList.size()>0){
                juegoDelListadoDeJuegos.setMiPuntuacion(this.calcularPuntos(respuestasJuegosList));
                juegoDelListadoDeJuegos.setMiFechaParticipacion(respuestasJuegosList.get(0).getFechaRespuesta());
            }

            listadoDeJuegos.add(juegoDelListadoDeJuegos);
        }

        return listadoDeJuegos;
    }


    /**
     * Devuelve un juego para un usuario con las respuestas que dió en caso de haber participado en el,
     * si no tan solo las preguntas para que pueda participar
     * @param juego
     * @param usuario
     * @return
     */
    public MJuegoParaUnUsuario devolverUnJuegoParaUnUsuario(JuegoAdivinarPeliculaSerie juego, Usuario usuario) {

        List<PreguntasJuego> preguntasJuegos = this.preguntaJuegoRepository.
                findByIdJuegoAndActivoTrue(juego).orElse(new ArrayList<>());
        List<RespuestasJuego> respuestasJuegos = this.respuestaPreguntaJuegoRepository.
                findByIdUsuarioAndIdPreguntaJuegoAndActivoTrue(usuario, preguntasJuegos).orElse(new ArrayList<>());
        
        if (respuestasJuegos.size() == 0) {
            return new MJuegoParaUnUsuario(juego, preguntasJuegos);
        } else {
            MJuegoParaUnUsuario miJuego = new MJuegoParaUnUsuario(juego, preguntasJuegos, this.conversorRespuestas(respuestasJuegos));
            miJuego.setMiPuntuacion(this.calcularPuntos(respuestasJuegos));
            miJuego.setMiFechaParticipacion(respuestasJuegos.get(0).getFechaRespuesta());
            return miJuego;
        }

    }


    //TODO Metodo que devuelve el listado de los usuarios y su clasificacion en el ranking
    public List<MUsuarioRankingJuego> devolverRakingdeUsuariosEnJuegos(int pagina, int cuantosPorPagina) {
        Pageable paginator = PageRequest.of(pagina, cuantosPorPagina);
        List<MUsuarioRankingJuego> mUsuarioRankingJuegosList = new ArrayList<>();

        List<Long> usuarios = this.respuestaPreguntaJuegoRepository.obtenerListadoUsuariosQueHanParticipado(paginator).
                orElse(new ArrayList<>());

        for (Long usuario: usuarios
             ) {
            List<RespuestasJuego> respuestasDelUsuario = this.respuestaPreguntaJuegoRepository.obtenerTodasLasRespuestasDelUsuario(usuario).
                    orElse(new ArrayList<>());

            mUsuarioRankingJuegosList.add(new MUsuarioRankingJuego(respuestasDelUsuario.get(0).getIdUsuario().getUsername(), this.calcularPuntos(respuestasDelUsuario)));
        }

        Collections.sort(mUsuarioRankingJuegosList);

        return mUsuarioRankingJuegosList;

    }

    public List<RespuestasJuego> obtenerUltimasPaticipacionesTimeLine(List<Long> usuarios, MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.respuestaPreguntaJuegoRepository.obtenerLasUltimasParticipacionesJuegosAntesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.respuestaPreguntaJuegoRepository.obtenerLasUltimasParticipacionesJuegosDespuesLimiteFechaTimeLine(usuarios,10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.respuestaPreguntaJuegoRepository.obtenerLasUltimasParticipacionesJuegosTimeLine(usuarios,10);
        }
    }

    public List<JuegoAdivinarPeliculaSerie> obtenenerJuegosTimeLine (MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.juegoRepository.obtenerJuegoAntesLimiteFechaTimeLine(10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.juegoRepository.obtenerJuegoDespuesLimiteFechaTimeLine(10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.juegoRepository.obenerJuegoTimeLine(10);
        }
    }


    /**
     * Con este metodo se calculan los puntos totales de un listado de respuestas pasadas
     * @param respuestasJuegos
     * @return
     */
    private int calcularPuntos(List<RespuestasJuego> respuestasJuegos) {

        int puntos = 0;

        for (RespuestasJuego respuestaDelJugador : respuestasJuegos
        ) {
            if (respuestaDelJugador.getRespuestaCorrecta().equals(respuestaDelJugador.getRespuesta())) {

                switch (respuestaDelJugador.getIdPreguntaJuego().getIdJuego().getDificultad()){
                    case "facil":
                        switch (respuestaDelJugador.getTiempoTardado()) {
                            case 1:
                            case 2:
                                puntos += 800;
                                break;
                            case 3:
                            case 4:
                                puntos += 700;
                                break;
                            case 5:
                            case 6:
                                puntos += 600;
                                break;
                            case 7:
                            case 8:
                                puntos += 500;
                                break;
                            case 9:
                            case 10:
                                puntos += 400;
                                break;
                            case 11:
                            case 12:
                                puntos += 200;
                                break;
                            case 13:
                            case 14:
                                puntos += 100;
                                break;
                            case 15:
                                puntos += 50;
                                break;
                        }
                        break;
                    case "media":
                        switch (respuestaDelJugador.getTiempoTardado()) {
                            case 1:
                            case 2:
                                puntos += 900;
                                break;
                            case 3:
                            case 4:
                                puntos += 800;
                                break;
                            case 5:
                            case 6:
                                puntos += 700;
                                break;
                            case 7:
                            case 8:
                                puntos += 600;
                                break;
                            case 9:
                            case 10:
                                puntos += 500;
                                break;
                            case 11:
                            case 12:
                                puntos += 250;
                                break;
                            case 13:
                            case 14:
                                puntos += 125;
                                break;
                            case 15:
                                puntos += 75;
                                break;
                        }
                        break;
                    case "dificil":
                        switch (respuestaDelJugador.getTiempoTardado()) {
                            case 1:
                            case 2:
                                puntos += 1000;
                                break;
                            case 3:
                            case 4:
                                puntos += 900;
                                break;
                            case 5:
                            case 6:
                                puntos += 800;
                                break;
                            case 7:
                            case 8:
                                puntos += 700;
                                break;
                            case 9:
                            case 10:
                                puntos += 600;
                                break;
                            case 11:
                            case 12:
                                puntos += 400;
                                break;
                            case 13:
                            case 14:
                                puntos += 200;
                                break;
                            case 15:
                                puntos += 100;
                                break;
                        }
                        break;
                }

            }
        }

        return puntos;
    }

    /**
     * Con este metodo se convierte un array de Respuestas del juego a MRespuestasJuegoUsuario
     * @param respuestas
     * @return
     */
    private List<MRespuestasJuegoUsuario> conversorRespuestas(List<RespuestasJuego> respuestas){

        List<MRespuestasJuegoUsuario> mRespuestasJuegoUsuarios = new ArrayList<>();

        for (RespuestasJuego respuestasJuego: respuestas
             ) {
            mRespuestasJuegoUsuarios.add(new MRespuestasJuegoUsuario(
                    respuestasJuego.getIdPreguntaJuego().getId(),
                    respuestasJuego.getTiempoTardado(),
                    respuestasJuego.getRespuesta(),
                    respuestasJuego.getRespuestaCorrecta()));
        }

        return  mRespuestasJuegoUsuarios;
    }


    public int darDeBajaAltaParticipacionesJuego(Usuario usuario, boolean altaBaja){
        return this.respuestaPreguntaJuegoRepository.darDeBajaAltaParticipacionesJuego(usuario, altaBaja);
    }

}
