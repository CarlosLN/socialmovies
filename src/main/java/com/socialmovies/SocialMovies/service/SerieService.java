package com.socialmovies.SocialMovies.service;

import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionCapituloSerie;
import com.socialmovies.SocialMovies.model.Serie.*;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.repository.CapituloSerieRepository;
import com.socialmovies.SocialMovies.repository.SerieRepository;
import com.socialmovies.SocialMovies.repository.VotacionCapituloSerieRepository;
import com.socialmovies.SocialMovies.service.exceptions.GlobalExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.socialmovies.SocialMovies.constantes.Constantes.SERIE_DONT_EXISTS;
import static com.socialmovies.SocialMovies.constantes.Constantes.SERIE_NO_GUARDADA;

@Service("servicioSerie")
public class SerieService {

    @Autowired
    private SerieRepository serieRepository;

    @Autowired
    private VotacionCapituloSerieRepository votacionCapituloSerieRepository;

    @Autowired
    private CapituloSerieRepository capituloSerieRepository;

    @Autowired
    private AmistadesService amistadesService;

    @Autowired
    private VotacionCapituloService votacionCapituloService;

    @Autowired
    private ComentarioCapituloSerieService comentarioCapituloSerieService;

    @Autowired
    private UsuarioService usuarioService;

    public boolean crearSerie(MCrearSerie serie) {
        try {
            serie.setFecha_incluida(new Date());
            serie.setActiva(true);
            Serie s = this.serieRepository.save(serie.devolverDatosSerie());
            for (CapituloSerie capitulo : serie.getCapituloSerieList()
            ) {
                capitulo.setIdSerie(s);
                capitulo.setFechaIncluido(new Date());
                capitulo.setActivo(true);
                this.capituloSerieRepository.save(capitulo);
                Thread.sleep(1000);
            }

            return true;
        } catch (Exception e) {
            throw new GlobalExcepcion(621, SERIE_NO_GUARDADA);
        }
    }

    public MSeriesBuscadasEncontradas buscarSerie(String nombreSerie, int pagina, int cuantasPorPagina) {
        return new MSeriesBuscadasEncontradas(
                this.obtenerCuantasSeriesBuscadas(nombreSerie),
                this.obtenerListadoSeriesBuscadas(nombreSerie, pagina, cuantasPorPagina)
        );
    }

    public List<MTemporada> obtenerTemporadasDeUnaSerie(Serie serie, Usuario u){
        List<MTemporada> temporadas = new ArrayList<>();
        for (Integer temporada: this.capituloSerieRepository.obtenerTemporadasDeUnaSerie(serie).orElse(new ArrayList<>())) {
            temporadas.add(new MTemporada(
                    temporada,
                    this.votacionCapituloSerieRepository.cuantosVotosPorTemporadaSerie(serie, temporada),
                    this.votacionCapituloSerieRepository.mediaVotosTemporadaSerie(serie, temporada).orElse(0.0),
                    this.capituloSerieRepository.obtenerCuantosCapitulosTieneUnaTemporada(serie, temporada),
                    new ArrayList<>()
                    ));
        }

        return temporadas;
    }

    public List<MCapituloSerie> obtenerCapitulosDeUnaTemporadaDeUnaSerie(Serie serie, Usuario u, int temporada){
        List<MCapituloSerie> capituloSeries = new ArrayList<>();

        for (CapituloSerie capitulo:this.capituloSerieRepository.
                findByIdSerieAndNumeroTemporadaAndActivoTrueOrderByNumeroCapituloAsc(serie, temporada).orElse(new ArrayList<>())
             ) {
            VotacionCapituloSerie votacionCapituloSerie = this.votacionCapituloSerieRepository.findByIdUsuarioAndIdCapituloSerieAndActivoTrue(u,capitulo);
            if(votacionCapituloSerie == null){
                capituloSeries.add(new MCapituloSerie(
                        capitulo,
                        this.votacionCapituloSerieRepository.cuantosVotosPorCapitulosTemporadaSerie(serie, temporada, capitulo),
                        this.votacionCapituloSerieRepository.mediaVotosCapituloTemporadaSerie(serie, temporada, capitulo).orElse(0.0),
                        -1,
                        null,
                        this.comentarioCapituloSerieService.obtenerComentarioDeUnUsuarioDeUnCapituloSerie(u, capitulo)
                ));
            }else{
                capituloSeries.add(new MCapituloSerie(
                        capitulo,
                        this.votacionCapituloSerieRepository.cuantosVotosPorCapitulosTemporadaSerie(serie, temporada, capitulo),
                        this.votacionCapituloSerieRepository.mediaVotosCapituloTemporadaSerie(serie, temporada, capitulo).orElse(0.0),
                        (int)votacionCapituloSerie.getVotacion(),
                        votacionCapituloSerie.getFechaVotacion(),
                        this.comentarioCapituloSerieService.obtenerComentarioDeUnUsuarioDeUnCapituloSerie(u, capitulo)
                ));
            }

        }

        return capituloSeries;
    }

    public MTemporada obtenerTemporadaYSusCapitulosDeUnaSerie(Serie serie, Usuario u, int temporada){
        return new MTemporada(
                temporada,
                this.votacionCapituloSerieRepository.cuantosVotosPorTemporadaSerie(serie, temporada),
                this.votacionCapituloSerieRepository.mediaVotosTemporadaSerie(serie, temporada).orElse(0.0),
                this.capituloSerieRepository.obtenerCuantosCapitulosTieneUnaTemporada(serie, temporada),
                this.obtenerCapitulosDeUnaTemporadaDeUnaSerie(serie, u, temporada)
        );
    }

    public MSerie buscarSerieConUsuario(Usuario u, Serie s, int cuantosAmigos){
        return new MSerie(
                s,
                this.obtenerTemporadasDeUnaSerie(s,u),
                this.votacionCapituloSerieRepository.mediaVotosSerie(s).orElse(0.0),
                this.votacionCapituloSerieRepository.cuantosVotosPorSerie(s),
                this.votacionCapituloSerieRepository.mediaMisVotosDeUnaSerie(u,s).orElse(-1.0),
                this.votacionCapituloService.obtenerVotacionesDeAmigosDeUnaSerie(u, s, 0, cuantosAmigos),
                this.votacionCapituloService.obtenerCuantosAmigosHanVotadoUnaSerie(u, s),
                this.comentarioCapituloSerieService.obtenerComentarioDeAmigosDeUnaSerie(u,s,0,cuantosAmigos),
                this.comentarioCapituloSerieService.obtenerCuantosAmigosHanComentadoCapitulosDeUnaSerie(u,s)
                );
    }

    public Serie obtenerSeriePorId(long idSerie){
        return this.serieRepository.findByIdAndActivaTrue(idSerie).orElseThrow(
                ()-> new GlobalExcepcion(622,SERIE_DONT_EXISTS)
        );
    }

    public MSerieRecomendadaMejorvaloradaMasvotada obtenerSerieRecomendada(Usuario usuario, Serie serie){
        List<Object[]> objects = null;
        if(serie==null){
            objects = this.serieRepository.obtenerSerieRecomendada(usuario,0).orElse(null);
        }else{
            objects = this.serieRepository.obtenerSerieRecomendada(usuario,serie.getId()).orElse(null);
        }

        try {
            Object[] object = objects.get(0);
            return new MSerieRecomendadaMejorvaloradaMasvotada(
                    Long.parseLong(object[0].toString()),
                    object[1].toString(),
                    object[2].toString(),
                    (Date)object[3],
                    Double.parseDouble(object[5].toString()),
                    Integer.parseInt(object[4].toString()),
                    -1
            );
        }catch (Exception e){
            return null;
        }

    }

    public MSerieRecomendadaMejorvaloradaMasvotada obtenerSerieMejorValorada(Usuario usuario, int pagina, int cuantosPorPagina){
        Pageable paginator = PageRequest.of(pagina, cuantosPorPagina);
        List<Object[]> objects = this.serieRepository.obtenerSerieMejorValorada(paginator).orElse(new ArrayList<>());

        try{
            Object[] object = objects.get(0);
            double miVoto = this.votacionCapituloSerieRepository.mediaMisVotosDeUnaSerie(usuario,
                    this.obtenerSeriePorId(Long.parseLong(object[0].toString()))).orElse(-1.0);

            return new MSerieRecomendadaMejorvaloradaMasvotada(
                    Long.parseLong(object[0].toString()),
                    object[1].toString(),
                    object[2].toString(),
                    (Date)object[3],
                    Double.parseDouble(object[5].toString()),
                    Integer.parseInt(object[4].toString()),
                    miVoto
            );

        }catch (Exception e){
            return null;
        }
    }

    public MSerieRecomendadaMejorvaloradaMasvotada obtenerSerieMasVotada(Usuario usuario, int pagina, int cuantosPorPagina){
        Pageable paginator = PageRequest.of(pagina, cuantosPorPagina);
        List<Object[]> objects = this.serieRepository.obtenerSerieMasVotada(paginator).orElse(new ArrayList<>());

        try{
            Object[] object = objects.get(0);
            double miVoto = this.votacionCapituloSerieRepository.mediaMisVotosDeUnaSerie(usuario,
                    this.obtenerSeriePorId(Long.parseLong(object[0].toString()))).orElse(-1.0);

            return new MSerieRecomendadaMejorvaloradaMasvotada(
                    Long.parseLong(object[0].toString()),
                    object[1].toString(),
                    object[2].toString(),
                    (Date)object[3],
                    Double.parseDouble(object[5].toString()),
                    Integer.parseInt(object[4].toString()),
                    miVoto
            );

        }catch (Exception e){
            return null;
        }
    }

    public CapituloSerie obtenerCapituloSeriePorId(long idCapituloSerie){
        return this.capituloSerieRepository.findByIdAndActivoTrue(idCapituloSerie).orElseThrow(
                ()-> new GlobalExcepcion(622,SERIE_DONT_EXISTS)
        );
    }

    private List<MSerieBuscada> obtenerListadoSeriesBuscadas(String parteBuscada, int pagina, int cuantasPorPagina) {
        Pageable paginator = PageRequest.of(pagina, cuantasPorPagina);
        List<Object[]> objects = this.serieRepository.obtenerListBusquedaSeries(parteBuscada, paginator).orElse(null);

        List<MSerieBuscada> seriesEncontradas = new ArrayList<>();

        if (objects != null) {
            for (Object[] object : objects
            ) {
                seriesEncontradas.add(new MSerieBuscada(
                                Long.parseLong(object[0].toString()),
                                object[1].toString(),
                                object[2].toString(),
                                (Date) object[3]
                        )
                );
            }
        }

        return seriesEncontradas;
    }

    private int obtenerCuantasSeriesBuscadas(String parteBuscada) {
        List<Integer> cuantos = this.serieRepository.obtenerCuantasBusquedaSeries(parteBuscada).orElse(new ArrayList<>());
        return cuantos.size();
    }

    public List<CapituloSerie> obtenerCapitulosSeriesTimeLine(MTimeLine timeLineActual, String cuando){
        switch (cuando){
            case "antes":
                return this.capituloSerieRepository.obtenerCapituloSerieAntesLimiteFechaTimeLine(10, timeLineActual.getFechaPrimerElemento());
            case "despues":
                return this.capituloSerieRepository.obtenerCapituloSerieDespuesLimiteFechaTimeLine(10, timeLineActual.getFechaUltimoElemento());
            default:
                return this.capituloSerieRepository.obenerCapituloSerieTimeLine(10);
        }
    }
}
