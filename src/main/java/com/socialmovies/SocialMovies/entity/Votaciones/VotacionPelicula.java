package com.socialmovies.SocialMovies.entity.Votaciones;

import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Votaciones")
public class VotacionPelicula implements Serializable {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "idUsuario", column = @Column(name = "id_usuario", nullable = false)),
            @AttributeOverride(name = "idPelicula", column = @Column(name = "id_pelicula", nullable = false))
    })
    private VotacionPeliculaID id_votacion;

    @ManyToOne()
    @JoinColumn(name = "id_usuario", nullable = false, insertable = false, updatable = false)
    private Usuario idUsuario;

    @ManyToOne()
    @JoinColumn(name = "id_pelicula", nullable = false, insertable = false, updatable = false)
    private Pelicula idPelicula;

    private double votacion;

    @Column(name = "fecha_votacion")
    private Date fechaVotacion;

    private boolean activo;

    public VotacionPelicula(){}

    public VotacionPelicula(VotacionPeliculaID id_votacion, Usuario idUsuario, Pelicula idPelicula, double votacion) {
        this.id_votacion = id_votacion;
        this.idUsuario = idUsuario;
        this.idPelicula = idPelicula;
        this.votacion = votacion;
        this.fechaVotacion = new Date();
        this.activo = true;
    }

    public VotacionPeliculaID getId_votacion() {
        return id_votacion;
    }

    public void setId_votacion(VotacionPeliculaID id_votacion) {
        this.id_votacion = id_votacion;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Pelicula getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(Pelicula idPelicula) {
        this.idPelicula = idPelicula;
    }

    public double getVotacion() {
        return votacion;
    }

    public void setVotacion(double votacion) {
        this.votacion = votacion;
    }

    public Date getFecha_votacion() {
        return fechaVotacion;
    }

    public void setFecha_votacion(Date fecha_votacion) {
        this.fechaVotacion = fecha_votacion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
