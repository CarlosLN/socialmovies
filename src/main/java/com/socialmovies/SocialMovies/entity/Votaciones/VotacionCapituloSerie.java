package com.socialmovies.SocialMovies.entity.Votaciones;

import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="Votaciones_Capitulos")
public class VotacionCapituloSerie implements Serializable {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "idUsuario", column = @Column(name = "id_usuario", nullable = false)),
            @AttributeOverride(name = "idCapitulo", column = @Column(name = "id_capitulo", nullable = false))
    })
    private VotacionCapituloSerieID id_votacion;

    @ManyToOne()
    @JoinColumn(name = "id_usuario", nullable = false, insertable = false, updatable = false)
    private Usuario idUsuario;

    @ManyToOne()
    @JoinColumn(name = "id_capitulo", nullable = false, insertable = false, updatable = false)
    private CapituloSerie idCapituloSerie;

    private double votacion;

    @Column(name = "fecha_votacion")
    private Date fechaVotacion;

    private boolean activo;

    public VotacionCapituloSerie(){}

    public VotacionCapituloSerie(VotacionCapituloSerieID id_votacion, Usuario idUsuario, CapituloSerie idCapituloSerie, double votacion) {
        this.id_votacion = id_votacion;
        this.idUsuario = idUsuario;
        this.idCapituloSerie = idCapituloSerie;
        this.votacion = votacion;
        this.fechaVotacion = new Date();
        this.activo = true;
    }

    public VotacionCapituloSerieID getId_votacion() {
        return id_votacion;
    }

    public void setId_votacion(VotacionCapituloSerieID id_votacion) {
        this.id_votacion = id_votacion;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public CapituloSerie getIdCapituloSerie() {
        return idCapituloSerie;
    }

    public void setIdCapituloSerie(CapituloSerie idCapituloSerie) {
        this.idCapituloSerie = idCapituloSerie;
    }

    public double getVotacion() {
        return votacion;
    }

    public void setVotacion(double votacion) {
        this.votacion = votacion;
    }

    public Date getFechaVotacion() {
        return fechaVotacion;
    }

    public void setFechaVotacion(Date fechaVotacion) {
        this.fechaVotacion = fechaVotacion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
