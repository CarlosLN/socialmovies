package com.socialmovies.SocialMovies.entity.Votaciones;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class VotacionCapituloSerieID implements Serializable {

    @Column(name="id_usuario", nullable = false)
    private long idUsuario;

    @Column(name="id_capitulo", nullable = false)
    private long idCapitulo;

    public VotacionCapituloSerieID(){}

    public VotacionCapituloSerieID(long idUsuario, long idCapitulo) {
        this.idUsuario = idUsuario;
        this.idCapitulo = idCapitulo;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdCapitulo() {
        return idCapitulo;
    }

    public void setIdCapitulo(long idCapitulo) {
        this.idCapitulo = idCapitulo;
    }
}
