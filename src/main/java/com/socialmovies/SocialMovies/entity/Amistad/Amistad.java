package com.socialmovies.SocialMovies.entity.Amistad;

import com.socialmovies.SocialMovies.entity.Usuario;

import java.util.Date;
import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "AMISTADES")
public class Amistad implements Serializable{

	/* ATRIBUTOS */
//	@GeneratedValue
//	@Id
//	@Column(name = "ID_AMISTAD", unique = true)
//	private long id;
	
	@EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "idUserSolicitante", column = @Column(name = "ID_USER_SOLICITANTE", nullable = false)),
            @AttributeOverride(name = "idUserSolicitado", column = @Column(name = "ID_USER_SOLICITADO", nullable = false)) })
	private AmistadID id;

	@Column(name = "ACTIVO")
	private boolean activo;

	@Column(name = "FECHA")
	private Date fecha;
	
	@Column(name = "PETICION_ACEPTADA")
	private boolean peticionAceptada;

	@ManyToOne()
	//@ManyToOne(cascade = CascadeType.ALL)
	//@MapsId
	@JoinColumn(name = "ID_USER_SOLICITANTE", nullable = false, insertable = false, updatable = false)
	private Usuario idUserSolicitante;

	@ManyToOne()
	//@ManyToOne(cascade = CascadeType.ALL)
	//@MapsId
	@JoinColumn(name = "ID_USER_SOLICITADO", nullable = false, insertable = false, updatable = false)
	private Usuario idUserSolicitado;

	/* CONSTRUCTORES */
	public Amistad() {
		this.activo = true;
		this.peticionAceptada = false;
		this.fecha = new Date();
	}

	public Amistad(AmistadID id, boolean activo, Date fecha, boolean peticionAceptada, Usuario idUserSolicitante,
				   Usuario idUserSolicitado) {
		super();
		this.id = id;
		this.activo = activo;
		this.fecha = fecha;
		this.peticionAceptada = peticionAceptada;
		this.idUserSolicitante = idUserSolicitante;
		this.idUserSolicitado = idUserSolicitado;
	}
	

	/* GET & SET */
	public AmistadID getId() {
		return id;
	}

	public void setId(AmistadID id) {
		this.id = id;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public boolean isPeticionAceptada() {
		return peticionAceptada;
	}

	public void setPeticionAceptada(boolean peticionAceptada) {
		this.peticionAceptada = peticionAceptada;
	}

	public Usuario getIdUserSolicitante() {
		return idUserSolicitante;
	}

	public void setIdUserSolicitante(Usuario idUserSolicitante) {
		this.idUserSolicitante = idUserSolicitante;
	}

	public Usuario getIdUserSolicitado() {
		return idUserSolicitado;
	}

	public void setIdUserSolicitado(Usuario idUserSolicitado) {
		this.idUserSolicitado = idUserSolicitado;
	}
	

}
