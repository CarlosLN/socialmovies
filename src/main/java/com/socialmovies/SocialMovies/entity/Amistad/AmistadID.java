package com.socialmovies.SocialMovies.entity.Amistad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AmistadID implements Serializable{

	@Column(name = "ID_USER_SOLICITANTE", nullable = false)
	private long idUserSolicitante;
	
	@Column(name = "ID_USER_SOLICITADO", nullable = false)
	private long idUserSolicitado;

	public AmistadID() {
	}
	
	public AmistadID(long idUserSolicitante, long idUserSolicitado) {
		super();
		this.idUserSolicitante = idUserSolicitante;
		this.idUserSolicitado = idUserSolicitado;
	}

	public long getIdUserSolicitante() {
		return idUserSolicitante;
	}

	public void setIdUserSolicitante(long idUserSolicitante) {
		this.idUserSolicitante = idUserSolicitante;
	}

	public long getIdUserSolicitado() {
		return idUserSolicitado;
	}

	public void setIdUserSolicitado(long idUserSolicitado) {
		this.idUserSolicitado = idUserSolicitado;
	}
	
}
