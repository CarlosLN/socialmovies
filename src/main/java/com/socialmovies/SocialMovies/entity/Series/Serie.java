package com.socialmovies.SocialMovies.entity.Series;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SERIES")
public class Serie implements Serializable {

    @GeneratedValue
    @Id
    @Column(name = "ID_SERIE", unique = true)
    private long id;
    private String titulo;
    private String sinopsis;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_estreno;
    private String direccion;
    private String reparto;
    private String poster;
    private String duracion;
    private String trailer;
    private String genero;
    private String distribuidoras;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_incluida;
    private boolean activa;

    public Serie(){}

    public Serie(long id, String titulo, String sinopsis, Date fecha_estreno, String direccion, String reparto, String poster, String duracion, String trailer, String genero, String distribuidoras, Date fecha_incluida, boolean activa) {
        this.id = id;
        this.titulo = titulo;
        this.sinopsis = sinopsis;
        this.fecha_estreno = fecha_estreno;
        this.direccion = direccion;
        this.reparto = reparto;
        this.poster = poster;
        this.duracion = duracion;
        this.trailer = trailer;
        this.genero = genero;
        this.distribuidoras = distribuidoras;
        this.fecha_incluida = fecha_incluida;
        this.activa = activa;
    }

    public Serie(String titulo, String sinopsis, Date fecha_estreno, String direccion, String reparto, String poster, String duracion, String trailer, String genero, String distribuidoras, Date fecha_incluida, boolean activa) {
        this.titulo = titulo;
        this.sinopsis = sinopsis;
        this.fecha_estreno = fecha_estreno;
        this.direccion = direccion;
        this.reparto = reparto;
        this.poster = poster;
        this.duracion = duracion;
        this.trailer = trailer;
        this.genero = genero;
        this.distribuidoras = distribuidoras;
        this.fecha_incluida = fecha_incluida;
        this.activa = activa;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Date getFecha_estreno() {
        return fecha_estreno;
    }

    public void setFecha_estreno(Date fecha_estreno) {
        this.fecha_estreno = fecha_estreno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getReparto() {
        return reparto;
    }

    public void setReparto(String reparto) {
        this.reparto = reparto;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDistribuidoras() {
        return distribuidoras;
    }

    public void setDistribuidoras(String distribuidoras) {
        this.distribuidoras = distribuidoras;
    }

    public Date getFecha_incluida() {
        return fecha_incluida;
    }

    public void setFecha_incluida(Date fecha_incluida) {
        this.fecha_incluida = fecha_incluida;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }
}
