package com.socialmovies.SocialMovies.entity.Series;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Capitulos_Series")
public class CapituloSerie {

    @GeneratedValue
    @Id
    @Column(name = "ID_CAPITULO", unique = true)
    private long id;
    @ManyToOne()
    @JoinColumn(name = "id_serie")
    private Serie idSerie;
    private String tituloCapitulo;
    private int numeroTemporada;
    private int numeroCapitulo;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIncluido;
    private boolean activo;

    public CapituloSerie(){}

    public CapituloSerie(long id, Serie idSerie, String tituloCapitulo, int numeroTemporada, int numeroCapitulo, Date fechaIncluido, boolean activo) {
        this.id = id;
        this.idSerie = idSerie;
        this.tituloCapitulo = tituloCapitulo;
        this.numeroTemporada = numeroTemporada;
        this.numeroCapitulo = numeroCapitulo;
        this.fechaIncluido = fechaIncluido;
        this.activo = activo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Serie getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(Serie idSerie) {
        this.idSerie = idSerie;
    }

    public String getTituloCapitulo() {
        return tituloCapitulo;
    }

    public void setTituloCapitulo(String tituloCapitulo) {
        this.tituloCapitulo = tituloCapitulo;
    }

    public int getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(int numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public int getNumeroCapitulo() {
        return numeroCapitulo;
    }

    public void setNumeroCapitulo(int numeroCapitulo) {
        this.numeroCapitulo = numeroCapitulo;
    }

    public Date getFechaIncluido() {
        return fechaIncluido;
    }

    public void setFechaIncluido(Date fechaIncluido) {
        this.fechaIncluido = fechaIncluido;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
