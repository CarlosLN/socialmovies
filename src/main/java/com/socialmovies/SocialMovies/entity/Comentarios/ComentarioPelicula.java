package com.socialmovies.SocialMovies.entity.Comentarios;

import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Comentarios")
public class ComentarioPelicula implements Serializable {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "idUsuario", column = @Column(name = "id_usuario", nullable = false)),
            @AttributeOverride(name = "idPelicula", column = @Column(name = "id_pelicula", nullable = false))
    })
    private ComentarioPeliculaID id_comentario;

    @ManyToOne()
    @JoinColumn(name = "id_usuario", nullable = false, insertable = false, updatable = false)
    private Usuario idUsuario;

    @ManyToOne()
    @JoinColumn(name = "id_pelicula", nullable = false, insertable = false, updatable = false)
    private Pelicula idPelicula;

    private String comentarioPelicula;

    @Column(name = "fecha_comentario")
    private Date fechaComentario;

    private boolean activo;

    public ComentarioPelicula(){}

    public ComentarioPelicula(ComentarioPeliculaID id_comentario, Usuario idUsuario, Pelicula idPelicula, String comentarioPelicula) {
        this.id_comentario = id_comentario;
        this.idUsuario = idUsuario;
        this.idPelicula = idPelicula;
        this.comentarioPelicula = comentarioPelicula;
        this.fechaComentario = new Date();
        this.activo = true;
    }

    public ComentarioPeliculaID getId_comentario() {
        return id_comentario;
    }

    public void setId_comentario(ComentarioPeliculaID id_comentario) {
        this.id_comentario = id_comentario;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Pelicula getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(Pelicula idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getComentarioPelicula() {
        return comentarioPelicula;
    }

    public void setComentarioPelicula(String comentarioPelicula) {
        this.comentarioPelicula = comentarioPelicula;
    }

    public Date getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(Date fechaComentario) {
        this.fechaComentario = fechaComentario;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
