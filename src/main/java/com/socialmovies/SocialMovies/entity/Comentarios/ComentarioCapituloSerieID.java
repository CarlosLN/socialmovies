package com.socialmovies.SocialMovies.entity.Comentarios;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ComentarioCapituloSerieID implements Serializable {

    @Column(name="id_usuario", nullable = false)
    private long idUsuario;

    @Column(name="id_capitulo_serie", nullable = false)
    private long idCapituloSerie;

    public ComentarioCapituloSerieID(){}

    public ComentarioCapituloSerieID(long idUsuario, long idCapituloSerie) {
        super();
        this.idUsuario = idUsuario;
        this.idCapituloSerie = idCapituloSerie;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdCapituloSerie() {
        return idCapituloSerie;
    }

    public void setIdCapituloSerie(long idCapituloSerie) {
        this.idCapituloSerie = idCapituloSerie;
    }
}
