package com.socialmovies.SocialMovies.entity.Comentarios;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ComentarioPeliculaID implements Serializable {

    @Column(name="id_usuario", nullable = false)
    private long idUsuario;

    @Column(name="id_pelicula", nullable = false)
    private long idPelicula;

    public ComentarioPeliculaID(){}

    public ComentarioPeliculaID(long idUsuario, long idPelicula) {
        super();
        this.idUsuario = idUsuario;
        this.idPelicula = idPelicula;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(long idPelicula) {
        this.idPelicula = idPelicula;
    }
}
