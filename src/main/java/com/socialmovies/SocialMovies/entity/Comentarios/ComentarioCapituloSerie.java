package com.socialmovies.SocialMovies.entity.Comentarios;

import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Usuario;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Comentarios_Capitulos")
public class ComentarioCapituloSerie implements Serializable {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "idUsuario", column = @Column(name = "id_usuario", nullable = false)),
            @AttributeOverride(name = "idCapituloSerie", column = @Column(name = "id_capitulo_serie", nullable = false))
    })
    private ComentarioCapituloSerieID id_comentario;

    @ManyToOne()
    @JoinColumn(name = "id_Usuario", nullable = false, insertable = false, updatable = false)
    private Usuario idUsuario;

    @ManyToOne()
    @JoinColumn(name = "id_capitulo_serie", nullable = false, insertable = false, updatable = false)
    private CapituloSerie idCapituloSerie;

    private String comentarioCapituloSerie;

    @Column(name = "fecha_comentario")
    private Date fechaComentario;

    private boolean activo;

    public ComentarioCapituloSerie(){}

    public ComentarioCapituloSerie(ComentarioCapituloSerieID id_comentario, Usuario idUsuario, CapituloSerie idCapituloSerie, String comentarioCapituloSerie) {
        this.id_comentario = id_comentario;
        this.idUsuario = idUsuario;
        this.idCapituloSerie = idCapituloSerie;
        this.comentarioCapituloSerie = comentarioCapituloSerie;
        this.fechaComentario = new Date();
        this.activo = true;
    }

    public ComentarioCapituloSerieID getId_comentario() {
        return id_comentario;
    }

    public void setId_comentario(ComentarioCapituloSerieID id_comentario) {
        this.id_comentario = id_comentario;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public CapituloSerie getIdCapituloSerie() {
        return idCapituloSerie;
    }

    public void setIdCapituloSerie(CapituloSerie idCapituloSerie) {
        this.idCapituloSerie = idCapituloSerie;
    }

    public String getComentarioCapituloSerie() {
        return comentarioCapituloSerie;
    }

    public void setComentarioCapituloSerie(String comentarioCapituloSerie) {
        this.comentarioCapituloSerie = comentarioCapituloSerie;
    }

    public Date getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(Date fechaComentario) {
        this.fechaComentario = fechaComentario;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
