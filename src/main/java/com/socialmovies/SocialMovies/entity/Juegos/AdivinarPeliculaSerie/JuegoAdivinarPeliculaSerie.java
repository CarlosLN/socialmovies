package com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "JuegoAdivinarPeliculaSerie")
public class JuegoAdivinarPeliculaSerie implements Serializable {

    @GeneratedValue
    @Id
    @Column(name = "ID_JUEGO", unique = true)
    private long idJuego;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_creacion;
    private String titulo;
    private String dificultad; /** Puede ser facil, media o dificil **/
    private boolean activo;

    public JuegoAdivinarPeliculaSerie(){}

    public JuegoAdivinarPeliculaSerie(long idJuego, Date fecha_creacion, String titulo, String dificultad, boolean activo) {
        this.idJuego = idJuego;
        this.fecha_creacion = fecha_creacion;
        this.titulo = titulo;
        this.dificultad = dificultad;
        this.activo = activo;
    }

    public JuegoAdivinarPeliculaSerie(Date fecha_creacion, String titulo, String dificultad, boolean activo) {
        this.fecha_creacion = fecha_creacion;
        this.titulo = titulo;
        this.dificultad = dificultad;
        this.activo = activo;
    }

    public long getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(long idJuego) {
        this.idJuego = idJuego;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDificultad() {
        return dificultad;
    }

    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

}
