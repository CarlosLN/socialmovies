package com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Preguntas_Juego")
public class PreguntasJuego implements Serializable {

    @GeneratedValue
    @Id
    @Column(name = "ID_PREGUNTA", unique = true)
    private long id;
    @ManyToOne()
    @JoinColumn(name = "id_juego")
    private JuegoAdivinarPeliculaSerie idJuego;
    private String titulo;
    private String pregunta_imagen;
    private String opciones;
    private String respuestaCorrecta;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_creacion;
    private boolean activo;

    public PreguntasJuego(){}

    public PreguntasJuego(long id, JuegoAdivinarPeliculaSerie idJuego, String titulo, String pregunta_imagen, String opciones, String respuestaCorrecta, Date fecha_creacion, boolean activo) {
        this.id = id;
        this.idJuego = idJuego;
        this.titulo = titulo;
        this.pregunta_imagen = pregunta_imagen;
        this.opciones = opciones;
        this.respuestaCorrecta = respuestaCorrecta;
        this.fecha_creacion = fecha_creacion;
        this.activo = activo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public JuegoAdivinarPeliculaSerie getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(JuegoAdivinarPeliculaSerie idJuego) {
        this.idJuego = idJuego;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPregunta_imagen() {
        return pregunta_imagen;
    }

    public void setPregunta_imagen(String pregunta_imagen) {
        this.pregunta_imagen = pregunta_imagen;
    }

    public String getOpciones() {
        return opciones;
    }

    public void setOpciones(String opciones) {
        this.opciones = opciones;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
