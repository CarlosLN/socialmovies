package com.socialmovies.SocialMovies.entity.RespuestasJuego;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class RespuestasJuegoID implements Serializable {

    @Column(name="id_usuario", nullable = false)
    private long idUsuario;

    @Column(name="id_pregunta", nullable = false)
    private long idPreguntaJuego;

    public RespuestasJuegoID(){}

    public RespuestasJuegoID(long idUsuario, long idPreguntaJuego) {
        this.idUsuario = idUsuario;
        this.idPreguntaJuego = idPreguntaJuego;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdPreguntaJuego() {
        return idPreguntaJuego;
    }

    public void setIdPreguntaJuego(long idPreguntaJuego) {
        this.idPreguntaJuego = idPreguntaJuego;
    }
}
