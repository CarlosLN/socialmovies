package com.socialmovies.SocialMovies.entity.RespuestasJuego;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.PreguntasJuego;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Juego.MRespuestasJuegoUsuario;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="Respuestas_Juego")
public class RespuestasJuego implements Serializable {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "idUsuario", column = @Column(name = "id_usuario", nullable = false)),
            @AttributeOverride(name = "idPreguntaJuego", column = @Column(name = "id_pregunta", nullable = false))
    })
    private RespuestasJuegoID id_respuesta;

    @ManyToOne()
    @JoinColumn(name = "id_usuario", nullable = false, insertable = false, updatable = false)
    private Usuario idUsuario;

    @ManyToOne()
    @JoinColumn(name = "id_pregunta", nullable = false, insertable = false, updatable = false)
    private PreguntasJuego idPreguntaJuego;

    private int tiempoTardado;
    private String respuesta;
    private String respuestaCorrecta;
    @Column(name = "fecha_respuesta")
    private Date fechaRespuesta;
    private boolean activo;

    public RespuestasJuego(){}

    public RespuestasJuego(RespuestasJuegoID id_respuesta, Usuario idUsuario, PreguntasJuego idPreguntaJuego, int tiempoTardado, String respuesta, String respuestaCorrecta, Date fechaRespuesta, boolean activo) {
        this.id_respuesta = id_respuesta;
        this.idUsuario = idUsuario;
        this.idPreguntaJuego = idPreguntaJuego;
        this.tiempoTardado = tiempoTardado;
        this.respuesta = respuesta;
        this.respuestaCorrecta = respuestaCorrecta;
        this.fechaRespuesta = fechaRespuesta;
        this.activo = activo;
    }

    public RespuestasJuego(MRespuestasJuegoUsuario mRespuestasJuegoUsuario){
        this.tiempoTardado = mRespuestasJuegoUsuario.getTiempoTardado();
        this.respuesta = mRespuestasJuegoUsuario.getRespuesta();
        this.respuestaCorrecta = mRespuestasJuegoUsuario.getRespuestaCorrecta();
    }

    public RespuestasJuegoID getId_respuesta() {
        return id_respuesta;
    }

    public void setId_respuesta(RespuestasJuegoID id_respuesta) {
        this.id_respuesta = id_respuesta;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public PreguntasJuego getIdPreguntaJuego() {
        return idPreguntaJuego;
    }

    public void setIdPreguntaJuego(PreguntasJuego idPreguntaJuego) {
        this.idPreguntaJuego = idPreguntaJuego;
    }

    public int getTiempoTardado() {
        return tiempoTardado;
    }

    public void setTiempoTardado(int tiempoTardado) {
        this.tiempoTardado = tiempoTardado;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Date getFechaRespuesta() {
        return fechaRespuesta;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public void setFechaRespuesta(Date fechaRespuesta) {
        this.fechaRespuesta = fechaRespuesta;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

}
