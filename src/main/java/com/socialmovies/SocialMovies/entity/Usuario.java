package com.socialmovies.SocialMovies.entity;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionPelicula;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable{

	/* ATRIBUTOS */
	@GeneratedValue
	@Id
	@Column(name = "ID_USUARIO", unique = true)
	private long id;

	@Column(name = "ACTIVO")
	private boolean activo;

	@Column(name = "EMAIL", unique = true)
	private String email;

	@Column(name = "USERNAME", unique = true)
	private String username;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "AVATAR")
	private String avatar;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idUserSolicitado")
    private Set<Amistad> amigosSolicitados = new HashSet<Amistad>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idUserSolicitante")
    private Set<Amistad> amigosSolicitantes = new HashSet<Amistad>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idUsuario")
	private Set<VotacionPelicula> peliculasVotadas = new HashSet<VotacionPelicula>();


	/* CONSTRUCTORES */
	public Usuario() {

	}

	public Usuario(long id, boolean activo, String email, String username, String password, String avatar) {
		this.id = id;
		this.activo = activo;
		this.email = email;
		this.username = username;
		this.password = password;
		this.avatar = avatar;
	}

	/* GET & SET */
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Set<Amistad> getAmigosSolicitados() {
		return amigosSolicitados;
	}

	public void setAmigosSolicitados(Set<Amistad> amigosSolicitados) {
		this.amigosSolicitados = amigosSolicitados;
	}

	public Set<Amistad> getAmigosSolicitantes() {
		return amigosSolicitantes;
	}

	public void setAmigosSolicitantes(Set<Amistad> amigosSolicitantes) {
		this.amigosSolicitantes = amigosSolicitantes;
	}

	public Set<VotacionPelicula> getPeliculasVotadas() {
		return peliculasVotadas;
	}

	public void setPeliculasVotadas(Set<VotacionPelicula> peliculasVotadas) {
		this.peliculasVotadas = peliculasVotadas;
	}
}
