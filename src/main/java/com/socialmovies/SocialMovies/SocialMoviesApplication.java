package com.socialmovies.SocialMovies;

import com.socialmovies.SocialMovies.controller.JuegoController;
import com.socialmovies.SocialMovies.controller.PeliculaController;
import com.socialmovies.SocialMovies.controller.SerieController;
import com.socialmovies.SocialMovies.controller.exceptions.*;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.socialmovies.SocialMovies.controller.UsuarioController;

@SpringBootApplication
public class SocialMoviesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialMoviesApplication.class, args);
	}

	@Bean
	public ResourceConfig jerseyConfig() {
		final ResourceConfig resourceConfig = new ResourceConfig();

		resourceConfig.register(UsuarioController.class);
		resourceConfig.register(PeliculaController.class);
		resourceConfig.register(SerieController.class);
		resourceConfig.register(JuegoController.class);
		resourceConfig.register(GlobalExceptionMapper.class);
		
		return resourceConfig;
	}
	
}
