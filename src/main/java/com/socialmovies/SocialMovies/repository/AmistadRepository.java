package com.socialmovies.SocialMovies.repository;

import org.springframework.data.domain.Pageable;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.socialmovies.SocialMovies.entity.Amistad.Amistad;
import com.socialmovies.SocialMovies.entity.Usuario;

import javax.transaction.Transactional;

@Repository("amistadesRepository")
public interface AmistadRepository extends JpaRepository<Amistad,Serializable> {

	boolean existsByIdAndPeticionAceptadaAndActivo(Serializable id, boolean peticion_aceptada, boolean activo);

	public abstract List<Amistad> findByIdUserSolicitadoAndPeticionAceptadaAndActivo(Usuario id, boolean aceptado, boolean activo);

	public abstract List<Amistad> findByIdUserSolicitanteAndPeticionAceptadaAndActivo(Usuario id, boolean aceptado, boolean activo);

	@Query("select a from Amistad a where (a.idUserSolicitante = ?1 or a.idUserSolicitado = ?1) and a.peticionAceptada = ?2 and a.activo = ?3")
	List<Amistad> obtenerAmistades(Usuario id, boolean aceptado, boolean activo);

	boolean  existsByIdUserSolicitadoAndIdUserSolicitanteAndPeticionAceptadaAndActivo(Usuario id, Usuario id2, boolean aceptado, boolean activo);

	@Query("select count(a.idUserSolicitante) from Amistad a where (a.idUserSolicitante = ?1 or a.idUserSolicitado = ?1) and a.peticionAceptada = true and a.activo = true")
	Optional<Integer> obtenerNumAmistadesDeUnAmigo(Usuario usuarioAmigo);

	@Query(nativeQuery = true, value = "SELECT u.username FROM usuario as u " +
			"where (" +
			"u.id_Usuario IN (select a.ID_USER_SOLICITADO from amistades as a where a.id_user_Solicitante = ?1 and a.activo = true and a.peticion_aceptada = true) " +
			"or " +
			"u.id_Usuario IN (select a.id_user_Solicitante from amistades as a where a.ID_USER_SOLICITADO = ?1 and a.activo = true and a.peticion_aceptada = true)) " +
			"and u.activo= true " +
			"order by u.username asc")
	Optional<List<String>> obtenerListadoAmigosDeUnUsuario(Usuario usuario, Pageable p);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "update amistades set activo = ?2 where id_user_solicitado = ?1 or id_user_solicitante = ?1")
	int darDeBajaAltaAmistades(Usuario usuario, boolean altaBaja);
}