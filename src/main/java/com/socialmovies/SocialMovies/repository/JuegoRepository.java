package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface JuegoRepository extends JpaRepository<JuegoAdivinarPeliculaSerie, Serializable> {

    Optional<JuegoAdivinarPeliculaSerie> findByIdJuegoAndActivoTrue(long id);

    List<JuegoAdivinarPeliculaSerie> findByActivoTrue();



    @Query(nativeQuery = true, value = "SELECT * FROM juegoadivinarpeliculaserie where activo = true order by fecha_creacion desc limit ?1")
    List<JuegoAdivinarPeliculaSerie> obenerJuegoTimeLine(int limite);

    @Query(nativeQuery = true, value = "SELECT * FROM juegoadivinarpeliculaserie where activo = true and fecha_creacion > ?2 order by fecha_creacion desc limit ?1")
    List<JuegoAdivinarPeliculaSerie> obtenerJuegoAntesLimiteFechaTimeLine(int limite, Date fecha);

    @Query(nativeQuery = true, value = "SELECT * FROM juegoadivinarpeliculaserie where activo = true and fecha_creacion < ?2 order by fecha_creacion desc limit ?1")
    List<JuegoAdivinarPeliculaSerie> obtenerJuegoDespuesLimiteFechaTimeLine(int limite, Date fecha);

}
