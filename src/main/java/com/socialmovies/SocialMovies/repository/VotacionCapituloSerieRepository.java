package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionCapituloSerie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository("votacionCapituloSerieRepository")
public interface VotacionCapituloSerieRepository extends JpaRepository<VotacionCapituloSerie, Serializable> {

    @Query(nativeQuery = true, value = "select CAST(AVG(v.votacion) AS DECIMAL(10,1)) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where " +
            "v.id_usuario = ?1 and " +
            "c.id_serie = ?2 and " +
            "c.activo = true and " +
            "v.activo = true")
    Optional<Double> mediaMisVotosDeUnaSerie(Usuario u, Serie s);

    @Query(nativeQuery = true, value = "select CAST(AVG(v.votacion) AS DECIMAL(10,1)) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where " +
            "c.id_serie = ?1 and " +
            "c.activo = true and " +
            "v.activo = true")
    Optional<Double> mediaVotosSerie(Serie s);

    @Query(nativeQuery = true, value = "select CAST(AVG(v.votacion) AS DECIMAL(10,1)) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where " +
            "c.id_serie = ?1 and " +
            "c.numeroTemporada = ?2 and " +
            "c.activo = true and " +
            "v.activo = true")
    Optional<Double> mediaVotosTemporadaSerie(Serie s, int temporada);

    @Query(nativeQuery = true, value = "select CAST(AVG(v.votacion) AS DECIMAL(10,1)) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where " +
            "c.id_serie = ?1 and " +
            "c.numeroTemporada = ?2 and " +
            "v.id_capitulo = ?3 and " +
            "c.activo = true and " +
            "v.activo = true")
    Optional<Double> mediaVotosCapituloTemporadaSerie(Serie s, int temporada, CapituloSerie capitulo);

    VotacionCapituloSerie findByIdUsuarioAndIdCapituloSerieAndActivoTrue(Usuario u, CapituloSerie cs);

    @Query(nativeQuery = true, value = "select count(*) from (select count(v.votacion) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where " +
            "c.id_serie = ?1 and " +
            "c.activo = true and " +
            "v.activo = true " +
            "group by v.id_usuario) r")
    int cuantosVotosPorSerie(Serie s);

    @Query(nativeQuery = true, value = "select count(*) from (select count(v.votacion) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where " +
            "c.id_serie = ?1 and " +
            "c.numeroTemporada = ?2 and " +
            "c.activo = true and " +
            "v.activo = true " +
            "group by v.id_usuario) r")
    int cuantosVotosPorTemporadaSerie(Serie s, int temporada);

    @Query(nativeQuery = true, value = "select count(v.votacion) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where " +
            "c.id_serie = ?1 and " +
            "c.numeroTemporada = ?2 and " +
            "v.id_capitulo = ?3 and " +
            "c.activo = true and " +
            "v.activo = true")
    int cuantosVotosPorCapitulosTemporadaSerie(Serie s, int temporada, CapituloSerie capitulo);

    @Query(nativeQuery = true, value = "select s.id_serie as idSerie, " +
            "s.titulo as titulo, " +
            "s.fecha_estreno as fechaEstreno, " +
            "s.poster as poster, " +
            "cs.numeroTemporada, " +
            "cs.numeroCapitulo, " +
            "cs.tituloCapitulo, " +
            "v.votacion as voto, " +
            "v.fecha_votacion as fechaVoto, " +
            "cs.ID_CAPITULO as idCapitulo " +
            "from  votaciones_capitulos v " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "inner join series s on cs.id_serie = s.id_serie " +
            "where v.Id_usuario = ?1 " +
            "and v.activo = true " +
            "and cs.activo = true " +
            "and s.activa = true " +
            "order by voto desc, titulo asc, numeroTemporada asc, numeroCapitulo asc")
    List<Object[]> obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorVotacionDesc(Usuario usuario, Pageable pageable);

    @Query(nativeQuery = true, value = "select s.id_serie as idSerie, " +
            "s.titulo as titulo, " +
            "s.fecha_estreno as fechaEstreno, " +
            "s.poster as poster, " +
            "cs.numeroTemporada, " +
            "cs.numeroCapitulo, " +
            "cs.tituloCapitulo, " +
            "v.votacion as voto, " +
            "v.fecha_votacion as fechaVoto, " +
            "cs.ID_CAPITULO as idCapitulo " +
            "from  votaciones_capitulos v " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "inner join series s on cs.id_serie = s.id_serie " +
            "where v.Id_usuario = ?1 " +
            "and v.activo = true " +
            "and cs.activo = true " +
            "and s.activa = true " +
            "order by voto asc, titulo asc, numeroTemporada asc, numeroCapitulo asc")
    List<Object[]> obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorVotacionAsc(Usuario usuario, Pageable pageable);

    @Query(nativeQuery = true, value = "select s.id_serie as idSerie, " +
            "s.titulo as titulo, " +
            "s.fecha_estreno as fechaEstreno, " +
            "s.poster as poster, " +
            "cs.numeroTemporada, " +
            "cs.numeroCapitulo, " +
            "cs.tituloCapitulo, " +
            "v.votacion as voto, " +
            "v.fecha_votacion as fechaVoto, " +
            "cs.ID_CAPITULO as idCapitulo " +
            "from  votaciones_capitulos v " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "inner join series s on cs.id_serie = s.id_serie " +
            "where v.Id_usuario = ?1 " +
            "and v.activo = true " +
            "and cs.activo = true " +
            "and s.activa = true " +
            "order by fechaVoto desc, titulo asc, numeroTemporada asc, numeroCapitulo asc")
    List<Object[]> obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorFechaVotacionDesc(Usuario usuario, Pageable pageable);

    @Query(nativeQuery = true, value = "select s.id_serie as idSerie, " +
            "s.titulo as titulo, " +
            "s.fecha_estreno as fechaEstreno, " +
            "s.poster as poster, " +
            "cs.numeroTemporada, " +
            "cs.numeroCapitulo, " +
            "cs.tituloCapitulo, " +
            "v.votacion as voto, " +
            "v.fecha_votacion as fechaVoto, " +
            "cs.ID_CAPITULO as idCapitulo " +
            "from  votaciones_capitulos v " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "inner join series s on cs.id_serie = s.id_serie " +
            "where v.Id_usuario = ?1 " +
            "and v.activo = true " +
            "and cs.activo = true " +
            "and s.activa = true " +
            "order by fechaVoto asc, titulo asc, numeroTemporada asc, numeroCapitulo asc")
    List<Object[]> obtenerVotacionesCapitulosSerieUnUsuarioOrdenadoPorFechaVotacionAsc(Usuario usuario, Pageable pageable);



    /*@Query(nativeQuery = true, value = "select u.username as username, u.avatar as avatar, v.fecha_votacion as fechaVoto, v.votacion as voto, cs.numeroTemporada as numTemporada, cs.numeroCapitulo as numCapitulo, cs.tituloCapitulo " +
            "from usuario u " +
            "inner join votaciones_capitulos v on u.Id_usuario = v.id_usuario " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "where u.Id_usuario IN (?1) " +
            "and cs.id_serie = ?2 " +
            "and u.activo = true " +
            "and v.activo = true " +
            "and cs.activo = true " +
            "order by fechaVoto desc")
            List<Object[]> obtenerVotacionesDeAmigosDeUnCapituloDeUnaSerie(List<Long> usuarios, Serie serie, Pageable pageable);*/
    @Query(nativeQuery = true, value = "select u.username as username, u.avatar as avatar, v.fecha_votacion as fechaVoto, CAST(AVG(v.votacion) AS DECIMAL(10,1)) as voto, cs.numeroTemporada as numTemporada, cs.numeroCapitulo as numCapitulo, cs.tituloCapitulo " +
            "from usuario u " +
            "inner join votaciones_capitulos v on u.Id_usuario = v.id_usuario " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "where u.Id_usuario IN (?1) " +
            "and cs.id_serie = ?2 " +
            "and u.activo = true " +
            "and v.activo = true " +
            "and cs.activo = true " +
            "group by u.Id_usuario " +
            "order by fechaVoto desc")
    List<Object[]> obtenerVotacionesMediaDeAmigosDeUnaSerie(List<Long> usuarios, Serie serie, Pageable pageable);

    /*@Query(nativeQuery = true, value = "select count(u.username) " +
            "from usuario u " +
            "inner join votaciones_capitulos v on u.Id_usuario = v.id_usuario " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "where u.Id_usuario IN (?1) " +
            "and cs.id_serie = ?2 " +
            "and u.activo = true " +
            "and v.activo = true " +
            "and cs.activo = true ")
    int obtenerCuantosAmigosHanVotadoUnCapituloDeUnaSerie(List<Long> usuarios, Serie serie);*/

    @Query(nativeQuery = true, value = "select count(*) from (select count(u.username) " +
            "from usuario u " +
            "inner join votaciones_capitulos v on u.Id_usuario = v.id_usuario " +
            "inner join capitulos_series cs on v.id_capitulo = cs.id_capitulo " +
            "where u.Id_usuario IN (?1) " +
            "and cs.id_serie = ?2 " +
            "and u.activo = true " +
            "and v.activo = true " +
            "and cs.activo = true " +
            "group by u.Id_usuario) r")
    int obtenerCuantosAmigosHanVotadoUnCapituloDeUnaSerie(List<Long> usuarios, Serie serie);


    @Query(nativeQuery = true, value = "select count(*) from (select count(*) from Votaciones_capitulos as v " +
            "inner join capitulos_series as c on c.id_capitulo = v.id_capitulo " +
            "where v.id_Usuario = ?1 and " +
            "c.activo = true and " +
            "v.activo = true " +
            "group by c.id_serie) " +
            "r")
    int numSeriesVotadasPorUnUsuario(Usuario u);

    @Query(nativeQuery = true, value = "select count(*) from votaciones_capitulos as v " +
            "where v.id_Usuario = ?1 " +
            "and v.activo = true")
    int numCapitulosVotadosPorUnsuario(Usuario u);

    @Query(nativeQuery = true, value = "select CAST(AVG(v.votacion) AS DECIMAL(10,1)) from Votaciones_capitulos v where v.id_Usuario = ?1 and v.activo = true")
    Optional<Double> mediaVotosDeSeriesDeUnUsuario(Usuario u);



    @Query(nativeQuery = true, value = "select * from votaciones_capitulos v where v.Id_usuario IN (?1) and v.activo = true order by v.fecha_votacion desc limit ?2")
    List<VotacionCapituloSerie> obtenerLasUltimasVotacionesTimeLine(List<Long> usuarios, int limite);

    @Query(nativeQuery = true, value = "select * from votaciones_capitulos v where v.Id_usuario IN (?1) and v.activo = true and v.fecha_votacion > ?3 order by v.fecha_votacion desc limit ?2")
    List<VotacionCapituloSerie> obtenerLasUltimasVotacionesAntesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Query(nativeQuery = true, value = "select * from votaciones_capitulos v where v.Id_usuario IN (?1) and v.activo = true and v.fecha_votacion < ?3 order by v.fecha_votacion desc limit ?2")
    List<VotacionCapituloSerie> obtenerLasUltimasVotacionesDespuesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update votaciones_capitulos set activo = ?2 where id_usuario = ?1")
    int darDeBajaAltaVotacionCapituloSerie(Usuario usuario, boolean altaBaja);

}
