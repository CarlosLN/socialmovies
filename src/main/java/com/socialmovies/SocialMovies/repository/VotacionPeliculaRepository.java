package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionPelicula;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository("votacionPeliculaRepository")
public interface VotacionPeliculaRepository extends JpaRepository<VotacionPelicula, Serializable> {

    @Query(nativeQuery = true, value = "select CAST(AVG(v.votacion) AS DECIMAL(10,1)) from Votaciones v where v.id_Pelicula = ?1 and v.activo = true")
    double mediaVotosPelicula(Pelicula p);

    VotacionPelicula findByIdUsuarioAndIdPeliculaAndActivoTrue(Usuario u, Pelicula p);

    @Query("select count(v.votacion) from VotacionPelicula v where v.idPelicula = ?1 and v.activo = true")
    int cuantosVotosPorPelicula(Pelicula p);

    ArrayList<VotacionPelicula> findByIdUsuarioAndActivoTrueOrderByVotacionDesc(Usuario u, Pageable pageable);

    ArrayList<VotacionPelicula> findByIdUsuarioAndActivoTrueOrderByVotacionAsc(Usuario u, Pageable pageable);

    ArrayList<VotacionPelicula> findByIdUsuarioAndActivoTrueOrderByFechaVotacionDesc(Usuario u, Pageable pageable);

    ArrayList<VotacionPelicula> findByIdUsuarioAndActivoTrueOrderByFechaVotacionAsc(Usuario u, Pageable pageable);

    @Query(nativeQuery = true, value = "select u.username as username, u.avatar as avatar, v.fecha_votacion as fechaVoto, v.votacion as voto " +
            "from usuario u " +
            "inner join votaciones v on u.Id_usuario = v.id_usuario " +
            "where u.Id_usuario IN (?1) " +
            "and v.id_pelicula = ?2 " +
            "and u.activo = true " +
            "and v.activo = true " +
            "order by u.username")
    Optional<List<Object[]>> obtenerVotacionesDeAmigosDeUnaPelicula(List<Long> usuarios, Pelicula pelicula, Pageable pageable);

    @Query(nativeQuery = true, value = "select count(u.username) " +
            "from usuario u " +
            "inner join votaciones v on u.Id_usuario = v.id_usuario " +
            "where u.Id_usuario IN (?1) " +
            "and v.id_pelicula = ?2 " +
            "and u.activo = true " +
            "and v.activo = true " +
            "order by u.username")
    Optional<Integer> obtenerCuantosAmigosHanVotadoUnaPelicula(List<Long> usuarios, Pelicula pelicula);

    @Query("select count (v.votacion) from VotacionPelicula v where v.idUsuario = ?1 and v.activo = true")
    Optional<Integer> numPeliculasVotadasPorUnUsuario(Usuario u);

    @Query(nativeQuery = true, value = "select CAST(AVG(v.votacion) AS DECIMAL(10,1)) from Votaciones v where v.id_Usuario = ?1 and v.activo = true")
    Optional<Double> mediaVotosDePeliculasDeUnUsuario(Usuario u);


    @Query(nativeQuery = true, value = "select * from votaciones v where v.Id_usuario IN (?1) and v.activo = true order by v.fecha_votacion desc limit ?2")
    List<VotacionPelicula> obtenerLasUltimasVotacionesTimeLine(List<Long> usuarios, int limite);

    @Query(nativeQuery = true, value = "select * from votaciones v where v.Id_usuario IN (?1) and v.activo = true and v.fecha_votacion > ?3 order by v.fecha_votacion desc limit ?2")
    List<VotacionPelicula> obtenerLasUltimasVotacionesAntesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Query(nativeQuery = true, value = "select * from votaciones v where v.Id_usuario IN (?1) and v.activo = true and v.fecha_votacion < ?3 order by v.fecha_votacion desc limit ?2")
    List<VotacionPelicula> obtenerLasUltimasVotacionesDespuesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update votaciones set activo = ?2 where id_usuario = ?1")
    int darDeBajaAltaVotacionPelicula(Usuario usuario, boolean altaBaja);
}
