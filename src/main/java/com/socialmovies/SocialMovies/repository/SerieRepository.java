package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Usuario;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Repository("serieRepository")
public interface SerieRepository extends JpaRepository<Serie, Serializable> {

    Optional<Serie> findByIdAndActivaTrue(long id);

    @Query(nativeQuery = true, value = "select s.id_serie, s.titulo, s.poster, s.fecha_estreno, " +
            "(select count(*) from (select count(v.votacion) from Votaciones_capitulos as v " +
            "inner join capitulos_series as ca on ca.id_capitulo = v.id_capitulo " +
            "where " +
            "ca.id_serie = s.ID_SERIE and " +
            "ca.activo = true and " +
            "v.activo = true " +
            "group by v.id_usuario) r) as cuantasvotaciones " +
            ", cast(avg(vc.votacion) as decimal(10,1)) as votacion_media from series s " +
            "inner join capitulos_series cs on s.id_serie = cs.id_serie " +
            "inner join votaciones_capitulos vc on cs.id_capitulo = vc.id_capitulo " +
            "where s.id_serie NOT IN ( " +
            "select ser.id_serie from series ser " +
            "inner join capitulos_series cs on ser.id_serie = cs.id_serie " +
            "inner join votaciones_capitulos vc on cs.id_capitulo = vc.id_capitulo " +
            "where vc.id_usuario = ?1 " +
            "and ser.activa = true " +
            "and cs.activo = true " +
            "and vc.activo = true) " +
            "and s.id_serie != ?2 " +
            "and s.activa = true " +
            "and cs.activo = true " +
            "and vc.activo = true " +
            "group by s.titulo " +
            "order by RAND()" +
            "LIMIT 1")
    Optional<List<Object[]>> obtenerSerieRecomendada(Usuario u, long idSerieAnterior);

    @Query(nativeQuery = true, value = "select s.id_serie, s.titulo, s.poster, s.fecha_estreno, " +
            "(select count(*) from (select count(v.votacion) from Votaciones_capitulos as v " +
            "            inner join capitulos_series as ca on ca.id_capitulo = v.id_capitulo " +
            "            where " +
            "            ca.id_serie = s.ID_SERIE and " +
            "            ca.activo = true and " +
            "            v.activo = true " +
            "            group by v.id_usuario) r) as cuantasvotaciones " +
            ", cast(avg(vc.votacion) as decimal(10,1)) as votacion_media from series s " +
            "inner join capitulos_series cs on s.id_serie = cs.id_serie " +
            "inner join votaciones_capitulos vc on cs.id_capitulo = vc.id_capitulo " +
            "and s.activa = true " +
            "and cs.activo = true " +
            "and vc.activo = true " +
            "group by s.titulo " +
            "order by votacion_media desc, cuantasvotaciones desc")
    Optional<List<Object[]>> obtenerSerieMejorValorada(Pageable pageable);

    @Query(nativeQuery = true, value = "select s.id_serie, s.titulo, s.poster, s.fecha_estreno, " +
            "(select count(*) from (select count(v.votacion) from Votaciones_capitulos as v " +
            "            inner join capitulos_series as ca on ca.id_capitulo = v.id_capitulo " +
            "            where " +
            "            ca.id_serie = s.ID_SERIE and " +
            "            ca.activo = true and " +
            "            v.activo = true " +
            "            group by v.id_usuario) r) as cuantasvotaciones " +
            ", cast(avg(vc.votacion) as decimal(10,1)) as votacion_media from series s " +
            "inner join capitulos_series cs on s.id_serie = cs.id_serie " +
            "inner join votaciones_capitulos vc on cs.id_capitulo = vc.id_capitulo " +
            "and s.activa = true " +
            "and cs.activo = true " +
            "and vc.activo = true " +
            "group by s.titulo " +
            "order by cuantasvotaciones desc, votacion_media desc ")
    Optional<List<Object[]>> obtenerSerieMasVotada(Pageable pageable);



    @Query(nativeQuery=true, value="select id_serie, titulo, poster, fecha_estreno FROM " +
            "( " +
            "   SELECT 1 AS rnk, id_serie, titulo, poster, fecha_estreno FROM series " +
            "   WHERE titulo LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 2 AS rnk, id_serie, titulo, poster, fecha_estreno FROM series " +
            "   WHERE direccion LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 3 AS rnk, id_serie, titulo, poster, fecha_estreno FROM series " +
            "   WHERE reparto LIKE %?1% and activa= true " +
            ") as tab " +
            "group by titulo " +
            "ORDER BY rnk, fecha_estreno asc ")
    Optional<List<Object[]>> obtenerListBusquedaSeries(String busqueda, Pageable p);

    @Query(nativeQuery=true, value="select count(*) FROM " +
            "( " +
            "   SELECT 1 AS rnk, titulo, fecha_estreno FROM series " +
            "   WHERE titulo LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 2 AS rnk, titulo, fecha_estreno FROM series " +
            "   WHERE direccion LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 3 AS rnk, titulo, fecha_estreno FROM series " +
            "   WHERE reparto LIKE %?1% and activa= true " +
            ") as tab " +
            "group by titulo " +
            "ORDER BY rnk, fecha_estreno asc;")
    Optional<List<Integer>> obtenerCuantasBusquedaSeries(String busqueda);

}
