package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository("capituloSerieRepository")
public interface CapituloSerieRepository extends JpaRepository<CapituloSerie, Serializable> {

    Optional<CapituloSerie> findByIdAndActivoTrue(long id);

    @Query(nativeQuery=true, value="SELECT numeroTemporada FROM capitulos_series " +
            "where id_serie = ?1 " +
            "and activo = 1 " +
            "group by numeroTemporada " +
            "order by numeroTemporada asc;")
    Optional<List<Integer>> obtenerTemporadasDeUnaSerie(Serie buscada);

    Optional<List<CapituloSerie>> findByIdSerieAndNumeroTemporadaAndActivoTrueOrderByNumeroCapituloAsc(Serie serie, int numTemporada);

    @Query("select count(c.idSerie) from CapituloSerie c where c.idSerie = ?1 and numeroTemporada = ?2 and c.activo = true")
    int obtenerCuantosCapitulosTieneUnaTemporada(Serie s, int temporada);


    @Query(nativeQuery = true, value = "select * from capitulos_series p where p.activo = true order by p.fechaIncluido desc limit ?1")
    List<CapituloSerie> obenerCapituloSerieTimeLine(int limite);

    @Query(nativeQuery = true, value = "select * from capitulos_series p where p.activo = true and p.fechaIncluido > ?2 order by p.fechaIncluido desc limit ?1")
    List<CapituloSerie> obtenerCapituloSerieAntesLimiteFechaTimeLine(int limite, Date fecha);

    @Query(nativeQuery = true, value = "select * from capitulos_series p where p.activo = true and p.fechaIncluido < ?2 order by p.fechaIncluido desc limit ?1")
    List<CapituloSerie> obtenerCapituloSerieDespuesLimiteFechaTimeLine(int limite, Date fecha);

}
