package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.PreguntasJuego;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface PreguntaJuegoRepository extends JpaRepository<PreguntasJuego, Serializable> {

    Optional<PreguntasJuego> findByIdAndActivoTrue(long id);

    Optional<List<PreguntasJuego>> findByIdJuegoAndActivoTrue(JuegoAdivinarPeliculaSerie juego);

}
