package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioPelicula;
import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository("comentarioPeliculaRepository")
public interface ComentarioPeliculaRepository extends JpaRepository<ComentarioPelicula, Serializable> {


    //Obtener cuantas peliculas ha comentado un usuario
    @Query("select count (c.comentarioPelicula) from ComentarioPelicula c where c.idUsuario = ?1 and c.activo = true")
    Optional<Integer> numPeliculasComentadasPorUnUsuario(Usuario u);

    //Obtener el comentario de un usuario de una Pelicula
    ComentarioPelicula findByIdUsuarioAndIdPeliculaAndActivoTrue(Usuario u, Pelicula p);

    //Obtener listado de peliculas comentadas por un usuario(paginado, ordenacion)
    List<ComentarioPelicula> findByIdUsuarioAndActivoTrueOrderByFechaComentarioDesc(Usuario u, Pageable p);

    List<ComentarioPelicula> findByIdUsuarioAndActivoTrueOrderByFechaComentarioAsc(Usuario u, Pageable p);


    //Obtener el listado de amigos que han comentado una pelicula(paginado)
    @Query(nativeQuery = true, value = "select u.username as username, u.avatar as avatar, c.fecha_comentario as fechaComentario, c.comentarioPelicula as comentarioPelicula " +
            "from usuario u " +
            "inner join comentarios c on u.Id_usuario = c.id_usuario " +
            "where u.Id_usuario IN (?1) " +
            "and c.id_pelicula = ?2 " +
            "and u.activo = true " +
            "and c.activo = true " +
            "order by u.username")
    List<Object[]> obtenerComentariosDeAmigosDeUnaPelicula(List<Long> usuarios, Pelicula pelicula, Pageable pageable);

    //Obtener cuantos usuarios han comentado una pelicula
    @Query("select count(c.comentarioPelicula) from ComentarioPelicula c where c.idPelicula = ?1 and c.activo=true")
    Optional<Integer> cuantosComentariosPorPelicula(Pelicula p);

    //Obtener cuantos amigos han comentado una pelicula
    @Query(nativeQuery = true, value = "select count(u.username) " +
            "from usuario u " +
            "inner join comentarios c on u.Id_usuario = c.id_usuario " +
            "where u.Id_usuario IN (?1) " +
            "and c.id_pelicula = ?2 " +
            "and u.activo = true " +
            "and c.activo = true " +
            "order by u.username")
    Optional<Integer> obtenerCuantosAmigosHanComentadoUnaPelicula(List<Long> usuarios, Pelicula pelicula);




    @Query(nativeQuery = true, value = "select * from comentarios c where c.Id_usuario IN (?1) and c.activo = true order by c.fecha_comentario desc limit ?2")
    List<ComentarioPelicula> obtenerLosUltimosComentariosTimeLine(List<Long> usuarios, int limite);

    @Query(nativeQuery = true, value = "select * from comentarios c where c.Id_usuario IN (?1) and c.activo = true and c.fecha_comentario > ?3 order by c.fecha_comentario desc limit ?2")
    List<ComentarioPelicula> obtenerLosUltimosComentariosAntesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Query(nativeQuery = true, value = "select * from comentarios c where c.Id_usuario IN (?1) and c.activo = true and c.fecha_comentario < ?3 order by c.fecha_comentario desc limit ?2")
    List<ComentarioPelicula> obtenerLosUltimosComentariosDespuesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update comentarios set activo = ?2 where id_usuario = ?1")
    int darDeBajaAltaComentariosPeliculas(Usuario usuario, boolean altaBaja);

}
