package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.PreguntasJuego;
import com.socialmovies.SocialMovies.entity.RespuestasJuego.RespuestasJuego;
import com.socialmovies.SocialMovies.entity.Usuario;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface RespuestaPreguntaJuegoRepository extends JpaRepository<RespuestasJuego, Serializable> {

    @Query(nativeQuery=true, value="SELECT * FROM respuestas_juego where id_usuario = ?1 and id_pregunta IN (?2) and activo = true;")
    Optional<List<RespuestasJuego>> findByIdUsuarioAndIdPreguntaJuegoAndActivoTrue(Usuario usuario, List<PreguntasJuego> preguntasJuegos);


    @Query(nativeQuery = true, value = "SELECT * FROM social_movies.respuestas_juego " +
            "where id_pregunta IN (select id_pregunta from preguntas_juego where id_juego = ?2 and activo = true) " +
            "and id_usuario = ?1 " +
            "and activo = true;")
    Optional<List<RespuestasJuego>>obtenerLasRespuestasDeUnUsuarioParaUnJuego(Usuario usuario, JuegoAdivinarPeliculaSerie juego);

    @Query(nativeQuery = true, value = "SELECT id_Usuario FROM respuestas_juego where activo = true group by id_Usuario")
    Optional<List<Long>>obtenerListadoUsuariosQueHanParticipado(Pageable p);

    @Query(nativeQuery = true, value = "SELECT * FROM respuestas_juego where id_usuario = ?1 and activo = true")
    Optional<List<RespuestasJuego>>obtenerTodasLasRespuestasDelUsuario(Long usuario);


    @Query(nativeQuery = true, value = "SELECT t1.* FROM respuestas_juego t1 join preguntas_juego t2 on t1.id_pregunta = t2.id_pregunta " +
            "where t1.id_usuario IN(?1) and t1.activo = true group by t1.id_usuario, t2.id_juego order by t1.fecha_respuesta desc limit ?2")
    List<RespuestasJuego> obtenerLasUltimasParticipacionesJuegosTimeLine(List<Long> usuarios, int limite);

    @Query(nativeQuery = true, value = "SELECT t1.* FROM respuestas_juego t1 join preguntas_juego t2 on t1.id_pregunta = t2.id_pregunta " +
            "where t1.id_usuario IN(?1) and t1.activo = true and t1.fecha_respuesta > ?3 group by t1.id_usuario, t2.id_juego order by t1.fecha_respuesta desc limit ?2")
    List<RespuestasJuego> obtenerLasUltimasParticipacionesJuegosAntesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Query(nativeQuery = true, value = "SELECT t1.* FROM respuestas_juego t1 join preguntas_juego t2 on t1.id_pregunta = t2.id_pregunta " +
            "where t1.id_usuario IN(?1) and t1.activo = true and t1.fecha_respuesta < ?3 group by t1.id_usuario, t2.id_juego order by t1.fecha_respuesta desc limit ?2")
    List<RespuestasJuego> obtenerLasUltimasParticipacionesJuegosDespuesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update respuestas_juego set activo = ?2 where id_usuario = ?1")
    int darDeBajaAltaParticipacionesJuego(Usuario usuario, boolean altaBaja);


}
