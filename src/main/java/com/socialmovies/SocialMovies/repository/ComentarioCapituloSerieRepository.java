package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioCapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Repository("comentarioCapituloSerieRepository")
public interface ComentarioCapituloSerieRepository extends JpaRepository<ComentarioCapituloSerie, Serializable> {

    //Obtener cuantos capitulos ha comentado un usuario
    @Query("select count (c.comentarioCapituloSerie) from ComentarioCapituloSerie c where c.idUsuario = ?1 and c.activo = true")
    int numCapitulosSeriesComentadasPorUnUsuario(Usuario u);

    //Obtener cuantas series ha comentado un usuario
    @Query(nativeQuery = true, value = "select count(*) from (select count(*) from Comentarios_capitulos as comen " +
            "inner join capitulos_series as c on c.id_capitulo = comen.id_capitulo_serie " +
            "where comen.id_Usuario = ?1 and " +
            "c.activo = true and " +
            "comen.activo = true " +
            "group by c.id_serie) " +
            "r")
    int numSeriesComentadasPorUnUsuario(Usuario u);

    //Obtener el comentario de un usuario de un Capitulo
    ComentarioCapituloSerie findByIdUsuarioAndIdCapituloSerieAndActivoTrue(Usuario u, CapituloSerie capituloSerie);

    //Obtener listado de capitulos comentadas por un usuario(paginado, ordenacion)
    @Query(nativeQuery = true, value = "select s.id_serie as idSerie, " +
            "    s.titulo as titulo, " +
            "    s.fecha_estreno as fechaEstreno, " +
            "    s.poster as poster, " +
            "    cs.numeroTemporada, " +
            "    cs.numeroCapitulo, " +
            "    cs.tituloCapitulo, " +
            "    c.comentarioCapituloSerie as comentario, " +
            "    c.fecha_Comentario as fechaComentario " +
            "    from comentarios_capitulos c " +
            "    inner join capitulos_series cs on c.id_capitulo_serie = cs.id_capitulo " +
            "    inner join series s on cs.id_serie = s.id_serie " +
            "    where c.Id_usuario = ?1 " +
            "    and c.activo = true " +
            "    and cs.activo = true " +
            "    and s.activa = true " +
            "    order by fechaComentario desc, titulo asc, numeroTemporada asc, numeroCapitulo asc")
    List<Object[]> findByIdUsuarioAndActivoTrueOrderByFechaComentarioDesc(Usuario usuario, Pageable pageable);

    @Query(nativeQuery = true, value = "select s.id_serie as idSerie, " +
            "    s.titulo as titulo, " +
            "    s.fecha_estreno as fechaEstreno, " +
            "    s.poster as poster, " +
            "    cs.numeroTemporada, " +
            "    cs.numeroCapitulo, " +
            "    cs.tituloCapitulo, " +
            "    c.comentarioCapituloSerie as comentario, " +
            "    c.fecha_Comentario as fechaComentario " +
            "    from comentarios_capitulos c " +
            "    inner join capitulos_series cs on c.id_capitulo_serie = cs.id_capitulo " +
            "    inner join series s on cs.id_serie = s.id_serie " +
            "    where c.Id_usuario = ?1 " +
            "    and c.activo = true " +
            "    and cs.activo = true " +
            "    and s.activa = true " +
            "    order by fechaComentario asc, titulo asc, numeroTemporada asc, numeroCapitulo asc")
    List<Object[]> findByIdUsuarioAndActivoTrueOrderByFechaComentarioAsc(Usuario usuario, Pageable pageable);

    //Obtener el listado de amigos que han comentado los capitulos de una serie (paginado)
    @Query(nativeQuery = true, value = "select u.username as username, u.avatar as avatar, comen.comentarioCapituloSerie as comentarioCapitulo, comen.fecha_comentario as fechaComentario, cs.numeroTemporada as numTemporada, cs.numeroCapitulo as numCapitulo, cs.tituloCapitulo " +
            "from usuario u " +
            "inner join comentarios_capitulos comen on u.Id_usuario = comen.id_usuario " +
            "inner join capitulos_series cs on comen.id_capitulo_serie = cs.id_capitulo " +
            "where u.Id_usuario IN (?1) " +
            "and cs.id_serie = ?2 " +
            "and u.activo = true " +
            "and comen.activo = true " +
            "and cs.activo = true " +
            "order by fechaComentario desc")
    List<Object[]> obtenerComentariosDeAmigosDeLosCapitulosDeUnaSerie(List<Long> usuarios, Serie serie, Pageable pageable);

    //Obtener cuantos amigos han comentado los capitulos de una serie
    @Query(nativeQuery = true, value = "select count(u.username) " +
            "from usuario u " +
            "inner join comentarios_capitulos comen on u.Id_usuario = comen.id_usuario " +
            "inner join capitulos_series cs on comen.id_capitulo_serie = cs.id_capitulo " +
            "where u.Id_usuario IN (?1) " +
            "and cs.id_serie = ?2 " +
            "and u.activo = true " +
            "and comen.activo = true " +
            "and cs.activo = true ")
    int cuantosComentariosPorCapituloDeUnaSerie(List<Long> usuarios, Serie s);



    @Query(nativeQuery = true, value = "select * from comentarios_capitulos c where c.Id_usuario IN (?1) and c.activo = true order by c.fecha_comentario desc limit ?2")
    List<ComentarioCapituloSerie> obtenerLosUltimosComentariosTimeLine(List<Long> usuarios, int limite);

    @Query(nativeQuery = true, value = "select * from comentarios_capitulos c where c.Id_usuario IN (?1) and c.activo = true and c.fecha_comentario > ?3 order by c.fecha_comentario desc limit ?2")
    List<ComentarioCapituloSerie> obtenerLosUltimosComentariosAntesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Query(nativeQuery = true, value = "select * from comentarios_capitulos c where c.Id_usuario IN (?1) and c.activo = true and c.fecha_comentario < ?3 order by c.fecha_comentario desc limit ?2")
    List<ComentarioCapituloSerie> obtenerLosUltimosComentariosDespuesLimiteFechaTimeLine(List<Long> usuarios, int limite, Date fecha);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update comentarios_capitulos set activo = ?2 where id_usuario = ?1")
    int darDeBajaAltaComentariosCapitulosSeries(Usuario usuario, boolean altaBaja);

}
