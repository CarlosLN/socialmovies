package com.socialmovies.SocialMovies.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.socialmovies.SocialMovies.entity.Usuario;

import javax.transaction.Transactional;

@Repository("usuarioRepository")
public interface UsuarioRepository extends JpaRepository<Usuario, Serializable>{

	Optional<Usuario> findByUsername(String nombre);
	
	Optional<Usuario> findByUsernameAndActivo(String nombre, boolean activo);
	
	public abstract Usuario findByEmail(String email);
	
	public abstract List<Usuario> findByActivo(boolean activo);
	
	Optional<Usuario> findByUsernameAndPassword(String username, String password);

	@Query("select u from Usuario u where u.username LIKE %?1% AND u.activo = 1")
	Optional <List<Usuario>> findAllContainsUsername(String username);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "update usuario set activo = ?2  where id_usuario = ?1")
	int darDeBajaAltaUsuario(Usuario usuario, boolean altaBaja);
	
	
}
