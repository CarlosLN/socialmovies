package com.socialmovies.SocialMovies.repository;

import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository("peliculaRepository")
public interface PeliculaRepository extends JpaRepository<Pelicula, Serializable> {

    @Query("select p from Pelicula p where p.titulo LIKE %?1% AND p.activa = 1")
    ArrayList<Pelicula> findAllContainsTitulo(String titulo);

    Optional<Pelicula> findByIdAndActivaTrue(long id);

    @Query(nativeQuery = true, value = "select p.id_pelicula, p.titulo,p.poster,p.fecha_estreno, count(v.votacion) as cuantasvotaciones, " +
            "CAST(AVG(v.votacion) AS DECIMAL(10,1)) as votacion_media from peliculas p " +
            "inner join votaciones v on p.id_pelicula = v.id_pelicula " +
            "where p.id_pelicula NOT IN (select id_pelicula from votaciones where id_usuario = ?1 and activo = true ) " +
            "and p.activa = true " +
            "and v.activo = true " +
            "and p.id_pelicula != ?2 " +
            "group by p.titulo " +
            "order by RAND() " +
            "LIMIT 1")
    Optional<List<Object[]>> obtenerPeliculaRecomendada(Usuario u, long idPeliculaAnterior);

    @Query(nativeQuery = true, value="select p.id_pelicula, p.titulo,p.poster,p.fecha_estreno, count(v.votacion) as cuantasvotaciones, CAST(AVG(v.votacion) AS DECIMAL(10,1)) as votacion_media from peliculas p " +
            "inner join votaciones v on p.id_pelicula = v.id_pelicula " +
            "and p.activa = true " +
            "and v.activo = true " +
            "group by p.titulo " +
            "order by votacion_media desc, cuantasvotaciones desc")
    Optional<List<Object[]>> obtenerPeliculaMejorValorada(Pageable p);

    @Query(nativeQuery = true, value="select p.id_pelicula, p.titulo,p.poster,p.fecha_estreno, count(v.votacion) as cuantasvotaciones, CAST(AVG(v.votacion) AS DECIMAL(10,1)) as votacion_media from peliculas p " +
            "inner join votaciones v on p.id_pelicula = v.id_pelicula " +
            "and p.activa = true " +
            "and v.activo = true " +
            "group by p.titulo " +
            "order by cuantasvotaciones desc, votacion_media desc ")
    Optional<List<Object[]>> obtenerPeliculaMasVotada(Pageable p);

    @Query(nativeQuery=true, value="select id_pelicula, titulo, poster, fecha_estreno FROM " +
            "( " +
            "   SELECT 1 AS rnk, id_pelicula, titulo, poster, fecha_estreno FROM peliculas " +
            "   WHERE titulo LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 2 AS rnk, id_pelicula, titulo, poster, fecha_estreno FROM peliculas " +
            "   WHERE direccion LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 3 AS rnk, id_pelicula, titulo, poster, fecha_estreno FROM peliculas " +
            "   WHERE reparto LIKE %?1% and activa= true " +
            ") as tab " +
            "group by titulo " +
            "ORDER BY rnk, fecha_estreno asc ")
    Optional<List<Object[]>> obtenerListBusquedaPeliculas(String busqueda,Pageable p);

    @Query(nativeQuery=true, value="select count(*) FROM " +
            "( " +
            "   SELECT 1 AS rnk, titulo, fecha_estreno FROM peliculas " +
            "   WHERE titulo LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 2 AS rnk, titulo, fecha_estreno FROM peliculas " +
            "   WHERE direccion LIKE %?1% and activa= true " +
            "   UNION all " +
            "   SELECT 3 AS rnk, titulo, fecha_estreno FROM peliculas " +
            "   WHERE reparto LIKE %?1% and activa= true " +
            ") as tab " +
            "group by titulo " +
            "ORDER BY rnk, fecha_estreno asc;")
    Optional<List<Integer>> obtenerCuantasBusquedaPeliculas(String busqueda);



    @Query(nativeQuery = true, value = "select * from peliculas p where p.activa = true order by p.fecha_incluida desc limit ?1")
    List<Pelicula> obtenerPeliculasTimeLine(int limite);

    @Query(nativeQuery = true, value = "select * from peliculas p where p.activa = true and p.fecha_incluida > ?2 order by p.fecha_incluida desc limit ?1")
    List<Pelicula> obtenerPeliculasAntesLimiteFechaTimeLine(int limite, Date fecha);

    @Query(nativeQuery = true, value = "select * from peliculas p where p.activa = true and p.fecha_incluida < ?2 order by p.fecha_incluida desc limit ?1")
    List<Pelicula> obtenerPeliculasDespuesLimiteFechaTimeLine(int limite, Date fecha);
}
