package com.socialmovies.SocialMovies.constantes;

public final class Constantes {
	/**
	 * Mensajes
	 */
	public static final String SOLICITUD_AMISTAD_ENVIADA_CORRECTAMENTE = "Solicitud de amistad enviada correctamente a ";
	public static final String PETICION_ACEPTADA = "Petición aceptada correctamente";
	public static final String PETICION_RECHAZADA = "Petición rechazada correctamente";
	public static final String AMISTAD_ELIMINADA = "Amistad eliminada correctamente";
	
	
	/**
	 * Mensajes de Excepciones
	 */
	public static final String PASS_NOT_EQUALS = "Su antigua contraseña no coincide";				//601
	public static final String USER_ALREADY_EXISTS = "Este usuario ya existe";						//602
	public static final String USER_DONT_EXISTS = "No se ha podido hacer login, compruebe que el usuario y contraseña son correctos";//603
	public static final String USER_NOT_AUTHORIZED = "Acceso no autorizado";						//604
	public static final String ERROR_UPDATE_PASSWORD = "No se ha podido actualizar su contraseña";	//605
	public static final String YA_SOIS_AMIGOS = "Ya sois amigos";									//606
	public static final String PETICION_AMISTAD_ENVIADA = "La petición ya fue enviada anteriormente";//607
	public static final String PETICION_AMISTAD_RECIBIDA = "Ya ha recibido una peticion de amistad de este usuario, revise sus solicitudes"; //608
	public static final String SIN_PETICIONES_RECIBIDAS = "No tiene ninguna peticion pendiente"; //609
	public static final String ACEPTAR_PETICION = "No se ha podido aceptar la petición de amistad"; //610
	public static final String RECHAZAR_PETICION = "No se ha podido rechazar la peticion de amistad"; //611
	public static final String ELIMINAR_AMISTAD = "No se ha eliminar la amistad"; //612
	public static final String NO_TIENES_AMISTADES = "No tienes amistades en estos momentos"; //613
	public static final String SIN_PETICIONES_ENVIADAS = "No tiene ninguna peticion enviada"; //614
	public static final String USUARIOS_NO_ENCONTRADOS = "No se han encontrado coincidencias"; //615
	public static final String ERROR_USUARIO = "Usuario no válido"; //616
	public static final String NO_PUEDE_ACCEDER_PERFIL = "No puedes acceder al perfil de este usuario"; //617
	public static final String ERROR_ACTUALIZANDO_USUARIO = "No se ha podido actualizar el usuario"; //618
	public static final String PELICULA_NO_GUARDADA = "No se ha podido crear la película"; //619
	public static final String PELICULA_DONT_EXISTS = "No se han encontrado resultados"; //620
	public static final String SERIE_NO_GUARDADA = "No se ha podido crear la serie"; //621
	public static final String SERIE_DONT_EXISTS = "No se han encontrado resultados"; //622
	public static final String JUEGO_NO_GUARDADO = "No se ha podido crear el juego"; //623
	public static final String JUEGO_NO_EXISTE = "No se han encontrado resultados"; //624
	public static final String PARTIDA_NO_GUARDADA = "Se ha producido un error guardando la partida"; //625



}
