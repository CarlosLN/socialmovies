package com.socialmovies.SocialMovies.controller;

import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Pelicula.*;
import com.socialmovies.SocialMovies.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("/peliculas")
@Produces(MediaType.APPLICATION_JSON)
public class PeliculaController {

    @Autowired
    private PeliculaService peliculaService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private VotacionPeliculaService votacionPeliculaService;

    @Autowired
    private ComentarioPeliculaService comentarioPeliculaService;

    @Autowired
    private AmistadesService amistadesService;

    @POST
    public ResponseEntity<String> crearPelicula(@RequestBody @Valid @NotNull(message = "El cuerpo de la petición no puede estar vacío.") Pelicula pelicula) {
        this.peliculaService.crearPelicula(pelicula);
        return ResponseEntity.ok(HttpStatus.ACCEPTED.toString());
    }

    @GET
    @Path("/buscarPelicula/{peliculaBuscada}")
    public ResponseEntity<MPeliculasBuscadasEncontradas> buscarPelicula(
            @PathParam("peliculaBuscada") final String peliculaBuscada,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantasPeliculas") int cuantasPeliculas
            ) {
        return ResponseEntity.ok(this.peliculaService.buscarPelicula(peliculaBuscada, pagina, cuantasPeliculas));
    }

    @GET
    @Path("/devolverPeliculaUsuario/{idPelicula}/usuario/{username}")
    public ResponseEntity<MPelicula> devolverPeliculaUsuario(
            @PathParam("idPelicula") final long idPelicula,
            @PathParam("username") final String username
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        Pelicula pelicula = this.peliculaService.obtenerPeliculaPorId(idPelicula);

        return ResponseEntity.ok(this.peliculaService.buscarPeliculaConUsuario(usuario, pelicula, 3));
    }

    @GET
    @Path("/devolverVotacionesAmigosEnFichaDePelicula/{idPelicula}/usuario/{username}")
    public ResponseEntity<List<MVotacionAmigoEnFichaPelicula>> devolverVotacionesAmigosEnFichaDePelicula(
            @PathParam("idPelicula") final long idPelicula,
            @PathParam("username") final String username,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosAmigos") int cuantosAmigos
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        Pelicula pelicula = this.peliculaService.obtenerPeliculaPorId(idPelicula);

        return ResponseEntity.ok(this.votacionPeliculaService.obtenerVotacionesDeAmigosDeUnaPelicula(usuario,pelicula,pagina,cuantosAmigos));
    }

    @GET
    @Path("/devolverComentariosAmigosEnFichaDePelicula/{idPelicula}/usuario/{username}")
    public ResponseEntity<List<MComentarioAmigoEnFichaPelicula>> devolverComentariosAmigosEnFichaDePelicula(
            @PathParam("idPelicula") final long idPelicula,
            @PathParam("username") final String username,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosAmigos") int cuantosAmigos
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        Pelicula pelicula = this.peliculaService.obtenerPeliculaPorId(idPelicula);

        return ResponseEntity.ok(this.comentarioPeliculaService.obtenerComentarioDeAmigosDeUnaPelicula(usuario,pelicula,pagina,cuantosAmigos));
    }

    @GET
    @Path("/obtenerPeliculaMejorValorada/usuario/{username}")
    public ResponseEntity<MPeliculaRecomendadaMejorvaloradaMasvotada> obtenerPeliculaMejorValorada(
            @PathParam("username") final String username,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosPorPagina") int cuantosPorPagina
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        return ResponseEntity.ok(this.peliculaService.obtenerPeliculaMejorValorada(usuario, pagina, cuantosPorPagina));
    }

    @GET
    @Path("/obtenerPeliculaMasVotada/usuario/{username}")
    public ResponseEntity<MPeliculaRecomendadaMejorvaloradaMasvotada> obtenerPeliculaMasVotada(
            @PathParam("username") final String username,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosPorPagina") int cuantosPorPagina
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        return ResponseEntity.ok(this.peliculaService.obtenerPeliculaMasVotada(usuario, pagina, cuantosPorPagina));
    }

}
