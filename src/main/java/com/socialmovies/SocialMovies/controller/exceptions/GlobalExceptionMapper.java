package com.socialmovies.SocialMovies.controller.exceptions;

import com.socialmovies.SocialMovies.service.exceptions.GlobalExcepcion;
import org.json.JSONObject;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class GlobalExceptionMapper implements ExceptionMapper<GlobalExcepcion> {

    @Override
    public Response toResponse(GlobalExcepcion e) {
        JSONObject json = new JSONObject();
        json.put("error", e.getClass().getSimpleName());
        json.put("code", e.getCodigoError());
        json.put("error_description", e.getMensajeError());

        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(json.toMap())
                .build();
    }
}
