package com.socialmovies.SocialMovies.controller;

import com.socialmovies.SocialMovies.entity.Juegos.AdivinarPeliculaSerie.JuegoAdivinarPeliculaSerie;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Juego.MCrearJuego;
import com.socialmovies.SocialMovies.model.Juego.MJuegoDelListadoDeJuegos;
import com.socialmovies.SocialMovies.model.Juego.MJuegoParaUnUsuario;
import com.socialmovies.SocialMovies.model.Juego.MUsuarioRankingJuego;
import com.socialmovies.SocialMovies.service.JuegoService;
import com.socialmovies.SocialMovies.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("/juegos")
@Produces(MediaType.APPLICATION_JSON)
public class JuegoController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private JuegoService juegoService;

    @POST
    public ResponseEntity<String> crearJuego(@RequestBody @Valid @NotNull(message = "El cuerpo de la petición no puede estar vacío.") MCrearJuego juego) {
        this.juegoService.crearJuego(juego);
        return ResponseEntity.ok(HttpStatus.ACCEPTED.toString());
    }

    @GET
    @Path("/devolverTodosLosJuegos/usuario/{username}")
    public ResponseEntity<List<MJuegoDelListadoDeJuegos>> devolverTodosLosJuegos(
            @PathParam("username") final String username
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);

        return ResponseEntity.ok(this.juegoService.devolverListadoDeJuegosParaUnUsuario(usuario));
    }


    @GET
    @Path("/devolverJuego/{idJuego}/usuario/{username}")
    public ResponseEntity<MJuegoParaUnUsuario> devolverUnJuegoParaUnUsuario(
            @PathParam("idJuego") final long idJuego,
            @PathParam("username") final String username
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        JuegoAdivinarPeliculaSerie juego = this.juegoService.buscarJuego(idJuego);

        return ResponseEntity.ok(this.juegoService.devolverUnJuegoParaUnUsuario(juego, usuario));
    }


    //TODO DEVOLVER EL RANKINGS DE USUARIOS
    @GET
    @Path("/devolverRakingTotaldeUsuariosEnJuegos")
    public ResponseEntity<List<MUsuarioRankingJuego>> devolverRakingdeUsuariosEnJuegos(
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosPorPagina") int cuantosPorPagina
    ){
        return ResponseEntity.ok(this.juegoService.devolverRakingdeUsuariosEnJuegos(pagina, cuantosPorPagina));
    }

}
