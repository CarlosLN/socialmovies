package com.socialmovies.SocialMovies.controller;

import java.util.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.socialmovies.SocialMovies.entity.Comentarios.ComentarioCapituloSerie;
import com.socialmovies.SocialMovies.entity.Pelicula;
import com.socialmovies.SocialMovies.entity.Series.CapituloSerie;
import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Votaciones.VotacionCapituloSerie;
import com.socialmovies.SocialMovies.model.Juego.MRespuestasJuegoUsuario;
import com.socialmovies.SocialMovies.model.Jwt.JwtResponse;
import com.socialmovies.SocialMovies.model.Pelicula.MComentarioPeliculaEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.Pelicula.MPeliculaRecomendadaMejorvaloradaMasvotada;
import com.socialmovies.SocialMovies.model.Pelicula.MVotacionPeliculaEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.Serie.MComentarioCapituloSerieEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.Serie.MDatosActualizadosTrasVotarCapitulo;
import com.socialmovies.SocialMovies.model.Serie.MSerieRecomendadaMejorvaloradaMasvotada;
import com.socialmovies.SocialMovies.model.Serie.MVotacionSerieEnPerfilAmigo;
import com.socialmovies.SocialMovies.model.TimeLine.MTimeLine;
import com.socialmovies.SocialMovies.model.Usuario.*;
import com.socialmovies.SocialMovies.repository.VotacionCapituloSerieRepository;
import com.socialmovies.SocialMovies.service.*;
import com.socialmovies.SocialMovies.service.exceptions.GlobalExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import com.socialmovies.SocialMovies.config.JwtTokenUtil;
import com.socialmovies.SocialMovies.entity.Usuario;

import static com.socialmovies.SocialMovies.constantes.Constantes.*;

@Component
@Path("/usuarios")
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioController {
	
	@Autowired
	@Qualifier("servicioUsuario")
	UsuarioService servicioUsuario;
	
	@Autowired
	@Qualifier("servicioAmistades")
	AmistadesService servicioAmistades;

	@Autowired
	PeliculaService servicioPelicula;

	@Autowired
	SerieService serieService;

	@Autowired
	SerieService servicioSerie;

	@Autowired
	VotacionCapituloService votacionCapituloSerie;

	@Autowired
    VotacionPeliculaService votacionPeliculaService;

	@Autowired
	ComentarioPeliculaService comentarioPeliculaService;

	@Autowired
	ComentarioCapituloSerieService comentarioCapituloSerieService;

	@Autowired()
	TimeLineService timeLineService;

	@Autowired
	private JuegoService juegoService;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@POST
	public ResponseEntity<JwtResponse> agregarUsuario(@RequestBody @Valid @NotNull(message = "El cuerpo de la petición no puede estar vacío.") Usuario usuario) {
		final UserDetails userDetails = servicioUsuario.crear(usuario);
		final String token = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new JwtResponse(token));
	}

	@PATCH
	@Path("/usuario/{username}")
	public ResponseEntity<String> modificarUsuario(
			@RequestBody HashMap<String, String> body,
			@PathParam("username")final String username){

		Usuario usuarioAux = this.servicioUsuario.devolverUsuario(username);

		String campo = body.get("campo");
		String nuevoValor = body.get("valor");

		switch (campo){
			case "avatar":
				usuarioAux.setAvatar(nuevoValor);
				this.servicioUsuario.actualizar(usuarioAux);
				return ResponseEntity.ok("Avatar actualizado");
			case "email":
				usuarioAux.setEmail(nuevoValor);
				this.servicioUsuario.actualizar(usuarioAux);
				return ResponseEntity.ok("Email actualizado");
			default:
				throw new GlobalExcepcion(618, ERROR_ACTUALIZANDO_USUARIO);
		}
	}
	
	@PATCH
	@Path("/usuario/{username}/cambiarPassword")
	public Response cambiarPassAutenticado(
			@RequestBody HashMap<String, String> body,
			@PathParam("username")final String username) {
		
		boolean actualizado = false;
		//String campo = body.get("campo");
		String nuevoValor = body.get("valor");
		
		//if(campo.equals("password")) {
			String oldPassword = body.get("oldpassword");
			
			Usuario user = servicioUsuario.devolverUsuario(username);
			
			if(!user.getPassword().equals(oldPassword)) {
				throw new GlobalExcepcion(601, PASS_NOT_EQUALS);
			}
			
			actualizado = servicioUsuario.updatePassword(user, nuevoValor);
		//} else {
			
		//}
		
		
		
		return Response.ok(actualizado).build();
	}

	
	@PUT
	public boolean actualizarUsuario(@RequestBody @Valid Usuario usuario) {
		return servicioUsuario.actualizar(usuario);
	}

	@POST
	@Path("/usuario/{username}/darDeAltaBaja/{altaBaja}")
	public ResponseEntity<Boolean> bajaAltaUsuario(
			@PathParam("username")final String username,
			@PathParam("altaBaja")final String altaBaja
	) {
		Usuario usuario = this.servicioUsuario.devolverUsuario(username);
		boolean darAltaBaja = false;
		if(altaBaja.equals("alta")){
			darAltaBaja = true;
		}
		return ResponseEntity.ok(servicioUsuario.darDeBajaAltaUsuario(usuario, darAltaBaja));
	}
	
	@GET
	//@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
	public Response obtenerUsuarios(){
	    return Response.ok(servicioUsuario.obtener()).build();
	}
	
	@GET
	@Path("/usuario/{username}")
	//@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
	public Response obtenerUsuario(@PathParam("username") final String nombre){
		
		/*
		 * 
		 * TODO CREAR UNO PARECIDO QUE NO DEVUELVA TODA LA INFORMACION
		 * 
		 * */
		return Response.ok(new MUsuario(servicioUsuario.devolverUsuario(nombre))).build();
	}
	
	@POST
	@Path("/login")
	public ResponseEntity<Map<String, String>> hacerLogin(@RequestBody @Valid Usuario usuario) {
			
		final UserDetails userDetails = servicioUsuario.login(usuario.getUsername(), usuario.getPassword());
		final Usuario user = servicioUsuario.devolverUsuario(usuario.getUsername());
		final String token = jwtTokenUtil.generateToken(userDetails);

		HashMap<String, String> map = new HashMap<>();
		map.put("token",token);
		map.put("avatar", user.getAvatar());

		return ResponseEntity.ok(map);
    }
	
	@POST
	@Path("/{username}/peticionesAmistad")
	public ResponseEntity<String> mandarPeticionAmistad(
			@RequestBody HashMap<String, String> body,
			@PathParam("username")final String usernameSolicitante) {
		
		String usernameSolicitado = body.get("usernameSolicitado");
		
		Usuario userSolicitante = servicioUsuario.devolverUsuario(usernameSolicitante);
		Usuario userSolicitado = servicioUsuario.devolverUsuario(usernameSolicitado);
		
		servicioAmistades.mandarPeticionAmistad(userSolicitante, userSolicitado);
		
		return ResponseEntity.ok(SOLICITUD_AMISTAD_ENVIADA_CORRECTAMENTE + usernameSolicitado);
	}
	
	@GET
	@Path("/{username}/peticionesAmistad")
	public ResponseEntity<List<MUsuarioAvatar>> peticionesAmistadRecibidas(@PathParam("username")final String usernameSolicitante){
		
		Usuario user =  servicioUsuario.devolverUsuario(usernameSolicitante);
		
		return ResponseEntity.ok(servicioAmistades.obtenerPeticionesAmistad(user, true));
	}

	@GET
	@Path("/{username}/peticionesAmistadEnviadas")
	public ResponseEntity<List<MUsuarioAvatar>> peticionesAmistadEnviadas(@PathParam("username")final String usernameSolicitante){

		Usuario user =  servicioUsuario.devolverUsuario(usernameSolicitante);

		return ResponseEntity.ok(servicioAmistades.obtenerPeticionesAmistad(user, false));
	}


	@GET
	@Path("/{username}/busquedaUsuario/{usernameBuscado}")
	public ResponseEntity<List<MUsuariosBuscados>> buscarUsuarios(@PathParam("username")final String usernameSolicitante,
																  @PathParam("usernameBuscado")final String usernameBuscado){

		List<Usuario> listUsuarios = servicioUsuario.buscarUsuarios(usernameBuscado);
		Usuario user =  servicioUsuario.devolverUsuario(usernameSolicitante);
		List<MUsuariosBuscados> usuariosBuscados = servicioAmistades.devolverUsuariosBuscados(user, listUsuarios);

		return ResponseEntity.ok(usuariosBuscados);
	}

	
	@PATCH
	@Path("/{username}/peticionesAmistad")
	public ResponseEntity<String> aceptarRechazarEliminarAmistad(
			@RequestBody HashMap<String, String> body,
			@PathParam("username")final String usernameSolicitante){
		
		String accion = body.get("accion");
		String usernameAmigo = body.get("usernameAmigo");
		
		Usuario user =  servicioUsuario.devolverUsuario(usernameSolicitante);
		Usuario userAmigo = servicioUsuario.devolverUsuario(usernameAmigo);
		
		if(accion.equals("aceptarPeticion")) {
			this.servicioAmistades.aceptarPeticionAmistad(user, userAmigo);
			return ResponseEntity.ok(PETICION_ACEPTADA);
		}else if(accion.equals("rechazarPeticion")) {
			this.servicioAmistades.eliminarCancelarAmistad(user, userAmigo, "rechazarPeticion");
			return ResponseEntity.ok(PETICION_RECHAZADA);
		}else if(accion.equals("eliminarAmistad")) {
			this.servicioAmistades.eliminarCancelarAmistad(user, userAmigo, "eliminarAmistad");
			return ResponseEntity.ok(AMISTAD_ELIMINADA);
		}
		
		return ResponseEntity.ok("");
	}

	@GET
	@Path("/{username}/obtenerAmigos")
	public ResponseEntity<List<MUsuarioAvatar>> listadoDeAmigos(@PathParam("username")final String usernameSolicitante){

		Usuario user =  servicioUsuario.devolverUsuario(usernameSolicitante);

		return ResponseEntity.ok(servicioAmistades.obtenerAmistadesMUsuarioAvatar(user));

	}

	@GET
	@Path("/{username}/devolverPerfilUsuario/{perfilUsuarioDevuelto}")
	public ResponseEntity<MPerfilUsuario> devolverPerfilUsuario(@PathParam("username")final String usernameSolicitante,
																@PathParam("perfilUsuarioDevuelto")final String usernameSolicitado){
		//Si soy yo mismo
		if(usernameSolicitado.equals(usernameSolicitante)){
			//TODO Obtener num de peliculas/series.. votadas, comentarios....
			Usuario user =  servicioUsuario.devolverUsuario(usernameSolicitante);
			return ResponseEntity.ok(new MPerfilUsuario(user));
		}else{
			Usuario user =  servicioUsuario.devolverUsuario(usernameSolicitante);
			Usuario userPerfilSolicitado = servicioUsuario.devolverUsuario(usernameSolicitado);

			//COMPROBAR QUE SON O AMIGOS O QUE LE HA MANDADO LA SOLICITUD
			this.servicioAmistades.sonAmigosOPeticion(user, userPerfilSolicitado);

			//TODO Obtener num de peliculas/series.. votadas, comentarios.... del usuario solicitado
			return ResponseEntity.ok(new MPerfilUsuario(userPerfilSolicitado));
		}
	}

	@GET
	@Path("/{username}/devolverDatosPerfilAmigo/{nombreAmigo}")
	public ResponseEntity<MDatosPerfilAmigo> devolverDatosPerfilAmigo(@PathParam("username")final String usernameSolicitante,
																	  @PathParam("nombreAmigo")final String usernameAmigo){

		Usuario user = servicioUsuario.devolverUsuario(usernameSolicitante);
		Usuario userAmigo = servicioUsuario.devolverUsuario(usernameAmigo);

		return ResponseEntity.ok(this.servicioAmistades.devolverDatosPerfilAmigo(user,userAmigo));
	}



	/*
	VOTACIONES
	 */

	@POST
	@Path("/{username}/votarPelicula/{idPelicula}")
	public ResponseEntity<HttpStatus> votarPelicula(@PathParam("username")final String usuario,	@PathParam("idPelicula")final long idPelicula,
			@RequestBody HashMap<String, Double> body
	){
		double voto = body.get("voto");

		Usuario u = servicioUsuario.devolverUsuario(usuario);
		Pelicula p = servicioPelicula.obtenerPeliculaPorId(idPelicula);

		this.votacionPeliculaService.crearActualizarVotacion(u,p,voto);

		return ResponseEntity.ok(HttpStatus.OK);
	}

	@GET
	@Path("/{username}/obtenerVotacionesPeliculasAmigo/{usernameAmigo}")
	public ResponseEntity<ArrayList<MVotacionPeliculaEnPerfilAmigo>> obtenerVotacionesPeliculasAmigo(
			@PathParam("username") final String username,
			@PathParam("usernameAmigo") final String usernameAmigo,
			@QueryParam(value = "orden") final String orden,
			@QueryParam(value = "pagina") int pagina,
			@QueryParam(value = "cuantasPeliculas") int cuantasPeliculas){

		Usuario usuario = this.servicioUsuario.devolverUsuario(username);
		Usuario amigo = this.servicioUsuario.devolverUsuario(usernameAmigo);

		return ResponseEntity.ok(this.votacionPeliculaService.obtenerVotacionesAmigo(usuario, amigo,orden,pagina,cuantasPeliculas));
	}

	/*
	VOTACIONES SERIES
	 */
	@POST
	@Path("/{username}/votarCapitulo/{idCapitulo}")
	public ResponseEntity<MDatosActualizadosTrasVotarCapitulo> votarCapitulo(@PathParam("username")final String usuario, @PathParam("idCapitulo")final long idCapitulo,
																			 @RequestBody HashMap<String, Double> body
	){
		double voto = body.get("voto");

		Usuario u = servicioUsuario.devolverUsuario(usuario);
		CapituloSerie p = servicioSerie.obtenerCapituloSeriePorId(idCapitulo);

		return ResponseEntity.ok(this.votacionCapituloSerie.crearActualizarVotacion(u,p,voto));
	}

	@GET
	@Path("/{username}/obtenerVotacionesSeriesAmigo/{usernameAmigo}")
	public ResponseEntity<List<MVotacionSerieEnPerfilAmigo>> obtenerVotacionesSeriesAmigo(
			@PathParam("username") final String username,
			@PathParam("usernameAmigo") final String usernameAmigo,
			@QueryParam(value = "orden") final String orden,
			@QueryParam(value = "pagina") int pagina,
			@QueryParam(value = "cuantasSeries") int cuantasSeries){

		Usuario usuario = this.servicioUsuario.devolverUsuario(username);
		Usuario amigo = this.servicioUsuario.devolverUsuario(usernameAmigo);

		return ResponseEntity.ok(this.votacionCapituloSerie.obtenerVotacionesDeSerieDeUnAmigo(usuario, amigo,orden,pagina,cuantasSeries));
	}


	/*
	COMENTARIOS
	 */

	@POST
	@Path("/{username}/comentarPelicula/{idPelicula}")
	public ResponseEntity<HttpStatus> comentarPelicula(
			@PathParam("username")final String usuario,
			@PathParam("idPelicula")final long idPelicula,
			@RequestBody HashMap<String, String> body){

		String comentario = body.get("comentario");

		Usuario u = servicioUsuario.devolverUsuario(usuario);
		Pelicula p = servicioPelicula.obtenerPeliculaPorId(idPelicula);

		this.comentarioPeliculaService.crearActualizarEliminarComentario(u,p,comentario);

		return ResponseEntity.ok(HttpStatus.OK);
	}

	@GET
	@Path("/{username}/obtenerComentariosPeliculasAmigo/{usernameAmigo}")
	public ResponseEntity<List<MComentarioPeliculaEnPerfilAmigo>> obtenerComentariosPeliculasAmigo(
			@PathParam("username") final String username,
			@PathParam("usernameAmigo") final String usernameAmigo,
			@QueryParam(value = "orden") final String orden,
			@QueryParam(value = "pagina") int pagina,
			@QueryParam(value = "cuantasPeliculas") int cuantasPeliculas){

		Usuario usuario = this.servicioUsuario.devolverUsuario(username);
		Usuario amigo = this.servicioUsuario.devolverUsuario(usernameAmigo);

		return ResponseEntity.ok(this.comentarioPeliculaService.obtenerComentariosPeliculasAmigo(usuario, amigo,orden,pagina,cuantasPeliculas));
	}

	/*
	COMENTARIOS SERIES
	 */

	@POST
	@Path("/{username}/comentarCapituloSerie/{idCapituloSerie}")
	public ResponseEntity<HttpStatus> comentarCapituloSerie(
			@PathParam("username")final String usuario,
			@PathParam("idCapituloSerie")final long idCapituloSerie,
			@RequestBody HashMap<String, String> body){

		String comentario = body.get("comentario");

		Usuario u = servicioUsuario.devolverUsuario(usuario);
		CapituloSerie capituloSerie = serieService.obtenerCapituloSeriePorId(idCapituloSerie);

		this.comentarioCapituloSerieService.crearActualizarEliminarComentarioCapituloSerie(u,capituloSerie,comentario);

		return ResponseEntity.ok(HttpStatus.OK);
	}

	@GET
	@Path("/{username}/obtenerComentariosCapitulosSeriesAmigo/{usernameAmigo}")
	public ResponseEntity<List<MComentarioCapituloSerieEnPerfilAmigo>> obtenerComentariosCapituloSerieAmigo(
			@PathParam("username") final String username,
			@PathParam("usernameAmigo") final String usernameAmigo,
			@QueryParam(value = "orden") final String orden,
			@QueryParam(value = "pagina") int pagina,
			@QueryParam(value = "cuantasSeries") int cuantasSeries){

		//Usuario usuario = this.servicioUsuario.devolverUsuario(username);
		Usuario amigo = this.servicioUsuario.devolverUsuario(usernameAmigo);

		return ResponseEntity.ok(this.comentarioCapituloSerieService.obtenerComentarioCapituloSerieAmigo(amigo,orden,pagina,cuantasSeries));
	}

	/*
	RECOMENDACIONES
	 */

	@GET
	@Path("/{username}/obtenerPeliculaRecomendada/{idPelicula}")
	public ResponseEntity<MPeliculaRecomendadaMejorvaloradaMasvotada> obtenerPeliculaRecomendada(
			@PathParam("username")final String usuario,
			@PathParam("idPelicula")final long idPelicula
	){
		Usuario u = servicioUsuario.devolverUsuario(usuario);
		Pelicula p;
		try{
			p = servicioPelicula.obtenerPeliculaPorId(idPelicula);
		}catch(Exception e){
			return ResponseEntity.ok(this.servicioPelicula.obtenerPeliculaRecomendada(u,null));
		}

		return ResponseEntity.ok(this.servicioPelicula.obtenerPeliculaRecomendada(u,p));
	}

	@GET
	@Path("/{username}/obtenerSerieRecomendada/{idSerie}")
	public ResponseEntity<MSerieRecomendadaMejorvaloradaMasvotada> obtenerSerieRecomendada(
			@PathParam("username")final String usuario,
			@PathParam("idSerie")final long idSerie
	){
		Usuario u = servicioUsuario.devolverUsuario(usuario);
		Serie serie;
		try{
			serie = servicioSerie.obtenerSeriePorId(idSerie);
		}catch(Exception e){
			return ResponseEntity.ok(this.servicioSerie.obtenerSerieRecomendada(u,null));
		}

		return ResponseEntity.ok(this.servicioSerie.obtenerSerieRecomendada(u,serie));
	}

	@GET
	@Path("/{username}/obtenerListadoDeAmigosDeUnAmigo/{usernameAmigo}")
	public ResponseEntity<List<MUsuariosBuscados>> obtenerListadoDeAmigosDeUnAmigo(
			@PathParam("username")final String usuario,
			@PathParam("usernameAmigo")final String usuarioAmigo,
			@QueryParam(value = "pagina") int pagina,
			@QueryParam(value = "cuantosPorPagina") int cuantosPorPagina
	){
		Usuario u = servicioUsuario.devolverUsuario(usuario);
		Usuario uAmigo = servicioUsuario.devolverUsuario(usuarioAmigo);

		return ResponseEntity.ok(this.servicioAmistades.obtenerMiRelacionConAmigosDeUnAmigo(u,uAmigo,pagina,cuantosPorPagina));
	}



	/*
	TIME LINE
	 */


	@POST
	@Path("/{username}/timeLine/{cuando}")
	public ResponseEntity<MTimeLine> obtenerTimeLineDeUnUsuario(
			@PathParam("username")final String usuario,
			@PathParam("cuando") String cuando,
			@RequestBody MTimeLine timeLineActual){

		String cuando2;

		if(!cuando.equals("antes") && !cuando.equals("despues")){
			cuando2="";
		}else{
			cuando2=cuando;
		}

		Usuario u = servicioUsuario.devolverUsuario(usuario);

		if(timeLineActual==null){
			return ResponseEntity.ok(this.timeLineService.obtenerTimeLineUsuario(u, cuando2, new MTimeLine()));
		}else{
			return ResponseEntity.ok(this.timeLineService.obtenerTimeLineUsuario(u, cuando2, timeLineActual));
		}

	}


	/**
	 *	PARTICIPAR EN UN JUEGO
	 */
	//TODO PARTICIPAR EN UN JUEGO
	@POST
	@Path("/{username}/participarEnJuego")
	public ResponseEntity<Integer> participarEnUnJuego(
			@PathParam("username")final String usuario,
			@RequestBody List<MRespuestasJuegoUsuario> respuestasJuegoUsuario) {

		Usuario u = servicioUsuario.devolverUsuario(usuario);

		return ResponseEntity.ok(this.juegoService.participarEnUnJuego(u, respuestasJuegoUsuario));
	}


}
