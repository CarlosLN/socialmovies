package com.socialmovies.SocialMovies.controller;

import com.socialmovies.SocialMovies.entity.Series.Serie;
import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Serie.*;
import com.socialmovies.SocialMovies.service.ComentarioCapituloSerieService;
import com.socialmovies.SocialMovies.service.SerieService;
import com.socialmovies.SocialMovies.service.UsuarioService;
import com.socialmovies.SocialMovies.service.VotacionCapituloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("/series")
@Produces(MediaType.APPLICATION_JSON)
public class SerieController {

    @Autowired
    private SerieService serieService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private VotacionCapituloService votacionCapituloService;

    @Autowired
    private ComentarioCapituloSerieService comentarioCapituloSerieService;

    @POST
    public ResponseEntity<String> crearSerie(@RequestBody @Valid @NotNull(message = "El cuerpo de la petición no puede estar vacío.") MCrearSerie crearSerie) {
        this.serieService.crearSerie(crearSerie);
        return ResponseEntity.ok(HttpStatus.ACCEPTED.toString());
    }

    @GET
    @Path("/buscarSerie/{serieBuscada}")
    public ResponseEntity<MSeriesBuscadasEncontradas> buscarSeries(
            @PathParam("serieBuscada") final String serieBuscada,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantasSeries") int cuantasSeries
    ){
        return ResponseEntity.ok(this.serieService.buscarSerie(serieBuscada, pagina, cuantasSeries));
    }

    @GET
    @Path("/devolverSerieUsuario/{idSerie}/usuario/{username}")
    public ResponseEntity<MSerie> devolverSerieUsuario(
            @PathParam("idSerie") final long idSerie,
            @PathParam("username") final String username,
            @QueryParam(value = "cuantosAmigosPorVotosYComentarios") int cuantosAmigos
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        Serie serie = this.serieService.obtenerSeriePorId(idSerie);

        return ResponseEntity.ok(this.serieService.buscarSerieConUsuario(usuario, serie, cuantosAmigos));
    }


    @GET
    @Path("/devolverTemporadaYSusCapitulosDeUnaSerie/{idSerie}/temporada/{temporada}/usuario/{username}")
    public ResponseEntity<MTemporada> obtenerTemporadaYSusCapitulosDeUnaSerie(
            @PathParam("idSerie") final long idSerie,
            @PathParam("username") final String username,
            @PathParam("temporada") int temporada
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        Serie serie = this.serieService.obtenerSeriePorId(idSerie);

        return ResponseEntity.ok(this.serieService.obtenerTemporadaYSusCapitulosDeUnaSerie(serie, usuario, temporada));
    }


    @GET
    @Path("/devolverVotacionesAmigosEnFichaDeSerie/{idSerie}/usuario/{username}")
    public ResponseEntity<List<MVotacionAmigoEnFichaSerie>> devolverVotacionesAmigosEnFichaDeSerie(
        @PathParam("idSerie") final long idSerie,
        @PathParam("username") final String username,
        @QueryParam(value = "pagina") int pagina,
        @QueryParam(value = "cuantosAmigos") int cuantosAmigos
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        Serie serie = this.serieService.obtenerSeriePorId(idSerie);

        return ResponseEntity.ok(this.votacionCapituloService.obtenerVotacionesDeAmigosDeUnaSerie(usuario,serie,pagina,cuantosAmigos));
    }

    @GET
    @Path("/devolverComentariosAmigosEnFichaCapituloSerie/{idSerie}/usuario/{username}")
    public ResponseEntity<List<MComentarioAmigoEnFichaCapituloSerie>> devolverComentariosAmigosEnFichaCapituloSerie(
            @PathParam("idSerie") final long idSerie,
            @PathParam("username") final String username,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosAmigos") int cuantosAmigos
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        Serie serie = this.serieService.obtenerSeriePorId(idSerie);

        return ResponseEntity.ok(this.comentarioCapituloSerieService.obtenerComentarioDeAmigosDeUnaSerie(usuario, serie, pagina, cuantosAmigos));
    }


    @GET
    @Path("/obtenerSerieMejorValorada/usuario/{username}")
    public ResponseEntity<MSerieRecomendadaMejorvaloradaMasvotada> obtenerSerieMejorValorada(
            @PathParam("username") final String username,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosPorPagina") int cuantosPorPagina
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        return ResponseEntity.ok(this.serieService.obtenerSerieMejorValorada(usuario, pagina, cuantosPorPagina));
    }

    @GET
    @Path("/obtenerSerieMasVotada/usuario/{username}")
    public ResponseEntity<MSerieRecomendadaMejorvaloradaMasvotada> obtenerSerieMasVotada(
            @PathParam("username") final String username,
            @QueryParam(value = "pagina") int pagina,
            @QueryParam(value = "cuantosPorPagina") int cuantosPorPagina
    ){
        Usuario usuario = this.usuarioService.devolverUsuario(username);
        return ResponseEntity.ok(this.serieService.obtenerSerieMasVotada(usuario, pagina, cuantosPorPagina));
    }



}
