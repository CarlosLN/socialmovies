package com.socialmovies.SocialMovies.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.socialmovies.SocialMovies.entity.Usuario;
import com.socialmovies.SocialMovies.model.Usuario.MUsuario;

@Component("convertidor")
public class Convertidor {

	public List<MUsuario> convertirUsuario(List<Usuario> usuarios){
		List<MUsuario> musuarios = new ArrayList<>();
		
		for(Usuario usuario: usuarios) {
			musuarios.add(new MUsuario(usuario));
		}
		
		return musuarios;
	}
}
